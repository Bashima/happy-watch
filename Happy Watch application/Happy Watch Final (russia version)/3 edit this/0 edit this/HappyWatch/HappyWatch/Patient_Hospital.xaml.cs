﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using HappyWatch.HWwcf;
using System.Diagnostics;

namespace HappyWatch
{
    public partial class Patient_Hospital : PhoneApplicationPage
    {
        public HappyWatchServiceClient proxy = new HappyWatchServiceClient("BasicHttpBinding_IHappyWatchService");
        public string Email { get; set; }
       public string str;
        public Patient_Hospital()
        {
            InitializeComponent();
            progress_bar.IsIndeterminate = true;
        }
        protected override void OnNavigatedTo(System.Windows.Navigation.NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            var title = NavigationContext.QueryString["Email"];

            Email = (String)title;
            Debug.WriteLine("Email: " + Email);
            //MakeList();
            MakeOwnList();



        }

        private void Panorama_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            switch (((Panorama)sender).SelectedIndex)
            {
                case 0: // defines the first PanoramaItem
                    //making own hospitallist
                    progress_bar.IsIndeterminate = true;
                    MakeOwnList();
                    break;

                case 1: // second one
                    //making all hospital list
                    progress_bar.IsIndeterminate = true;
                     MakeAllList();
                    break;

            }
        }

        public void MakeOwnList()
        {
            proxy.MakeOwnHosptialListCompleted += new EventHandler<MakeOwnHosptialListCompletedEventArgs>(proxy_MakeOwnHosptialListCompleted);
            proxy.MakeOwnHosptialListAsync(Email);
        }

        void proxy_MakeOwnHosptialListCompleted(object sender, MakeOwnHosptialListCompletedEventArgs e)
        {
            Debug.WriteLine("Own Hospital List::");
            peoplesList.ItemsSource = e.Result.ToList();
            progress_bar.IsIndeterminate = false;
            
        }
        public void MakeAllList()
        {
            proxy.MakeAllHosptialListCompleted += new EventHandler<MakeAllHosptialListCompletedEventArgs>(proxy_MakeAllHosptialListCompleted);
            proxy.MakeAllHosptialListAsync();
        }

        void proxy_MakeAllHosptialListCompleted(object sender, MakeAllHosptialListCompletedEventArgs e)
        {
            //throw new NotImplementedException();
            Debug.WriteLine("all hospital list");
            HospitalList.ItemsSource = e.Result.ToList();
            progress_bar.IsIndeterminate = false;
            List<string> hosps = new List<string>();
           
            foreach (HospitalMod h in e.Result.ToList())
            {
                str = h.Name + "(" + h.Mail + ")";
                hosps.Add(str);
 
            }
            auto_box.ItemsSource = hosps.ToList();
        }

        private void AddButton_Click(object sender, RoutedEventArgs e)
        {
            string sent;
            str = auto_box.Text;
            int i = str.IndexOf('(');
            int j = str.IndexOf(')');
            string hMail = str.Substring(i+1,j-i-1);
            sent = Email + "#" + hMail;
            Debug.WriteLine("sent: "+sent);
            proxy.MakeHospitalReqCompleted += new EventHandler<MakeHospitalReqCompletedEventArgs>(proxy_MakeHospitalReqCompleted);
            proxy.MakeHospitalReqAsync(sent);

           
        }

        void proxy_MakeHospitalReqCompleted(object sender, MakeHospitalReqCompletedEventArgs e)
        {
            Debug.WriteLine("Hospital Request is :"+e.Result);
            if (e.Result == true)
            {
                MessageBox.Show("request sent");
            }
            else
            {
                MessageBox.Show("failure while sending request");
            }
        }

        private void MyContextMenu_Opened(object sender, RoutedEventArgs e)
        {
            //khali thakbe
        }

        private void delete_hospital_clicked(object sender, RoutedEventArgs e)
        {
            //ekhane code boshbe
           // MessageBox.Show("clicked");
            //MessageBox.Show("clicked");
            string header = (sender as MenuItem).Header.ToString();
            Debug.WriteLine("doc said: " + header);
            ListBoxItem selectedListBoxItem = this.peoplesList.ItemContainerGenerator.ContainerFromItem((sender as MenuItem).DataContext) as ListBoxItem;
            ObPatient temp = (ObPatient)selectedListBoxItem.Content;
            if (selectedListBoxItem != null)
                Debug.WriteLine("Doc Mail: " + temp.pMail);
            else
                Debug.WriteLine("NULL");
            string sent = Email + "#" + temp.pMail;
            Debug.WriteLine("SENT: " + sent);
            proxy.PatientDelteHospitalCompleted += new EventHandler<PatientDelteHospitalCompletedEventArgs>(proxy_PatientDelteHospitalCompleted);
            proxy.PatientDelteHospitalAsync(sent);
        }

        void proxy_PatientDelteHospitalCompleted(object sender, PatientDelteHospitalCompletedEventArgs e)
        {
            Debug.WriteLine("res: "+e.Result);
        }
        private void add_hospital_clicked(object sender, RoutedEventArgs e)
        {
            string header = (sender as MenuItem).Header.ToString();
            Debug.WriteLine("doc said: " + header);
            ListBoxItem selectedListBoxItem = this.HospitalList.ItemContainerGenerator.ContainerFromItem((sender as MenuItem).DataContext) as ListBoxItem;
            HospitalMod temp = (HospitalMod)selectedListBoxItem.Content;
            if (selectedListBoxItem != null)
                Debug.WriteLine("Doc Mail: " + temp.Mail);
            else
                Debug.WriteLine("NULL");
            string sent = Email + "#" + temp.Mail;
            Debug.WriteLine("SENT: " + sent);
            proxy.MakeHospitalReqCompleted += new EventHandler<MakeHospitalReqCompletedEventArgs>(proxy_MakeHospitalReqCompleted);
            proxy.MakeHospitalReqAsync(sent);
 
        }
    }
}