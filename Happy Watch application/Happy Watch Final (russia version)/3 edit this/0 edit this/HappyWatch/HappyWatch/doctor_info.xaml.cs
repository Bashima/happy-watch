﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.Diagnostics;
using HappyWatch.HWwcf;

namespace HappyWatch
{
    public partial class doctor_info : PhoneApplicationPage
    {
        public string Email { get; set; }
        public HappyWatchServiceClient proxy;
        string sent;
        bool pop_up_flag = false;
        public doctor_info()
        {

            InitializeComponent();
            progress_bar.IsIndeterminate = true;
        }

        protected override void OnNavigatedTo(System.Windows.Navigation.NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            var title = NavigationContext.QueryString["DeviceId"];
            progress_bar.IsIndeterminate = true;
            Email = (String)title;
            proxy = new HappyWatchServiceClient("BasicHttpBinding_IHappyWatchService");
            ShowDocInfo();
        }

        public void ShowDocInfo()
        {
            progress_bar.IsIndeterminate = true;
            proxy.GetDocInfoCompleted += new EventHandler<GetDocInfoCompletedEventArgs>(proxy_GetDocInfoCompleted);
            proxy.GetDocInfoAsync(Email);

            
 
        }

        void proxy_GetDocInfoCompleted(object sender, GetDocInfoCompletedEventArgs e)
        {
            string res = e.Result;
            Debug.WriteLine("Doc Info: "+res);
            string[] ar = new string[10];
            ar = res.Split('#');
            Name_text.Text = ar[0];
            int i = ar[5].IndexOf(' ');

            string DOB = ar[5].Substring(0,i);
            Debug.WriteLine(DOB);
            Date_text.Text = DOB;
            Gender_text.Text=ar[4];
            password_text.Text = ar[2];
            Email_text.Text = Email;
            contact_text.Text=ar[3];
            speciality_text.Text=ar[6];
            progress_bar.IsIndeterminate = false;
           // update_date_picker.Value = Convert.ToDateTime(ar[5]);
        }

        private void update_button_click(object sender, System.Windows.RoutedEventArgs e)
        {
        	// TODO: Add event handler implementation here.
            string update_name, update_password, update_date, update_contact, update_speciality;
            update_name = update_name_box.Text;
            update_password = password_text_update.Text;
            update_date = String.Format("{0:yyyy-MM-dd}", update_date_picker.Value);
            update_contact=update_contact_box.Text;
            update_speciality = update_speciality_box.Text;
            if (update_name.Equals(""))
            {
                update_name = Name_text.Text;
            }
            if (update_password.Equals(""))
            {
                update_password = password_text.Text;    
            }
            if (update_contact.Equals(""))
            {
                update_contact = contact_text.Text;
            }
            if (update_speciality.Equals(""))
            {
                update_speciality = speciality_text.Text;
            }
            if (update_date.Equals(String.Format("{0:yyyy-MM-dd}", DateTime.Now)))
            {
                update_date=Date_text.Text;
                Debug.WriteLine("here:::"+update_date);
            }
            sent= Email+","+update_name+","+ update_password+","+ update_date+","+ update_contact+","+ update_speciality;
            Debug.WriteLine("sent:   "+sent);
            this.update_out.Begin();
            pop_up_flag = false;
            progress_bar.IsIndeterminate = true;
            proxy.UpdateDoctorInfoCompleted += new EventHandler<UpdateDoctorInfoCompletedEventArgs>(proxy_UpdateDoctorInfoCompleted);
            Debug.WriteLine("Mail: " + Email);
            proxy.UpdateDoctorInfoAsync(sent);
			
        }
        void proxy_UpdateDoctorInfoCompleted(object sender, UpdateDoctorInfoCompletedEventArgs e)//update completed
        {
            progress_bar.IsIndeterminate = false;
            if (e.Result == true)
            {
                MessageBox.Show("update completed");
            }
            else
            {
                MessageBox.Show("update failed");
            }
            ShowDocInfo();
        }
        private void cancell_button_click(object sender, System.Windows.RoutedEventArgs e)
        {
        	// TODO: Add event handler implementation here.
			this.update_out.Begin();
            pop_up_flag = false;
        }

        private void update_in_button_click(object sender, System.EventArgs e)
        {
        	this.update_in.Begin();
            pop_up_flag = true;
			// TODO: Add event handler implementation here.
        }
        protected override void OnBackKeyPress(System.ComponentModel.CancelEventArgs e)
        {
            if (pop_up_flag == true)
            {
                this.update_out.Begin();
                pop_up_flag = false;
                e.Cancel = true;
            }
        }
    }
}