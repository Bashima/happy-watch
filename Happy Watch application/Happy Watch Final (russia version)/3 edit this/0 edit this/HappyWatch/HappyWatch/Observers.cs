﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Shapes;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Collections;
using System.Diagnostics;
using HappyWatch.HWwcf;
namespace HappyWatch
{
    class Observers : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        private ObservableCollection<Obs> _listOfobservers;
        public ObservableCollection<Obs> ListOfObservers
        {
            get { return _listOfobservers; }
            set
            {
                _listOfobservers = value;
                if (PropertyChanged != null)
                    PropertyChanged(this, new PropertyChangedEventArgs("ListOfObservers"));
            }
        }
      public Observers()
        {
            HappyWatchServiceClient proxy = new HappyWatchServiceClient();
            proxy.AllObserversListCompleted += new EventHandler<AllObserversListCompletedEventArgs>(proxy_AllObserversListCompleted);
            Debug.WriteLine("PEMAIL: "+Constants.PEmail);
            proxy.AllObserversListAsync(Constants.PEmail);


        }
         void proxy_AllObserversListCompleted(object sender, AllObserversListCompletedEventArgs e)
         {
             ListOfObservers = new ObservableCollection<Obs>();
             List<ObPatient> list = new List<ObPatient>();
             list = e.Result.ToList();
             Debug.WriteLine("here");
             foreach (ObPatient x in list)
             {
                ListOfObservers.Add(new Obs() { Name=x.pMail});
             }

         }
    }
    public class Obs
    {
        public string Name { get; set; }
        public override string ToString()
        {
            return Name;
        }
    }
}
