﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using HappyWatch.HWwcf;
using System.Diagnostics;

namespace HappyWatch
{
    public partial class UserPage : PhoneApplicationPage
    {
        bool edit_state=false;
        string Email,sent,name,date,condition,password;
        HappyWatchServiceClient proxy;
        string dob;
        public UserPage()
        {
            proxy = new HappyWatchServiceClient("BasicHttpBinding_IHappyWatchService");
            InitializeComponent();
            progress_bar.IsIndeterminate = true;
        }
        protected override void OnNavigatedTo(System.Windows.Navigation.NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            var title = NavigationContext.QueryString["DeviceId"];
            Email = (String)title;
            proxy = new HappyWatchServiceClient("BasicHttpBinding_IHappyWatchService");
            userinfocall();
        }
        private void edit_button_click(object sender, System.EventArgs e)//press edit button to pop up the edit user info grid
        {
            this.popup_entry_anim.Begin();
            edit_state = true;
        }
        private void add_button_click(object sender, System.EventArgs e)//press update button to update info
        {
            if (update_name_box.Text.Equals(""))//checking if the name box is empty
            {
                name = Name_text.Text;
            }
            else
            {
                name = update_name_box.Text;
            }
            if (password_up_box.Text.Equals(""))//checking if the name box is empty
            {
                password=password_text.Text;
            }
            else
            {
                password=password_up_box.Text;
            }
            date = String.Format("{0:yyyy-MM-dd}", update_date_picker.Value);
            if (Convert.ToBoolean(update_autistic_box.IsChecked) && Convert.ToBoolean(update_pregnant_box.IsChecked))
            {
                condition = "Autistic+Pregnent";

            }
            else if (Convert.ToBoolean(update_autistic_box.IsChecked) && !(Convert.ToBoolean(update_pregnant_box.IsChecked)))
            {
                condition = "Autistic";

            }
            else if (!(Convert.ToBoolean(update_autistic_box.IsChecked)) && Convert.ToBoolean(update_pregnant_box.IsChecked))
            {
                condition = "Pregnent";

            }
            else
            {
                condition = "None";
            }
            sent = Email + "#" + name + "#" + password + "#" + date + "#" + condition;
            updatecall();
            this.pop_exit_anim.Begin();
            edit_state = false;
        }

        private void cancel_button_click(object sender, System.Windows.RoutedEventArgs e)//press cancel to exit edit pop up
        {
            this.pop_exit_anim.Begin();
            edit_state = false;
        }
		protected override void OnBackKeyPress(System.ComponentModel.CancelEventArgs e)//handle edit pop using back button
        {
            if (edit_state==true)
            {
                this.pop_exit_anim.Begin();
                edit_state = false;
                e.Cancel = true;

            }
            
        }
        void userinfocall()//connecting to the wcf to get info
        {

            progress_bar.IsIndeterminate = true;
            proxy.PatientInfoCompleted += new EventHandler<PatientInfoCompletedEventArgs>(proxy_PatientInfoCompleted);
            Debug.WriteLine("Mail: "+Email);
            proxy.PatientInfoAsync(Email);

        }

       

        void proxy_PatientInfoCompleted(object sender, PatientInfoCompletedEventArgs e)
        {
            Debug.WriteLine("Infos: " + e.Result);
            string[] infos = new string[20];
            infos = e.Result.ToString().Split('#');
            
          
            
            DeviceId_text.Text = infos[5];
            Name_text.Text = infos[0];
            Date_text.Text = infos[2];
            Gender_text.Text = infos[3];
            email_text.Text = infos[1];
            password_text.Text = infos[7];

            progress_bar.IsIndeterminate = false;

            

             Condition_text.Text =infos[6];

             if (infos[6] == "Autistic")
             {
                 update_autistic_box.IsChecked = true;
             }
             else if (infos[6] == "Pregnant")
             {
                 update_pregnant_box.IsChecked = true;
             }
             else if (infos[6] == "Autistic+Pregnent")
             {
                 update_pregnant_box.IsChecked = true;
                 update_autistic_box.IsChecked = true;
             }
       
        }

        /*
        void proxy_FindUserIdCompleted(object sender, FindUserIdCompletedEventArgs e)
        {
            DeviceId_text.Text = e.Result;

        }

        void proxy_FindUserNameCompleted(object sender, FindUserNameCompletedEventArgs e)
        {
            Name_text.Text = e.Result;

        }

        void proxy_FindUserDobCompleted(object sender, FindUserDobCompletedEventArgs e)
        {
            dob = e.Result;
            Date_text.Text = dob;
            DateTime date_new= Convert.ToDateTime(dob);
           // update_date_picker.Value = date_new;
        }
        void proxy_FindGenderCompleted(object sender, FindGenderCompletedEventArgs e)
        {
            Gender_text.Text = e.Result;
            if (e.Result.Equals("Male"))
            {
                update_male_button.IsChecked = true;
            }
            else if (e.Result.Equals("Female"))
            {
                update_female_button.IsChecked = true;
            }
            else
            {
                update_other_button.IsChecked = true;
            }

        }
        void proxy_FindPConditionCompleted(object sender, FindPConditionCompletedEventArgs e)
        {
            Condition_text.Text = e.Result;
            if (e.Result == "Autistic")
            {
                update_autistic_box.IsChecked = true;
            }
            else if(e.Result=="Pregnant")
            {
                update_pregnant_box.IsChecked= true;
            }
            else if (e.Result == "Autistic+Pregnent")
            {
                update_pregnant_box.IsChecked = true;
                update_autistic_box.IsChecked = true;
            }
        }*/
       
        
        /// <summary>
        /// /////////////////update
        /// </summary>
        /// /*
        void updatecall()//connect to wcf to update the database
        {
            progress_bar.IsIndeterminate = true;
            proxy.UpdateUserInfoCompleted += new EventHandler<UpdateUserInfoCompletedEventArgs>(proxy_UpdateUserInfoCompleted);
            Debug.WriteLine("Mail: "+Email);
            proxy.UpdateUserInfoAsync(sent);
        }

        void proxy_UpdateUserInfoCompleted(object sender, UpdateUserInfoCompletedEventArgs e)//update completed
        {
            progress_bar.IsIndeterminate = false;
            if (e.Result == true)
            {
                MessageBox.Show("update completed");
            }
            else
            {
                MessageBox.Show("update failed");
            }
            userinfocall();
        }

        private void delete_button_click(object sender, EventArgs e)
        {
            proxy.DeactivePatientCompleted += new EventHandler<DeactivePatientCompletedEventArgs>(proxy_DeactivePatientCompleted);
            proxy.DeactivePatientAsync(Email);
        }
        void proxy_DeactivePatientCompleted(object sender, DeactivePatientCompletedEventArgs e)
        {
            if (e.Result == true)
            {
                MessageBox.Show("profile deleted");
                NavigationService.Navigate(new Uri("/StartPage.xaml", UriKind.Relative));
            }
            else
                MessageBox.Show("error while deleting profile");
        }
        
    }
}