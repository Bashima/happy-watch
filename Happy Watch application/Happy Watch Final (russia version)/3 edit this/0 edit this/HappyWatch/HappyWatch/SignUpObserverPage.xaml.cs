﻿using System;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Microsoft.Xna.Framework;
using System.IO.IsolatedStorage;
using Microsoft.Phone.Scheduler;
using System.Diagnostics;
using HappyWatch.HWwcf;
namespace HappyWatch
{
    public partial class SignUpObserverPage : PhoneApplicationPage
    {
		string OName, Email,Password, Dob, Contact, Designation, Status, Gender, sent, warning,RePass;
        public HappyWatchServiceClient proxy;
        public SignUpObserverPage()
        {
            InitializeComponent();
            loading.Visibility = Visibility.Collapsed;
        }
        private void sign_up_observer(object sender, System.Windows.Input.GestureEventArgs e)
        {

            Debug.WriteLine("in the button");
            warning = "";
            OName = name_box_up.Text;
            Email = email_box_up.Text;
            String Pword = password_box_up.Text;
            RePass = re_password_box_up.Text;
            if ((Pword.Equals(RePass)))
            {
                Password = password_box_up.Text;
            }
            else
            {
                Password = "";
                Microsoft.Xna.Framework.GamerServices.Guide.BeginShowMessageBox("Warning", "retype same password", new string[] { "ok" }, 0, Microsoft.Xna.Framework.GamerServices.MessageBoxIcon.Alert, null, null);
            }
            Dob = String.Format("{0:yyyy-MM-dd}", sign_up_date_picker.Value);
            Contact = contact_box_up.Text;
            Designation = "observer";
            Status = "active";
            if (Convert.ToBoolean(sign_up_female_button.IsChecked))
            {
                Gender = "Female";
            }
            else if (Convert.ToBoolean(sign_up_male_button.IsChecked))
            {
                Gender = "Male";
            }
            else if (Convert.ToBoolean(sign_up_other_button.IsChecked))
            {
                Gender = "Other";
            }
            if (OName.Equals(""))
            {
                warning = "insert name";

            }
            else if (IsValidEmail(Email) == false)
            {
                warning = "insert valid email address";
            }
            else if (Email.Equals(""))
            {
                warning = "insert email address";

            }
            else if (Contact.Equals(""))
            {
                warning = "insert contact number";
            }
            else if (Password.Equals(""))
            {
                warning = "enter password";
            }
            else if (RePass.Equals(""))
            {
                warning = "retype password";
            }
            if (warning.Equals(""))
            {
                string temp;
                temp = Email + "," + "active";
                prog.IsIndeterminate = true;
                loading.Visibility = Visibility.Visible;
                proxy = new HappyWatchServiceClient("BasicHttpBinding_IHappyWatchService");
                proxy.VerifyEmailCompleted += new EventHandler<VerifyEmailCompletedEventArgs>(proxy_VerifyEmailCompleted);
                proxy.VerifyEmailAsync(temp);

               

            }
            else//verifying if the given deviceid is new
            {
                Microsoft.Xna.Framework.GamerServices.Guide.BeginShowMessageBox("Warning", warning, new string[] { "ok" }, 0, Microsoft.Xna.Framework.GamerServices.MessageBoxIcon.Alert, null, null);
            }
        }
        void proxy_VerifyEmailCompleted(object sender, VerifyEmailCompletedEventArgs e)
        {
            prog.IsIndeterminate = false;
            loading.Visibility = Visibility.Collapsed;
            Debug.WriteLine("Result is: " + e.Result);
            if (e.Result.Equals("N/A"))
            {
                proxy = new HappyWatchServiceClient("BasicHttpBinding_IHappyWatchService");
                sent = OName + "," + Password + "," + Dob + "," + Gender + "," + Contact + "," + Designation + "," + Email + "," + Status;
                Debug.WriteLine(sent);
                loading.Visibility = Visibility.Visible;
                prog.IsIndeterminate = true;
                Debug.WriteLine("Observer channel :"+Constants.MyChannel);
                sent = sent + "," + Constants.MyChannel;
                proxy.InsertInfoObserverCompleted += new EventHandler<InsertInfoObserverCompletedEventArgs>(proxy_InsertInfoObserverCompleted);
                proxy.InsertInfoObserverAsync(sent);

            }
            else
            {
                prog.IsIndeterminate = false;
                loading.Visibility = Visibility.Collapsed;
                Microsoft.Xna.Framework.GamerServices.Guide.BeginShowMessageBox("Error", "email id already exists", new string[] { "ok" }, 0, Microsoft.Xna.Framework.GamerServices.MessageBoxIcon.Alert, null, null);
         //       progress_bar.IsIndeterminate = false;

            }
        }
        void proxy_InsertInfoObserverCompleted(object sender, InsertInfoObserverCompletedEventArgs e)
        {
            prog.IsIndeterminate = false;
            loading.Visibility = Visibility.Collapsed;
            Debug.WriteLine(e.Result);
            if (e.Result==true)
            {
                Debug.WriteLine("signed up");
                NavigationService.Navigate(new Uri("/Observer.xaml?Email=" + Email, UriKind.Relative));
            }
            else
                Microsoft.Xna.Framework.GamerServices.Guide.BeginShowMessageBox("Error", "error while signing up", new string[] { "ok" }, 0, Microsoft.Xna.Framework.GamerServices.MessageBoxIcon.Alert, null, null);
        }
        public static bool IsValidEmail(string strIn)
        {
            // Return true if strIn is in valid e-mail format.
            return Regex.IsMatch(strIn,
                   @"^(?("")("".+?""@)|(([0-9a-zA-Z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-zA-Z])@))" +
                   @"(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-zA-Z][-\w]*[0-9a-zA-Z]\.)+[a-zA-Z]{2,6}))$");
        }
    }
}