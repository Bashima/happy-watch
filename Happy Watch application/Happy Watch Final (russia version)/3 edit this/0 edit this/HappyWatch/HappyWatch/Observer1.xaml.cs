﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using HappyWatch.Resources;
using System.Diagnostics;
using Microsoft.Xna.Framework;
using HappyWatch.HWwcf;
using System.Threading;
using System.ComponentModel;

namespace HappyWatch
{
    interface UIUpdateDelegateOb
    {
        void UpdateWithText(string text);
        void UpdateWithInfos(string bpm, string time, string location);
        void UpdateBPM(string bpm);
        void UpdateTime(string time);
        void UpdateLocation(string location);
        void UpdateCON(string loc);
    }
    public partial class Observer1 : PhoneApplicationPage,UIUpdateDelegateOb
    {
        bool pop_up_flag = false;
        public HappyWatchServiceClient proxy;
        public string[] EmailArray=new string[5];
        public string EmailCon;
        public string Email;
        int prog_count = 0;
        public void UpdateWithText(string text)
        {
            /////empty
        }
        public void UpdateLocation(string location)
        {
            try
            {
                Dispatcher.BeginInvoke(() =>
                {
                    data.LocData.Location = (location == null ? "N/A" : location.Trim());
                }
                );

            }
            catch (Exception ex)
            {
                Debug.WriteLine("OnLocationUpdate:" + ex.Message);
            }
            prog_count++;
        //    if (prog_count == 4)
          //      progress_bar.IsIndeterminate = false;
        }
        public void UpdateTime(string time)
        {
            try
            {
                Dispatcher.BeginInvoke(() =>
                {
                    data.Info.Time = time.Trim();
                }
                );

            }
            catch (Exception ex)
            {
                Debug.WriteLine("OnTimeUpdate:" + ex.Message);
            }
            prog_count++;
            //if (prog_count == 4)
              //  progress_bar.IsIndeterminate = false;
        }
        public void UpdateBPM(string bpm)
        {
            try
            {
                Dispatcher.BeginInvoke(() =>
                {
                    data.Info.BPMRate = bpm.Trim();
                }
                );

            }
            catch (Exception ex)
            {
                Debug.WriteLine("OnBMPUpdate:" + ex.Message);
            }
            prog_count++;
           // if (prog_count == 4)
             //   progress_bar.IsIndeterminate = false;
        }
        public void UpdateCON(string loc)
        {
            try
            {
                Dispatcher.BeginInvoke(() =>
                {
                    data.ConData.Condition = loc.Trim();
                }
                );

            }
            catch (Exception ex)
            {
                Debug.WriteLine("OnLOCupdate:" + ex.Message);
            }
            prog_count++;
            //if (prog_count == 4)
              //  progress_bar.IsIndeterminate = false;
        }
        /// <summary>
        /// Update UI from LiveThread;using delegate.
        /// </summary>
        /// <param name="bpm"></param>
        /// <param name="time"></param>
        /// <param name="location"></param>
        /// 
        public void UpdateWithInfos(string bpm, string time, string location)
        {
            //empty
        }


        GridData data = new GridData();
        
        public Observer1()
        {
            InitializeComponent();
            progress_bar.IsIndeterminate = true;
            proxy = new HappyWatchServiceClient("BasicHttpBinding_IHappyWatchService");
            data.Info = new BPMInfo();
            data.ConData = new ConditionData();

            data.LocData = new LocationData();

            
            /////thik kora lagbe
           // this.MainGrid.DataContext = data;
            this.data_grid.DataContext = data;

             

        }

        protected override void OnNavigatedTo(System.Windows.Navigation.NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            var title = NavigationContext.QueryString["Email"];
            EmailCon = (String)title;
            EmailArray = EmailCon.Split(',');
            Email = EmailArray[0];
            Debug.WriteLine("email: " + Email);
            Name_Title.Text = EmailArray[1];
            LiveDataThread liv = new LiveDataThread(Email, this);
            LiveDataThread.flag = true;
            Thread th = new Thread(new ThreadStart(liv.Beta));

            th.Start();

        }
        private void Panorama_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            switch (((Panorama)sender).SelectedIndex)
            {
                case 0: // defines the first PanoramaItem
                    //live data
                    progress_bar.IsIndeterminate = true;
                    break;

                case 1: // second one
                    //daily monthly
                    progress_bar.IsIndeterminate = true;
                    datacall();
                    break;

                case 2:

                    ////graph
                    progress_bar.IsIndeterminate = true;
                    graphCall();
                    break;


                case 3:
                    ///info
                    ///
                    progress_bar.IsIndeterminate = true;
                    infocall();
                    break;

            }
        }



        /// <summary>
        /// //liva data page
        /// </summary>
        class LiveDataThread
        {
            public static string loc { get; set; }
            public static string Email { get; set; }
            public static string BP { get; set; }
            public static bool flag = true;
            public static int count = 0;
            //  HealthDataServiceClient proxy = new HealthDataServiceClient("BasicHttpBinding_IHealthDataService");
            HappyWatchServiceClient proxy = new HappyWatchServiceClient("BasicHttpBinding_IHappyWatchService");

            UIUpdateDelegateOb dlgt;
            public LiveDataThread(string email, UIUpdateDelegateOb del)
            {
                Debug.WriteLine("LIVE THREAD" + email);
                Email = email;
                dlgt = del;

            }

            private void proxy_GetLocationCompleted(object sender, GetLocationCompletedEventArgs e)
            {

                Debug.WriteLine("LOCATION:   " + e.Result);
                loc = e.Result.ToString();
                
                dlgt.UpdateLocation(LiveDataThread.loc);


            }

            private void proxy_GetPulseCompleted(object sender, GetPulseCompletedEventArgs e)
            {
                Debug.WriteLine("LIVEDATA::::    " + e.Result);
                BP = e.Result;
                if (e.Result.Equals("N/A"))
                {
                    dlgt.UpdateBPM("N/A");
                    dlgt.UpdateCON("N/A");

                }
                else
                {

                    Constants.hp1 = Convert.ToDouble(BP);
                    dlgt.UpdateBPM(LiveDataThread.BP);
                    if (Constants.hp1 < 55)
                    {

                        dlgt.UpdateCON("low");
                         //recom_text.Text = "1.Hypertension\n2. Congenital heart defect\n3. Electrolytes imbalance\n4. Haemochromatosis\n5. Heart block(first degree , second degree complete)\n6. Sinus nodal problem\n7. Hypothyroidism\n8. Obstructive sleep apnea\n9. Medication";

                    }
                    else if (Constants.hp1 >= 76)
                    {
                        dlgt.UpdateCON("high");
                        //recom_text.Text = "1.Fever\n2.Anaemia\n3.Dehydration\n4.Excitment\n5.Congenital heart disease\n6.Atrial fibrillation\n7.Shock(Cardiogenic, Hypovolumic,Septic)";

                    }
                    else
                    {
                        dlgt.UpdateCON("average");
                        //recom_text.Text = "you are fit enough but keep track";

                    }
                }
            }
            private void proxy_GetTimeCompleted(object sender, GetTimeCompletedEventArgs e)
            {
                if (e.Result.Equals("N/A"))
                {
                    dlgt.UpdateTime("N/A");

                }
                else
                {
                    string[] t = new string[3];
                    t = e.Result.Split(' ');

                    Debug.WriteLine("TIME trimmed:  " + t[1]);

                    dlgt.UpdateTime(e.Result);
                }
            }



            public void Beta()
            {
                while (true)
                {

                    if (flag == false)
                    {
                        Thread.Sleep(100000);
                    }

                    Debug.WriteLine("ID: " + Email);
                    try
                    {
                        proxy.GetPulseCompleted += new EventHandler<GetPulseCompletedEventArgs>(proxy_GetPulseCompleted);
                        proxy.GetPulseAsync(Email);

                        proxy.GetLocationCompleted += new EventHandler<GetLocationCompletedEventArgs>(proxy_GetLocationCompleted);
                        proxy.GetLocationAsync(Email);

                        proxy.GetTimeCompleted += new EventHandler<GetTimeCompletedEventArgs>(proxy_GetTimeCompleted);
                        proxy.GetTimeAsync(Email);


                    }
                    catch (Exception ex)
                    {
                        Debug.WriteLine("Exception in Thread:" + ex.Message);
                    }



                    Thread.Sleep(60000);
                }
            }
        }


        class GridData : INotifyPropertyChanged
        {
            private BPMInfo _info;
            private LocationData _location;
            private ConditionData _condition;
            public BPMInfo Info
            {
                get { return _info; }
                set
                {
                    _info = value;
                    NotifyPropertyChanged("Info");
                }
            }
            public LocationData LocData
            {
                get { return _location; }
                set
                {
                    _location = value;
                    NotifyPropertyChanged("LocData");
                }
            }
            public ConditionData ConData
            {
                get { return _condition; }
                set
                {
                    _condition = value;
                    NotifyPropertyChanged("ConData");
                }
            }
            // Declare the PropertyChanged event.
            public event PropertyChangedEventHandler PropertyChanged;
            // NotifyPropertyChanged will raise  the PropertyChanged event, 
            // passing the source property that is being updated.
            public void NotifyPropertyChanged(string propertyName)
            {
                if (PropertyChanged != null)
                {
                    PropertyChanged(this,
                        new PropertyChangedEventArgs(propertyName));
                }
            }
        }
        class BPMInfo : INotifyPropertyChanged
        {
            private string _bpm;
            private string _time;
            public string BPMRate
            {
                get
                {
                    return _bpm;
                }
                set
                {
                    _bpm = value;
                    NotifyPropertyChanged("BPMRate");
                }

            }
            public string Time
            {
                get
                {
                    return _time;
                }
                set
                {
                    _time = value;
                    NotifyPropertyChanged("Time");
                }
            }
            // Declare the PropertyChanged event.
            public event PropertyChangedEventHandler PropertyChanged;
            // NotifyPropertyChanged will raise the PropertyChanged event, 
            // passing the source property that is being updated.
            public void NotifyPropertyChanged(string propertyName)
            {
                if (PropertyChanged != null)
                {
                    PropertyChanged(this,
                        new PropertyChangedEventArgs(propertyName));
                }
            }
        }
        class LocationData : INotifyPropertyChanged
        {
            private string _loc;
            public string Location
            {
                get
                {
                    return _loc;
                }
                set
                {
                    _loc = value;
                    NotifyPropertyChanged("Location");
                }
            }
            // Declare the PropertyChanged event.
            public event PropertyChangedEventHandler PropertyChanged;
            // NotifyPropertyChanged will raise the PropertyChanged event, 
            // passing the source property that is being updated.
            public void NotifyPropertyChanged(string propertyName)
            {
                if (PropertyChanged != null)
                {
                    PropertyChanged(this,
                        new PropertyChangedEventArgs(propertyName));
                }
            }
        }
        class ConditionData : INotifyPropertyChanged
        {
            private string _cond;
            public string Condition
            {
                get
                {
                    return _cond;
                }
                set
                {
                    _cond = value;
                    NotifyPropertyChanged("Condition");
                }
            }
            // Declare the PropertyChanged event.
            public event PropertyChangedEventHandler PropertyChanged;
            // NotifyPropertyChanged will raise the PropertyChanged event, 
            // passing the source property that is being updated.
            public void NotifyPropertyChanged(string propertyName)
            {
                if (PropertyChanged != null)
                {
                    PropertyChanged(this,
                        new PropertyChangedEventArgs(propertyName));
                }
            }
        }



        ////////////graph
        int count = 0, count1 = 0;
        bool flag;
        double hp;
        List<string> str, str1;
       
        List<Person> graph1;
        public void graphCall()
        {

            HappyWatchServiceClient proxy = new HappyWatchServiceClient("BasicHttpBinding_IHappyWatchService");
            proxy.FindPulseListCompleted += new EventHandler<FindPulseListCompletedEventArgs>(proxy_FindPulseListCompleted);
            proxy.FindPulseListAsync(Email);

        }
        void proxy_FindPulseListCompleted(object sender, FindPulseListCompletedEventArgs es)
        {

            Debug.WriteLine("DeviceId 2nd: " + Email);
            // Debug.WriteLine("graph here::  "+es.Result);
            count = 0;

            str = es.Result.ToList();

            flag = true;
            Debug.WriteLine("flag1: " + flag);
            foreach (string x in str)
            {
                Debug.WriteLine("flag2: " + flag);

                Constants.Bloodpulse[count++] = x;

                Debug.WriteLine(Constants.Bloodpulse[count - 1]);

                if (count == 60)
                {
                    break;
                }

            }
            flag = false;
            Debug.WriteLine("flag1: " + flag);
            Constants.CON = count;
            makeGraph();

        }
        public void makeGraph()
        {
            graph1 = new List<Person>();
            Constants.c = 1;
            Debug.WriteLine("CON: " + Constants.CON);
            for (int i = 0; i < Constants.CON; i++)
            {
                Debug.WriteLine("Persons: ");
                graph1.Add(new Person { time = (Constants.c).ToString(), bloodpulse = int.Parse(Constants.Bloodpulse[i]) });
                Constants.c += 1;
                // Debug.WriteLine("matai: "+ " " + Class1.Time[i].ToString());


            }
            try
            {
                Constants.CON = 0;
                mychart.DataSource = graph1;
            }

            catch (Exception ex)
            {
                Debug.WriteLine("erroor" + ex);

            }

        }

        ////////////////////////////////daily monthly

        void datacall()//connecting to the wcf to get info
        {

            // HealthDataServiceClient proxy = new HealthDataServiceClient("BasicHttpBinding_IHealthDataService");
            string datasentdaily, datasentmonthly, datasent, datamonth;
            DateTime date = DateTime.Now;
            datasent = "";
            datamonth = "";
            datasentdaily = Email + "," + string.Format("{0:yyyy-MM-dd}", date);
            datasentmonthly = Email + "," + string.Format("{0:yyyy-MM-dd}", date);
            datasent = datasentdaily + ",daily";
            datamonth = datasentmonthly + ",monthly";
            Debug.WriteLine("datamonth: " + datamonth);
            proxy.FindPulseMaxCompleted += new EventHandler<FindPulseMaxCompletedEventArgs>(proxy_FindPulseMaxCompleted);
            proxy.FindPulseMaxAsync(Email);

            proxy.FindPulseMinCompleted += new EventHandler<FindPulseMinCompletedEventArgs>(proxy_FindPulseMinCompleted);
            proxy.FindPulseMinAsync(Email);

            proxy.FindPulseAvCompleted += new EventHandler<FindPulseAvCompletedEventArgs>(proxy_FindPulseAvCompleted);
            proxy.FindPulseAvAsync(Email);

            proxy.FindPulseMaxMonthCompleted += new EventHandler<FindPulseMaxMonthCompletedEventArgs>(proxy_FindPulseMaxMonthCompleted);
            proxy.FindPulseMaxMonthAsync(Email);

            proxy.FindPulseMinMonthCompleted += new EventHandler<FindPulseMinMonthCompletedEventArgs>(proxy_FindPulseMinMonthCompleted);
            proxy.FindPulseMinMonthAsync(Email);

            proxy.FindPulseAvMonthCompleted += new EventHandler<FindPulseAvMonthCompletedEventArgs>(proxy_FindPulseAvMonthCompleted);
            proxy.FindPulseAvMonthAsync(Email);

        }
        void proxy_FindPulseMaxCompleted(object sender, FindPulseMaxCompletedEventArgs e)
        {
            if (e.Result.Equals("N/A"))
            {
                DailyMax.Text = "N/A";
                Debug.WriteLine("maxxxxxxxxxxxx:           " + e.Result);
            }
            else
            {
                DailyMax.Text = e.Result;
                Debug.WriteLine("maxxxxxxxxxxxx:           " + e.Result);
            }
        }
        void proxy_FindPulseMinCompleted(object sender, FindPulseMinCompletedEventArgs e)
        {
            if (e.Result.Equals("NULL"))
            {
                DailyMin.Text = "N/A";
            }
            else
            {
                DailyMin.Text = e.Result;
                Debug.WriteLine("min: " + e.Result);
            }
        }
        void proxy_FindPulseAvCompleted(object sender, FindPulseAvCompletedEventArgs e)
        {
            if (e.Result.Equals("N/A"))
            {
                DailyAv.Text = "N/A";
            }
            else
            {
                DailyAv.Text = e.Result;
                Debug.WriteLine("AVERAGE: " + e.Result);
            }
        }
        void proxy_FindPulseMaxMonthCompleted(object sender, FindPulseMaxMonthCompletedEventArgs e)
        {
            if (e.Result.Equals("N/A"))
            {
                MonthlyMax.Text = "N/A";
            }
            else
            {
                MonthlyMax.Text = e.Result;
                Debug.WriteLine("maxxxxxxxxxxxx:           " + e.Result);
            }
        }
        void proxy_FindPulseMinMonthCompleted(object sender, FindPulseMinMonthCompletedEventArgs e)
        {
            if (e.Result.Equals("N/A"))
            {
                MonthlyMin.Text = "N/A";
            }
            else
            {
                MonthlyMin.Text = e.Result;
                Debug.WriteLine("min: " + e.Result);
            }
        }
        void proxy_FindPulseAvMonthCompleted(object sender, FindPulseAvMonthCompletedEventArgs e)
        {
            if (e.Result.Equals("N/A"))
            {
                MonthlyAv.Text = "N/A";
            }
            else
            {
                MonthlyAv.Text = e.Result;
                Debug.WriteLine("AVERAGE: " + e.Result);
            }
        }
        //////////user info
        void infocall()
        {
            proxy.PatientInfoCompleted += new EventHandler<PatientInfoCompletedEventArgs>(proxy_PatientInfoCompleted);
            proxy.PatientInfoAsync(Email);
        }

        void proxy_PatientInfoCompleted(object sender, PatientInfoCompletedEventArgs e)
        {
            //////do some coding
            Debug.WriteLine("Infos: " + e.Result);
            string[] infos = new string[20];
            infos = e.Result.ToString().Split('#');
            Name_text.Text = infos[0];
            Email_text.Text = infos[1];
            Date_text.Text = infos[2].Trim();
            Gender_text.Text=infos[3];
            Contact_text.Text=infos[4];
            Condition_text.Text = infos[6];
            

        }

        private void recommendation(object sender, System.Windows.Input.GestureEventArgs e)
        {
        	//this.recom_in.Begin();
            //pop_up_flag = true;// TODO: Add event handler implementation here.
        }

        private void recommendation_done_clicked(object sender, System.Windows.RoutedEventArgs e)
        {
        	this.recom_out.Begin();
            pop_up_flag = false;
			// TODO: Add event handler implementation here.
        }
        protected override void OnBackKeyPress(System.ComponentModel.CancelEventArgs e)
        {
            if (pop_up_flag == true)
            {
                this.recom_out.Begin();
                pop_up_flag=false;
                e.Cancel = true;
            }
        }



    }
}