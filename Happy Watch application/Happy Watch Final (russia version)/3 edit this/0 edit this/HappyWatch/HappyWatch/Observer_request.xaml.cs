﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.Diagnostics;
using HappyWatch.HWwcf;

namespace HappyWatch
{
    public partial class Observer_request : PhoneApplicationPage
    {
        public string Email { get; set; }
        public HappyWatchServiceClient proxy;
        public Observer_request()
        {
            InitializeComponent();
            proxy = new HappyWatchServiceClient("BasicHttpBinding_IHappyWatchService");
            progress_bar.IsIndeterminate = true;
        }
        protected override void OnNavigatedTo(System.Windows.Navigation.NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            var title = NavigationContext.QueryString["Email"];
            Email = title;
            proxy = new HappyWatchServiceClient("BasicHttpBinding_IHappyWatchService");
            proxy.GetObNameCompleted += new EventHandler<GetObNameCompletedEventArgs>(proxy_GetObNameCompleted);
            proxy.GetObNameAsync(Email);
            MakeReqList();
        }

        void proxy_GetObNameCompleted(object sender, GetObNameCompletedEventArgs e)
        {
            Name_Title.Text = e.Result;
        }
        public void MakeReqList()
        {
            progress_bar.IsIndeterminate = true;   
            proxy.ObserverVsPatientRequestCompleted += new EventHandler<ObserverVsPatientRequestCompletedEventArgs>(proxy_ObserverVsPatientRequestCompleted);
            proxy.ObserverVsPatientRequestAsync(Email);
            
        }

        void proxy_ObserverVsPatientRequestCompleted(object sender, ObserverVsPatientRequestCompletedEventArgs e)
        {
            Debug.WriteLine("request list:");
            Debug.WriteLine(Email);
            List<request> req = new List<request>();
            foreach (ObPatient o in e.Result.ToList())
            {
                request temp = new request();
                Debug.WriteLine("name: "+o.pName);
                temp.name = o.pName;
                temp.mail = o.pMail;
                req.Add(temp);
            }
            AddList.ItemsSource = req.ToList();
            progress_bar.IsIndeterminate = false;
            
        }
		private void tap_deny_add(object sender, System.Windows.Input.GestureEventArgs e)
        {
            progress_bar.IsIndeterminate = true;
            string TargetName = (string)((Button)sender).Tag;
            proxy.RejecttPatientReqCompleted += new EventHandler<RejecttPatientReqCompletedEventArgs>(proxy_RejecttPatientReqCompleted);
            string sent = Email + "#" + TargetName;
            Debug.WriteLine("Sending: "+sent);
            proxy.RejecttPatientReqAsync(sent);
           
		}

        void proxy_RejecttPatientReqCompleted(object sender, RejecttPatientReqCompletedEventArgs e)
        {
            Debug.WriteLine("result is: "+e.Result);
            if (e.Result == true)
            {
                MessageBox.Show("request rejected");
            }
            else
            {
                MessageBox.Show("error while rejecting request");
            }
            MakeReqList();
            progress_bar.IsIndeterminate = false;
        }
		private void tap_accept_add(object sender, System.Windows.Input.GestureEventArgs e)
        {
            progress_bar.IsIndeterminate = true;
            string TargetName = (string)((Button)sender).Tag;
            
            proxy.AcceptPatientReqCompleted += new EventHandler<AcceptPatientReqCompletedEventArgs>(proxy_AcceptPatientReqCompleted);
            string sent = Email + "#" + TargetName;
            Debug.WriteLine("Sending: " + sent);
            proxy.AcceptPatientReqAsync(sent);
		}

        void proxy_AcceptPatientReqCompleted(object sender, AcceptPatientReqCompletedEventArgs e)
        {
            Debug.WriteLine("result is: " + e.Result);
            if (e.Result == true)
            {
                MessageBox.Show("request accepted");
            }
            else
            {
                MessageBox.Show("error while accepting request");
            }
            MakeReqList();
            progress_bar.IsIndeterminate = false;
        }

        public class request
        {
            public string name { get; set; }
            public string mail { get; set; }

        }

    }
    
    
}