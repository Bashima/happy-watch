﻿#pragma checksum "D:\Dropbox\implement code\HappyWatch\HappyWatch\StartPage.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "22D7F8DC775356E2BE3FC3E4F4902338"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.18033
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using Microsoft.Phone.Controls;
using System;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Automation.Peers;
using System.Windows.Automation.Provider;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Resources;
using System.Windows.Shapes;
using System.Windows.Threading;


namespace HappyWatch {
    
    
    public partial class MainPage : Microsoft.Phone.Controls.PhoneApplicationPage {
        
        internal System.Windows.Media.Animation.Storyboard signin;
        
        internal System.Windows.Media.Animation.Storyboard signup;
        
        internal System.Windows.Media.Animation.Storyboard signup_go;
        
        internal System.Windows.Media.Animation.Storyboard warning_in;
        
        internal System.Windows.Media.Animation.Storyboard warning_out;
        
        internal System.Windows.Controls.Grid RootLayout;
        
        internal System.Windows.Controls.Grid log_in_grid;
        
        internal Microsoft.Phone.Controls.PhoneTextBox device_id_box;
        
        internal System.Windows.Controls.Grid progress_bar;
        
        internal System.Windows.Controls.ProgressBar progressBar;
        
        internal System.Windows.Controls.ProgressBar progressBar_Copy;
        
        internal System.Windows.Controls.ProgressBar progressBar_Copy1;
        
        internal System.Windows.Controls.Grid sign_up_grid1;
        
        internal System.Windows.Controls.Button button;
        
        internal System.Windows.Controls.Button button1;
        
        internal System.Windows.Controls.Grid warning_box;
        
        internal System.Windows.Controls.TextBlock warning_text;
        
        internal System.Windows.Controls.Button Ok_button;
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Windows.Application.LoadComponent(this, new System.Uri("/HappyWatch;component/StartPage.xaml", System.UriKind.Relative));
            this.signin = ((System.Windows.Media.Animation.Storyboard)(this.FindName("signin")));
            this.signup = ((System.Windows.Media.Animation.Storyboard)(this.FindName("signup")));
            this.signup_go = ((System.Windows.Media.Animation.Storyboard)(this.FindName("signup_go")));
            this.warning_in = ((System.Windows.Media.Animation.Storyboard)(this.FindName("warning_in")));
            this.warning_out = ((System.Windows.Media.Animation.Storyboard)(this.FindName("warning_out")));
            this.RootLayout = ((System.Windows.Controls.Grid)(this.FindName("RootLayout")));
            this.log_in_grid = ((System.Windows.Controls.Grid)(this.FindName("log_in_grid")));
            this.device_id_box = ((Microsoft.Phone.Controls.PhoneTextBox)(this.FindName("device_id_box")));
            this.progress_bar = ((System.Windows.Controls.Grid)(this.FindName("progress_bar")));
            this.progressBar = ((System.Windows.Controls.ProgressBar)(this.FindName("progressBar")));
            this.progressBar_Copy = ((System.Windows.Controls.ProgressBar)(this.FindName("progressBar_Copy")));
            this.progressBar_Copy1 = ((System.Windows.Controls.ProgressBar)(this.FindName("progressBar_Copy1")));
            this.sign_up_grid1 = ((System.Windows.Controls.Grid)(this.FindName("sign_up_grid1")));
            this.button = ((System.Windows.Controls.Button)(this.FindName("button")));
            this.button1 = ((System.Windows.Controls.Button)(this.FindName("button1")));
            this.warning_box = ((System.Windows.Controls.Grid)(this.FindName("warning_box")));
            this.warning_text = ((System.Windows.Controls.TextBlock)(this.FindName("warning_text")));
            this.Ok_button = ((System.Windows.Controls.Button)(this.FindName("Ok_button")));
        }
    }
}

