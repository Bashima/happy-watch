﻿#pragma checksum "E:\final\HappyWatch\HappyWatch\Admin_Main_Page.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "EB8C1EFA92831B8547EF977C7837DDDA"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.18010
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using Microsoft.Phone.Controls;
using System;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Automation.Peers;
using System.Windows.Automation.Provider;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Resources;
using System.Windows.Shapes;
using System.Windows.Threading;


namespace HappyWatch {
    
    
    public partial class Admin_Main_Page : Microsoft.Phone.Controls.PhoneApplicationPage {
        
        internal System.Windows.Media.Animation.Storyboard update_in;
        
        internal System.Windows.Media.Animation.Storyboard update_out;
        
        internal System.Windows.Controls.Grid LayoutRoot;
        
        internal Microsoft.Phone.Controls.Panorama panorama;
        
        internal System.Windows.Controls.Grid MainGrid;
        
        internal System.Windows.Controls.TextBlock hospital_name;
        
        internal System.Windows.Controls.TextBlock Hospital_Name_text;
        
        internal System.Windows.Controls.TextBlock Email_text;
        
        internal System.Windows.Controls.TextBlock Address_text;
        
        internal System.Windows.Controls.TextBlock Doctor_No_text;
        
        internal System.Windows.Controls.TextBlock Patient_No_text;
        
        internal System.Windows.Controls.Border update_border;
        
        internal System.Windows.Controls.Grid update_grid;
        
        internal Microsoft.Phone.Controls.PhoneTextBox email_box_up;
        
        internal Microsoft.Phone.Controls.PhoneTextBox address_box_up;
        
        internal Microsoft.Phone.Controls.PhoneTextBox password_box_up;
        
        internal Microsoft.Phone.Controls.PhoneTextBox re_password_box_up;
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Windows.Application.LoadComponent(this, new System.Uri("/HappyWatch;component/Admin_Main_Page.xaml", System.UriKind.Relative));
            this.update_in = ((System.Windows.Media.Animation.Storyboard)(this.FindName("update_in")));
            this.update_out = ((System.Windows.Media.Animation.Storyboard)(this.FindName("update_out")));
            this.LayoutRoot = ((System.Windows.Controls.Grid)(this.FindName("LayoutRoot")));
            this.panorama = ((Microsoft.Phone.Controls.Panorama)(this.FindName("panorama")));
            this.MainGrid = ((System.Windows.Controls.Grid)(this.FindName("MainGrid")));
            this.hospital_name = ((System.Windows.Controls.TextBlock)(this.FindName("hospital_name")));
            this.Hospital_Name_text = ((System.Windows.Controls.TextBlock)(this.FindName("Hospital_Name_text")));
            this.Email_text = ((System.Windows.Controls.TextBlock)(this.FindName("Email_text")));
            this.Address_text = ((System.Windows.Controls.TextBlock)(this.FindName("Address_text")));
            this.Doctor_No_text = ((System.Windows.Controls.TextBlock)(this.FindName("Doctor_No_text")));
            this.Patient_No_text = ((System.Windows.Controls.TextBlock)(this.FindName("Patient_No_text")));
            this.update_border = ((System.Windows.Controls.Border)(this.FindName("update_border")));
            this.update_grid = ((System.Windows.Controls.Grid)(this.FindName("update_grid")));
            this.email_box_up = ((Microsoft.Phone.Controls.PhoneTextBox)(this.FindName("email_box_up")));
            this.address_box_up = ((Microsoft.Phone.Controls.PhoneTextBox)(this.FindName("address_box_up")));
            this.password_box_up = ((Microsoft.Phone.Controls.PhoneTextBox)(this.FindName("password_box_up")));
            this.re_password_box_up = ((Microsoft.Phone.Controls.PhoneTextBox)(this.FindName("re_password_box_up")));
        }
    }
}

