﻿#pragma checksum "F:\Dropbox\4 edit this\3 edit this\0 edit this\HappyWatch\HappyWatch\DataPage.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "1487C316BBC8BC9A31F962C3415C06ED"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.18331
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using Microsoft.Phone.Controls;
using System;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Automation.Peers;
using System.Windows.Automation.Provider;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Resources;
using System.Windows.Shapes;
using System.Windows.Threading;


namespace HappyWatch {
    
    
    public partial class DataPage : Microsoft.Phone.Controls.PhoneApplicationPage {
        
        internal System.Windows.Controls.Grid LayoutRoot;
        
        internal System.Windows.Controls.Grid appname;
        
        internal System.Windows.Controls.Grid ContentPanel;
        
        internal System.Windows.Controls.TextBlock DailyAv;
        
        internal System.Windows.Controls.TextBlock DailyMax;
        
        internal System.Windows.Controls.TextBlock DailyMin;
        
        internal System.Windows.Controls.TextBlock MonthlyAv;
        
        internal System.Windows.Controls.TextBlock MonthlyMax;
        
        internal System.Windows.Controls.TextBlock MonthlyMin;
        
        internal System.Windows.Controls.ProgressBar progress_bar;
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Windows.Application.LoadComponent(this, new System.Uri("/HappyWatch;component/DataPage.xaml", System.UriKind.Relative));
            this.LayoutRoot = ((System.Windows.Controls.Grid)(this.FindName("LayoutRoot")));
            this.appname = ((System.Windows.Controls.Grid)(this.FindName("appname")));
            this.ContentPanel = ((System.Windows.Controls.Grid)(this.FindName("ContentPanel")));
            this.DailyAv = ((System.Windows.Controls.TextBlock)(this.FindName("DailyAv")));
            this.DailyMax = ((System.Windows.Controls.TextBlock)(this.FindName("DailyMax")));
            this.DailyMin = ((System.Windows.Controls.TextBlock)(this.FindName("DailyMin")));
            this.MonthlyAv = ((System.Windows.Controls.TextBlock)(this.FindName("MonthlyAv")));
            this.MonthlyMax = ((System.Windows.Controls.TextBlock)(this.FindName("MonthlyMax")));
            this.MonthlyMin = ((System.Windows.Controls.TextBlock)(this.FindName("MonthlyMin")));
            this.progress_bar = ((System.Windows.Controls.ProgressBar)(this.FindName("progress_bar")));
        }
    }
}

