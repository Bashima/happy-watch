﻿#pragma checksum "F:\Dropbox\4 edit this\3 edit this\0 edit this\HappyWatch\HappyWatch\Admin_Doctor.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "7FFA635F316A3D5992B7E058440CD5B1"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.18010
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using Microsoft.Phone.Controls;
using System;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Automation.Peers;
using System.Windows.Automation.Provider;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Resources;
using System.Windows.Shapes;
using System.Windows.Threading;


namespace HappyWatch {
    
    
    public partial class Admin_Doctor : Microsoft.Phone.Controls.PhoneApplicationPage {
        
        internal System.Windows.Controls.Grid LayoutRoot;
        
        internal System.Windows.Controls.Grid ContentPanel;
        
        internal System.Windows.Controls.ListBox peoplesList;
        
        internal System.Windows.Controls.Grid appname_Copy;
        
        internal System.Windows.Controls.TextBlock No_Doctors;
        
        internal System.Windows.Controls.Grid appname;
        
        internal System.Windows.Controls.ProgressBar progress_bar;
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Windows.Application.LoadComponent(this, new System.Uri("/HappyWatch;component/Admin_Doctor.xaml", System.UriKind.Relative));
            this.LayoutRoot = ((System.Windows.Controls.Grid)(this.FindName("LayoutRoot")));
            this.ContentPanel = ((System.Windows.Controls.Grid)(this.FindName("ContentPanel")));
            this.peoplesList = ((System.Windows.Controls.ListBox)(this.FindName("peoplesList")));
            this.appname_Copy = ((System.Windows.Controls.Grid)(this.FindName("appname_Copy")));
            this.No_Doctors = ((System.Windows.Controls.TextBlock)(this.FindName("No_Doctors")));
            this.appname = ((System.Windows.Controls.Grid)(this.FindName("appname")));
            this.progress_bar = ((System.Windows.Controls.ProgressBar)(this.FindName("progress_bar")));
        }
    }
}

