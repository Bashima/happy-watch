﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using HappyWatch.HealthDataService;
using System.Diagnostics;

namespace HappyWatch
{
    public partial class graph_page : PhoneApplicationPage
    {
        int count = 0, count1 = 0;
        bool flag;
        double hp;
        List<string> str, str1;
        string DeviceId;
        List<Person> graph1;
        protected override void OnNavigatedTo(System.Windows.Navigation.NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            var title = NavigationContext.QueryString["DeviceId"];

            DeviceId = (String)title;
            Debug.WriteLine("DeviceId: " + DeviceId);
            graphCall();


        }
        public void graphCall()
        {

            HealthDataServiceClient proxy1 = new HealthDataServiceClient("BasicHttpBinding_IHealthDataService");
            proxy1.FindPulseListCompleted += new EventHandler<FindPulseListCompletedEventArgs>(proxy_FindPulseListCompleted);
            proxy1.FindPulseListAsync(DeviceId);

        }


        void proxy_FindPulseListCompleted(object sender, FindPulseListCompletedEventArgs es)
        {

            Debug.WriteLine("DeviceId 2nd: " + DeviceId);
            // Debug.WriteLine("graph here::  "+es.Result);
            count = 0;

            str = es.Result.ToList();

            flag = true;
            Debug.WriteLine("flag1: " + flag);
            foreach (string x in str)
            {
                Debug.WriteLine("flag2: " + flag);

                Class1.Bloodpulse[count++] = x;

                Debug.WriteLine(Class1.Bloodpulse[count - 1]);

                if (count == 10)
                {
                    break;
                }

            }
            flag = false;
            Debug.WriteLine("flag1: " + flag);
            Class1.CON = count;
            makeGraph();

        }
        public void makeGraph()
        {
            graph1 = new List<Person>();
            Debug.WriteLine("CON: " + Class1.CON);
            for (int i = 0; i < Class1.CON; i++)
            {
                Debug.WriteLine("Persons: ");
                graph1.Add(new Person { time = (Class1.c).ToString(), bloodpulse = int.Parse(Class1.Bloodpulse[i]) });
                Class1.c += 5;
                // Debug.WriteLine("matai: "+ " " + Class1.Time[i].ToString());


            }
            try
            {
                Class1.CON = 0;
                mychart.DataSource = graph1;
            }

            catch (Exception ex)
            {
                Debug.WriteLine("erroor" + ex);

            }

        }

        public graph_page()
        {
            InitializeComponent();


        }
    }
}