﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.IO;
using System.IO.IsolatedStorage;

namespace HappyWatch
{
    public partial class AddPeople : PhoneApplicationPage
    {
        IsolatedStorageFile myIsolatedStorage = IsolatedStorageFile.GetUserStoreForApplication();
        public AddPeople()
        {
            
 
            
            InitializeComponent();
        }

        private void add_button_click(object sender, RoutedEventArgs e)//click done
        {
            if (pName.Text.Equals("") || pNumber.Text.Equals("") || pMail.Text.Equals(""))
            {
                this.warning_in.Begin();
            }
            else
            {
                People aPeople = new People();
                aPeople.name = pName.Text;
                aPeople.phone = pNumber.Text;
                aPeople.email = pMail.Text;
                //   Class1.peopleList.Add(aPeople);
                using (StreamWriter writeFile = new StreamWriter(new IsolatedStorageFileStream("myFile.txt", FileMode.Append, FileAccess.Write, myIsolatedStorage)))
                {
                    string someTextData = pName.Text + "," + pNumber.Text + "," + pMail.Text;
                    writeFile.WriteLine(someTextData);
                    Constants.AddCounter++;
                    writeFile.Close();
                }

                NavigationService.Navigate(new Uri("/PeoplePage.xaml", UriKind.Relative));
            }
        }

        private void Ok_Button_Click(object sender, RoutedEventArgs e)
        {
            this.warn_out.Begin();
        }

    }
}