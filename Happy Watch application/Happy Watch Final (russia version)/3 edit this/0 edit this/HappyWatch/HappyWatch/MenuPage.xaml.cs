﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using HappyWatch.Resources;
using System.Diagnostics;
using Microsoft.Xna.Framework;
using HappyWatch.HWwcf;
using System.Threading;
using System.ComponentModel;
using System.IO.IsolatedStorage;
namespace HappyWatch
{
    
    interface UIUpdateDelegate
    {
        void UpdateWithText(string text);
        void UpdateWithInfos(string bpm,string time,string location);
        void UpdateBPM(string bpm);
        void UpdateTime(string time);
        void UpdateLocation(string location);
        void UpdateCON(string loc);
    }
    public partial class PanoramaPage1 : PhoneApplicationPage, UIUpdateDelegate
    {
        public string Email;
       public  string pCond, pGend;
        int count = 0;
        bool pop_up_flag=false;
        public void UpdateWithText(string text)
        {
            /////empty
        }
        public void UpdateLocation(string location)
        {
            try
            {
                Dispatcher.BeginInvoke(() =>
                {
                    data.LocData.Location = (location==null ? "N/A" : location.Trim());
                    progressbar.IsIndeterminate = false;
                }
                );

            }
            catch (Exception ex)
            {
                Debug.WriteLine("OnLocationUpdate:" + ex.Message);
            }
            count++;
          //  if (count == 6)
            //    progressbar.IsIndeterminate = false;
        }
        public void UpdateTime(string time)
        {
            try
            {
                Dispatcher.BeginInvoke(() =>
                {
                    data.Info.Time = time.Trim();
                }
                );

            }
            catch (Exception ex)
            {
                Debug.WriteLine("OnTimeUpdate:" + ex.Message);
            }
            count++;
            //if (count == 6)
              //  progressbar.IsIndeterminate = false;
        }
        public void UpdateBPM(string bpm)
        {
            try
            {
                Dispatcher.BeginInvoke(() =>
                {
                    data.Info.BPMRate = bpm.Trim();
                }
                );

            }
            catch (Exception ex)
            {
                Debug.WriteLine("OnBMPUpdate:" + ex.Message);
            }
            count++;
           // if (count == 6)
             //   progressbar.IsIndeterminate = false;
        }
        public void UpdateCON(string loc)
        {
            try
            {
                Dispatcher.BeginInvoke(() =>
                {
                    data.ConData.Condition = loc.Trim();
                }
                );

            }
            catch (Exception ex)
            {
                Debug.WriteLine("OnLOCupdate:" + ex.Message);
            }
            count++;
           // if (count == 6)
             //   progressbar.IsIndeterminate = false;
        }
        /// <summary>
        /// Update UI from LiveThread;using delegate.
        /// </summary>
        /// <param name="bpm"></param>
        /// <param name="time"></param>
        /// <param name="location"></param>
        /// 
        public void UpdateWithInfos(string bpm, string time, string location)
        {
            //empty
        }
        
        
        GridData data = new GridData();
        public PanoramaPage1()
        {

            InitializeComponent();
            progressbar.IsIndeterminate = true;
            data.Info = new BPMInfo();
            data.ConData = new ConditionData();

            data.LocData = new LocationData();


           
            this.MainGrid.DataContext = data;


        }
        protected override void OnNavigatedTo(System.Windows.Navigation.NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            var title = NavigationContext.QueryString["Email"];
            Email = (String)title;
            Debug.WriteLine("email: " + Email);
            LiveDataThread liv = new LiveDataThread(Email, this);
            LiveDataThread.flag = true;
            Thread th = new Thread(new ThreadStart(liv.Beta));

            th.Start();

        }

        private void see_recom_button_click(object sender, System.Windows.RoutedEventArgs e)
        {
            //here recom name of textblock recom_text
            progressbar.IsIndeterminate = true;

            pCond = "N/A";
            pGend = "N/A";
            HappyWatchServiceClient clinet = new HappyWatchServiceClient("BasicHttpBinding_IHappyWatchService");
            clinet.PrecriptionInfoCompleted += new EventHandler<PrecriptionInfoCompletedEventArgs>(clinet_PrecriptionInfoCompleted);
            clinet.PrecriptionInfoAsync(Email);

            Debug.WriteLine("Conditions: "+pCond+" "+pGend);
            if (pCond.Equals("pregnant"))
            {
                if (Constants.hp1 < 78)
                {

                    recom_text.Text = "It seems your Blood Pulse rate is lower than normal. You might be suffering from\n1.Hypertension\n2. Congenital heart defect\n3. Electrolytes imbalance\n4. Haemochromatosis\n5. Heart block(first degree , second degree complete)\n6. Sinus nodal problem\n7. Hypothyroidism\n8. Obstructive sleep apnea\n9. Medication";

                }
                else if (Constants.hp1 > 92)
                {

                    recom_text.Text = "It seems your Blood Pulse rate is higher than normal. You might be suffering from\n1.Fever\n2.Anaemia\n3.Dehydration\n4.Excitment\n5.Congenital heart disease\n6.Atrial fibrillation\n7.Shock(Cardiogenic, Hypovolumic,Septic)";

                }
                else
                {

                    recom_text.Text = "your Blood Pulse rate is normal but keep track";

                }

            }
            else
            {
                if (pGend.Equals("male"))
                {
                    if (Constants.hp1 < 55)
                    {

                        recom_text.Text = "It seems your Blood Pulse rate is lower than normal. You might be suffering from\n1.Hypertension\n2. Congenital heart defect\n3. Electrolytes imbalance\n4. Haemochromatosis\n5. Heart block(first degree , second degree complete)\n6. Sinus nodal problem\n7. Hypothyroidism\n8. Obstructive sleep apnea\n9. Medication";

                    }
                    else if (Constants.hp1 > 85)
                    {

                        recom_text.Text = "It seems your Blood Pulse rate is higher than normal. You might be suffering from\n1.Fever\n2.Anaemia\n3.Dehydration\n4.Excitment\n5.Congenital heart disease\n6.Atrial fibrillation\n7.Shock(Cardiogenic, Hypovolumic,Septic)";

                    }
                    else
                    {

                        recom_text.Text = "your Blood Pulse rate is normal but keep track";

                    }
                }
                else
                {
                    if (Constants.hp1 < 60)
                    {

                        recom_text.Text = "It seems your Blood Pulse rate is lower than normal. You might be suffering from\n1.Hypertension\n2. Congenital heart defect\n3. Electrolytes imbalance\n4. Haemochromatosis\n5. Heart block(first degree , second degree complete)\n6. Sinus nodal problem\n7. Hypothyroidism\n8. Obstructive sleep apnea\n9. Medication";

                    }
                    else if (Constants.hp1 > 85)
                    {

                        recom_text.Text = "It seems your Blood Pulse rate is higher than normal. You might be suffering from\n1.Fever\n2.Anaemia\n3.Dehydration\n4.Excitment\n5.Congenital heart disease\n6.Atrial fibrillation\n7.Shock(Cardiogenic, Hypovolumic,Septic)";

                    }
                    else
                    {

                        recom_text.Text = "your Blood Pulse rate is normal but keep track";

                    }
                }

 
            }
            if (Constants.hp1 < 55)
            {

                recom_text.Text = "It seems your Blood Pulse rate is lower than normal. You might be suffering from\n1.Hypertension\n2. Congenital heart defect\n3. Electrolytes imbalance\n4. Haemochromatosis\n5. Heart block(first degree , second degree complete)\n6. Sinus nodal problem\n7. Hypothyroidism\n8. Obstructive sleep apnea\n9. Medication";

            }
            else if (Constants.hp1 >= 76)
            {

                recom_text.Text = "It seems your Blood Pulse rate is higher than normal. You might be suffering from\n1.Fever\n2.Anaemia\n3.Dehydration\n4.Excitment\n5.Congenital heart disease\n6.Atrial fibrillation\n7.Shock(Cardiogenic, Hypovolumic,Septic)";

            }
            else
            {
                
                recom_text.Text = "your Blood Pulse rate is normal but keep track";

            }
            this.popuo_entry_anim.Begin();
            pop_up_flag = true;
            string specialist;
           /////////////////////// ///a proxy here
          //  clinet.GetLastPrescriptionCompleted += new EventHandler<GetLastPrescriptionCompletedEventArgs>(clinet_GetLastPrescriptionCompleted);
            //clinet.GetLastPrescriptionAsync(Email);
            clinet.PatientGetAllPrescriptionsCompleted += new EventHandler<PatientGetAllPrescriptionsCompletedEventArgs>(clinet_PatientGetAllPrescriptionsCompleted);
            clinet.PatientGetAllPrescriptionsAsync(Email);
        }

        void clinet_PrecriptionInfoCompleted(object sender, PrecriptionInfoCompletedEventArgs e)
        {
            string[] ar = new string[10];
            ar = e.Result.Split('#');

            
        }

        void clinet_PatientGetAllPrescriptionsCompleted(object sender, PatientGetAllPrescriptionsCompletedEventArgs e)
        {
            Debug.WriteLine("getting all the prescriptions: ");
            bool flag=false;
            string add="";
            foreach (string s in e.Result.ToList())
            {
                if(s.Length<3)
                {
                    add = "N/A";
                    break;
                }
                
                string[] ar = new string[10];
                ar = s.Split('#');
                
                add+= ar[2]+ "\n" + ar[0] + ": \n" + ar[1] + '\n'+'\n';
            }

            prescription_box.Text = add;
            progressbar.IsIndeterminate = false;

            
        }

     /*   void clinet_GetLastPrescriptionCompleted(object sender, GetLastPrescriptionCompletedEventArgs e)
        {
            Debug.WriteLine("prescription is: "+e.Result);
            if (e.Result.Equals("N/A"))
            {
                prescription_box.Text = "N/A";

            }
            else
            {
                string doctor, prescription;
                string[] ar = new string[10];
                ar = e.Result.ToString().Split('#');
                doctor = ar[0] + ": ";
                prescription = doctor + "\n" + ar[1];
                prescription_box.Text = prescription;
            }
        }*/

        private void recommendation_done_click(object sender, System.Windows.RoutedEventArgs e)
        {
            this.popup_exit_anim.Begin();
            pop_up_flag = false;
        }

        private void people_button_click(object sender, System.Windows.RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri("/Patient_see_Observer.xaml?Email="+Email, UriKind.Relative));

        }

        private void help_button_click(object sender, System.Windows.RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri("/InstructionPage.xaml", UriKind.Relative));

        }

        private void data_button_click(object sender, System.Windows.RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri("/DataPage.xaml?DeviceId=" + Email, UriKind.Relative));

        }

        private void user_button_click(object sender, System.Windows.RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri("/UserPage.xaml?DeviceId=" + Email, UriKind.Relative));

        }

        private void graph_button_click(object sender, System.Windows.Input.GestureEventArgs e)
        {
            NavigationService.Navigate(new Uri("/GraphPage.xaml?DeviceId=" + Email, UriKind.Relative));
        }

        private void btnBMP_tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            // TODO: Add event handler implementation here.
        }
        private void setting_app_icon_click(object sender, EventArgs e)
        {
            NavigationService.Navigate(new Uri("/SettingPage.xaml", UriKind.Relative));

        }

        private void help_app_icon_click(object sender, EventArgs e)
        {
            NavigationService.Navigate(new Uri("/InstructionPage.xaml", UriKind.Relative));

        }

        private void doctor_button_click(object sender, System.Windows.Input.GestureEventArgs e)
        {
        	// TODO: Add event handler implementation here.
            NavigationService.Navigate(new Uri("/Patient_see_doctor.xaml?Email=" + Email, UriKind.Relative));        
        }

        private void about_button_click(object sender, System.Windows.Input.GestureEventArgs e)
        {
            NavigationService.Navigate(new Uri("/AboutPage.xaml", UriKind.Relative));
        }

        private void hospital_button_click(object sender, System.Windows.Input.GestureEventArgs e)
        {
        	// TODO: Add event handler implementation here.
            NavigationService.Navigate(new Uri("/Patient_Hospital.xaml?Email=" + Email, UriKind.Relative));
        }
        protected override void OnBackKeyPress(System.ComponentModel.CancelEventArgs e)
        {
            MessageBoxResult m = MessageBox.Show("Are you sure?", "exit", MessageBoxButton.OKCancel);
            if (m == MessageBoxResult.Cancel)
            {
                e.Cancel = true;
            }
            else
            {
                if (pop_up_flag == true)
                {
                    this.popup_exit_anim.Begin();
                    pop_up_flag = false;
                    e.Cancel = true;
                }
                else
                {

                    Application.Current.Terminate();
                }
            }
        }
        #region CustomClasses
        class LiveDataThread
        {
            public static string loc { get; set; }
            public static string Email { get; set; }
            public static string BP { get; set; }
            public static bool flag = true;
            public static int count = 0;
          //  HealthDataServiceClient proxy = new HealthDataServiceClient("BasicHttpBinding_IHealthDataService");
            HappyWatchServiceClient proxy = new HappyWatchServiceClient("BasicHttpBinding_IHappyWatchService");

            UIUpdateDelegate dlgt;
            public LiveDataThread(string email, UIUpdateDelegate del)
            {
                Debug.WriteLine("LIVE THREAD" + email);
                Email=email;
                dlgt = del;
                
            }

            private void proxy_GetLocationCompleted(object sender,GetLocationCompletedEventArgs e)
            {

                Debug.WriteLine("LOCATION:   " + e.Result);
                loc = e.Result;
                dlgt.UpdateLocation(LiveDataThread.loc);
                

            }

            private void proxy_GetPulseCompleted(object sender, GetPulseCompletedEventArgs e)
            {
                Debug.WriteLine("LIVEDATA::::    " + e.Result);
                
                BP = e.Result;
                if (e.Result.Equals("N/A"))
                {
                    dlgt.UpdateBPM("N/A");
                    dlgt.UpdateCON("N/A");

                }
                else
                {
                    
                    Constants.hp1 = Convert.ToDouble(BP);
                    dlgt.UpdateBPM(LiveDataThread.BP);
                    if (Constants.hp1 < 55)
                    {

                        dlgt.UpdateCON("low");
                       // recom_text.Text = "1.Hypertension\n2. Congenital heart defect\n3. Electrolytes imbalance\n4. Haemochromatosis\n5. Heart block(first degree , second degree complete)\n6. Sinus nodal problem\n7. Hypothyroidism\n8. Obstructive sleep apnea\n9. Medication";

                    }
                    else if (Constants.hp1 >= 76)
                    {
                        dlgt.UpdateCON("high");
                        //recom_text.Text = "1.Fever\n2.Anaemia\n3.Dehydration\n4.Excitment\n5.Congenital heart disease\n6.Atrial fibrillation\n7.Shock(Cardiogenic, Hypovolumic,Septic)";

                    }
                    else
                    {
                        dlgt.UpdateCON("average");
                        //recom_text.Text = "you are fit enough but keep track";

                    }
                }
            }
            private void proxy_GetTimeCompleted(object sender, GetTimeCompletedEventArgs e)
            {
                if (e.Result.Equals("N/A"))
                {
                    dlgt.UpdateTime("N/A");

                }
                else
                {
                    string[] t = new string[3];
                    t = e.Result.Split(' ');
                 
                    Debug.WriteLine("TIME trimmed:  " + t[1]);

                        dlgt.UpdateTime(e.Result);
                }
            }



            public void Beta()
            {
                while (true)
                {

                    if (flag == false)
                    {
                        Thread.Sleep(100000);
                    }

                    Debug.WriteLine("ID: " + Email);
                    try
                    {
                        proxy.GetPulseCompleted += new EventHandler<GetPulseCompletedEventArgs>(proxy_GetPulseCompleted);
                        proxy.GetPulseAsync(Email);

                        proxy.GetLocationCompleted += new EventHandler<GetLocationCompletedEventArgs>(proxy_GetLocationCompleted);
                        proxy.GetLocationAsync(Email);

                        proxy.GetTimeCompleted += new EventHandler<GetTimeCompletedEventArgs>(proxy_GetTimeCompleted);
                        proxy.GetTimeAsync(Email);

                       
                    }
                    catch (Exception ex)
                    {
                        Debug.WriteLine("Exception in Thread:"+ex.Message);
                    }



                    Thread.Sleep(60000);
                }
            }
        }
        class GridData : INotifyPropertyChanged
        {
            private BPMInfo _info;
            private LocationData _location;
            private ConditionData _condition;
            public BPMInfo Info
            {
                get { return _info; }
                set
                {
                    _info = value;
                    NotifyPropertyChanged("Info");
                }
            }
            public LocationData LocData
            {
                get { return _location; }
                set
                {
                    _location = value;
                    NotifyPropertyChanged("LocData");
                }
            }
            public ConditionData ConData
            {
                get { return _condition; }
                set
                {
                    _condition = value;
                    NotifyPropertyChanged("ConData");
                }
            }
            // Declare the PropertyChanged event.
            public event PropertyChangedEventHandler PropertyChanged;
            // NotifyPropertyChanged will raise  the PropertyChanged event, 
            // passing the source property that is being updated.
            public void NotifyPropertyChanged(string propertyName)
            {
                if (PropertyChanged != null)
                {
                    PropertyChanged(this,
                        new PropertyChangedEventArgs(propertyName));
                }
            }
        }
        class BPMInfo : INotifyPropertyChanged
        {
            private string _bpm;
            private string _time;
            public string BPMRate
            {
                get
                {
                    return _bpm;
                }
                set
                {
                    _bpm = value;
                    NotifyPropertyChanged("BPMRate");
                }

            }
            public string Time
            {
                get
                {
                    return _time;
                }
                set
                {
                    _time = value;
                    NotifyPropertyChanged("Time");
                }
            }
            // Declare the PropertyChanged event.
            public event PropertyChangedEventHandler PropertyChanged;
            // NotifyPropertyChanged will raise the PropertyChanged event, 
            // passing the source property that is being updated.
            public void NotifyPropertyChanged(string propertyName)
            {
                if (PropertyChanged != null)
                {
                    PropertyChanged(this,
                        new PropertyChangedEventArgs(propertyName));
                }
            }
        }
        class LocationData : INotifyPropertyChanged
        {
            private string _loc;
            public string Location
            {
                get
                {
                    return _loc;
                }
                set
                {
                    _loc = value;
                    NotifyPropertyChanged("Location");
                }
            }
            // Declare the PropertyChanged event.
            public event PropertyChangedEventHandler PropertyChanged;
            // NotifyPropertyChanged will raise the PropertyChanged event, 
            // passing the source property that is being updated.
            public void NotifyPropertyChanged(string propertyName)
            {
                if (PropertyChanged != null)
                {
                    PropertyChanged(this,
                        new PropertyChangedEventArgs(propertyName));
                }
            }
        }
        class ConditionData : INotifyPropertyChanged
        {
            private string _cond;
            public string Condition
            {
                get
                {
                    return _cond;
                }
                set
                {
                    _cond = value;
                    NotifyPropertyChanged("Condition");
                }
            }
            // Declare the PropertyChanged event.
            public event PropertyChangedEventHandler PropertyChanged;
            // NotifyPropertyChanged will raise the PropertyChanged event, 
            // passing the source property that is being updated.
            public void NotifyPropertyChanged(string propertyName)
            {
                if (PropertyChanged != null)
                {
                    PropertyChanged(this,
                        new PropertyChangedEventArgs(propertyName));
                }
            }
        }

        #endregion

        private void logout_button_click(object sender, EventArgs e)
        {
            IsolatedStorageSettings settings = IsolatedStorageSettings.ApplicationSettings;
            string cookie = settings[Email].ToString();
            while (this.NavigationService.BackStack.Any())
            {
                this.NavigationService.RemoveBackEntry();
                
                
            }
            settings[Email] = "";
            settings[cookie] = 0;
            settings.Remove(Email);
            settings.Remove(cookie);
            settings.Clear();
            
            Application.Current.Terminate();
            
        }
        

    }

}