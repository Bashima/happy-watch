﻿using System;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Microsoft.Xna.Framework;
using System.IO.IsolatedStorage;
using Microsoft.Phone.Scheduler;
using System.Diagnostics;
using HappyWatch.HWwcf;
namespace HappyWatch
{
    public partial class SignUpDoctorPage : PhoneApplicationPage
    {
        string DName, Email,Password, Dob, Contact, Designation, Status, Gender, sent, LisenceId, Hospital,RePass, Speciality;
        String warning = "";
        public HappyWatchServiceClient proxy;

        public SignUpDoctorPage()
        {
           
            InitializeComponent();
           
            loading.Visibility = Visibility.Collapsed;
            proxy = new HappyWatchServiceClient("BasicHttpBinding_IHappyWatchService");
            MakeAllList();
        }

        private void Sign_Up_Doctors(object sender, System.Windows.Input.GestureEventArgs e)
        {
            Debug.WriteLine("in the button");
            warning = "";
            DName = name_box_up.Text;
            Email = email_box_up.Text;
            String Pword = password_box_up.Text;
            RePass = re_password_box_up.Text;
            if ((Pword.Equals(RePass)))
            {
                Password = password_box_up.Text;
            }
            else
            {
                Password = "";
                Microsoft.Xna.Framework.GamerServices.Guide.BeginShowMessageBox("Warning", "retype same password", new string[] { "ok" }, 0, Microsoft.Xna.Framework.GamerServices.MessageBoxIcon.Alert, null, null);
            }
            Dob = String.Format("{0:yyyy-MM-dd}", sign_up_date_picker.Value);
            Contact = contact_box_up.Text;
            Designation = "doctor";
            Status = "pending";
            if (Convert.ToBoolean(sign_up_female_button.IsChecked))
            {
                Gender = "Female";
            }
            else if (Convert.ToBoolean(sign_up_male_button.IsChecked))
            {
                Gender = "Male";
            }
            else if (Convert.ToBoolean(sign_up_other_button.IsChecked))
            {
                Gender = "Other";
            }
            LisenceId = doctor_lisence_id.Text;
            string hosp;
            try
            {
                hosp = hospital_box.Text;
                int i = hosp.IndexOf('(');

                hosp = hosp.Substring(0, i);
            }
            catch (Exception ex)
            {
                hosp = "null"; 
            }
            
                Debug.WriteLine("Hospital: " + hosp);
                Hospital = hosp;
            Speciality = speciality_box.Text;
            if (DName.Equals(""))
            {
                warning = "insert name";
                    
            }
            else if (Email.Equals(""))
            {
                warning = "insert email address";

            }
            else if (IsValidEmail(Email) == false)
            {
                warning = "insert valid email address";
            }
            else if (Contact.Equals(""))
            {
                warning = "insert contact number";
            }
            else if (Password.Equals(""))
            {
                warning = "enter password";
            }
            else if (RePass.Equals(""))
            {
                warning = "retype password";
            }
            if (warning.Equals(""))
            {
                string temp;
                temp = Email + "," + "active";
                prog.IsIndeterminate = true;
                loading.Visibility = Visibility.Visible;
                proxy = new HappyWatchServiceClient("BasicHttpBinding_IHappyWatchService");
                proxy.VerifyEmailCompleted += new EventHandler<VerifyEmailCompletedEventArgs>(proxy_VerifyEmailCompleted);
                proxy.VerifyEmailAsync(temp);

                
            }
            else//verifying if the given deviceid is new
            {
                Microsoft.Xna.Framework.GamerServices.Guide.BeginShowMessageBox("Warning", warning, new string[] { "ok" }, 0, Microsoft.Xna.Framework.GamerServices.MessageBoxIcon.Alert, null, null);
            }
        }
        void proxy_VerifyEmailCompleted(object sender, VerifyEmailCompletedEventArgs e)
        {
           // prog.IsIndeterminate = false;
           // loading.Visibility = Visibility.Collapsed;
            Debug.WriteLine("Result is: " + e.Result);
            if (e.Result.Equals("N/A"))
            {
                string temp;
                temp = LisenceId + ",active";
                proxy = new HappyWatchServiceClient("BasicHttpBinding_IHappyWatchService");
                proxy.VerifyDoctorIdCompleted += new EventHandler<VerifyDoctorIdCompletedEventArgs>(proxy_VerifyDoctorIdCompleted);
                proxy.VerifyDoctorIdAsync(temp);

            }
            else
            {
                Microsoft.Xna.Framework.GamerServices.Guide.BeginShowMessageBox("Error", "email id already exists", new string[] { "ok" }, 0, Microsoft.Xna.Framework.GamerServices.MessageBoxIcon.Alert, null, null);
                //progress_bar.IsIndeterminate = false;
                prog.IsIndeterminate = false;
                loading.Visibility = Visibility.Collapsed;
            }
        }
        void proxy_VerifyDoctorIdCompleted(object sender, VerifyDoctorIdCompletedEventArgs e)
        {

            prog.IsIndeterminate = false;
            loading.Visibility = Visibility.Collapsed;
            Debug.WriteLine("Result is: " + e.Result);
            if (e.Result.Equals("false"))
            {
                sent = DName + "," + Password + "," + Dob + "," + Gender + "," + Contact + "," + Designation + "," + Email + "," + Status + "," + LisenceId + "," + Hospital + "," + Speciality;
                Debug.WriteLine(sent);
                loading.Visibility = Visibility.Visible;
                prog.IsIndeterminate = true;
                insertcall();

            }
            else
            {
                prog.IsIndeterminate = false;
                loading.Visibility = Visibility.Collapsed;
                Microsoft.Xna.Framework.GamerServices.Guide.BeginShowMessageBox("Error", "license id already exists", new string[] { "ok" }, 0, Microsoft.Xna.Framework.GamerServices.MessageBoxIcon.Alert, null, null);
                prog.IsIndeterminate = false;
                loading.Visibility = Visibility.Collapsed;
                //     progress_bar.IsIndeterminate = false;

            }
        }
       
        void insertcall()//connecting to wcf to insert to database
         {
            ////////////// proxy. += new EventHandler<InsertInfoCompletedEventArgs>(proxy_InsertInfoCompleted);
             ////proxy.InsertInfoAsync(sent);
             proxy.InsertInfoDoctorCompleted += new EventHandler<InsertInfoDoctorCompletedEventArgs>(proxy_InsertInfoDoctorCompleted);
             sent = sent + "," + Constants.MyChannel;
             proxy.InsertInfoDoctorAsync(sent);
            
         }

        void proxy_InsertInfoDoctorCompleted(object sender, InsertInfoDoctorCompletedEventArgs e)
        {
            prog.IsIndeterminate = false;
            loading.Visibility = Visibility.Collapsed;
            Debug.WriteLine("RESULT IS:: "+e.Result.ToString());
            if (e.Result == "true")
            {
                Debug.WriteLine("signed up");
                //  NavigationService.Navigate(new Uri("/doctor.xaml?Email=" + Email, UriKind.Relative));
                Microsoft.Xna.Framework.GamerServices.Guide.BeginShowMessageBox("Sent", "SignUp request sent", new string[] { "ok" }, 0, Microsoft.Xna.Framework.GamerServices.MessageBoxIcon.Alert, null, null);
            }
            else
                Microsoft.Xna.Framework.GamerServices.Guide.BeginShowMessageBox("Error", "error while signing up", new string[] { "ok" }, 0, Microsoft.Xna.Framework.GamerServices.MessageBoxIcon.Alert, null, null);          
        }

        public void MakeAllList()
        {
            proxy.MakeAllHosptialListCompleted += new EventHandler<MakeAllHosptialListCompletedEventArgs>(proxy_MakeAllHosptialListCompleted);
            proxy.MakeAllHosptialListAsync();
        }

        void proxy_MakeAllHosptialListCompleted(object sender, MakeAllHosptialListCompletedEventArgs e)
        {
            //throw new NotImplementedException();
            Debug.WriteLine("all hospital list");
            string str;
            List<string> hosps = new List<string>();

            foreach (HospitalMod h in e.Result.ToList())
            {
                str = h.Name + "(" + h.Mail + ")";
                hosps.Add(str);

            }
            hospital_box.ItemsSource = hosps.ToList();
        }
        public static bool IsValidEmail(string strIn)
        {
            // Return true if strIn is in valid e-mail format.
            return Regex.IsMatch(strIn,
                   @"^(?("")("".+?""@)|(([0-9a-zA-Z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-zA-Z])@))" +
                   @"(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-zA-Z][-\w]*[0-9a-zA-Z]\.)+[a-zA-Z]{2,6}))$");
        }
       
    }
    
}