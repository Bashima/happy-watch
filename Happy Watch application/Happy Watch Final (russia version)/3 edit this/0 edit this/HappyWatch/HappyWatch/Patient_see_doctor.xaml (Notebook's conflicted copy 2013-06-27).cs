﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.Diagnostics;
using HappyWatch.HWwcf;

namespace HappyWatch
{
    public partial class Patient_see_doctor : PhoneApplicationPage
    {
        public string Email { get; set; }
        public HappyWatchServiceClient proxy;
        public Patient_see_doctor()
        {
            InitializeComponent();
            progress_bar.IsIndeterminate = true;
            proxy = new HappyWatchServiceClient();
        }
        protected override void OnNavigatedTo(System.Windows.Navigation.NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            proxy = new HappyWatchServiceClient();
            var title = NavigationContext.QueryString["Email"];

            Email = (String)title;
            Debug.WriteLine("Email: " + Email);
            MakeList();



        }
        void MakeAvailableList()
        {
            //proxy
            progress_bar.IsIndeterminate = true;
            proxy.MakeAvailableDocListCompleted += proxy_MakeAvailableDocListCompleted;
            proxy.MakeAvailableDocListAsync(Email);
        }

        void proxy_MakeAvailableDocListCompleted(object sender, MakeAvailableDocListCompletedEventArgs e)
        {
            doctorlist.ItemsSource = e.Result.ToList();
            Debug.WriteLine("Make all available list");
            List<string> docMails = new List<string>();
            foreach (ObPatient id in e.Result.ToList())
            {
                docMails.Add(id.pMail);
                Debug.WriteLine("Pmail: "+id.pName+" "+id.pMail);
            }
            auto_box.ItemsSource = docMails.ToList();
            progress_bar.IsIndeterminate = false;
        }

        void MakeList()
        {
            proxy.MakeOwnDocListCompleted += new EventHandler<MakeOwnDocListCompletedEventArgs>(proxy_MakeOwnDocListCompleted);
            proxy.MakeOwnDocListAsync(Email);
        }

        void proxy_MakeOwnDocListCompleted(object sender, MakeOwnDocListCompletedEventArgs e)
        {
            peoplesList.ItemsSource = e.Result.ToList();
            Debug.WriteLine("making list");
            progress_bar.IsIndeterminate = false;
            
        }
       

        private void Panoramaa_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            // TODO: Add event handler implementation here.
            switch (((Panorama)sender).SelectedIndex)
            {
                case 0: // defines the first PanoramaItem
                    //make list of his doctors
                    progress_bar.IsIndeterminate = true;
                    MakeList();

                    break;

                case 1: // second one
                    //make list of all hospital doctors he is under
                    MakeAvailableList();
                    progress_bar.IsIndeterminate = true;


                    break;



            }
        }

        private void AddButton_Click(object sender, RoutedEventArgs e)
        {
            string str = auto_box.Text;
            proxy.MakeDocRequestCompleted += new EventHandler<MakeDocRequestCompletedEventArgs>(proxy_MakeDocRequestCompleted);
            string sent = Email + "#" + str;
            Debug.WriteLine("doc pat req: "+sent);
            proxy.MakeDocRequestAsync(sent);
        }

        void proxy_MakeDocRequestCompleted(object sender, MakeDocRequestCompletedEventArgs e)
        {
            Debug.WriteLine("req result is: "+e.Result);
            if (e.Result == true)
            {
                MessageBox.Show("request sent");
            }
            else
            {
                MessageBox.Show("error while sending request");
            }
            progress_bar.IsIndeterminate = false;
        }
    }
}