﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using HappyWatch.HWwcf;
using System.Diagnostics;
using System.IO.IsolatedStorage;

namespace HappyWatch
{
    public partial class doctor : PhoneApplicationPage
    {
        public HappyWatchServiceClient proxy;
        public string Email { get; set; }
        public doctor()
        {
            InitializeComponent();
            proxy = new HappyWatchServiceClient("BasicHttpBinding_IHappyWatchService");
            progress_bar.IsIndeterminate = true;
        }
        protected override void OnBackKeyPress(System.ComponentModel.CancelEventArgs e)
        {
            //back press
            MessageBoxResult m = MessageBox.Show("Are you sure?", "exit", MessageBoxButton.OKCancel);
            if (m == MessageBoxResult.Cancel)
            {
                e.Cancel = true;
            }
            else
            {
                Application.Current.Terminate();
            }
        }
        protected override void OnNavigatedTo(System.Windows.Navigation.NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            var title = NavigationContext.QueryString["Email"];
            Email = (String)title;
            Debug.WriteLine("email: " + Email);
            makeList();
        }
        public void makeList()
        {
            proxy.OwnPatientListCompleted += new EventHandler<OwnPatientListCompletedEventArgs>(proxy_OwnPatientListCompleted);
            proxy.OwnPatientListAsync(Email);
 
        }

        void proxy_OwnPatientListCompleted(object sender, OwnPatientListCompletedEventArgs e)
        {
            progress_bar.IsIndeterminate = false;
            Debug.WriteLine("making list: ");
            peoplesList.ItemsSource = e.Result.ToList();
            List<ObPatient> oblist = new List<ObPatient>();
            oblist = e.Result.ToList();
            foreach (ObPatient var in oblist)
            {
                Debug.WriteLine("Name: " + var.pName);
            }
        }

        
        private void listBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                ObPatient ob = (ObPatient)peoplesList.SelectedItem;
                Debug.WriteLine("Mail is : " + ob.pMail);
                string pMail = ob.pMail + "," + ob.pName+","+Email;

                NavigationService.Navigate(new Uri("/doctor1.xaml?Email=" + pMail, UriKind.Relative));
            }
            catch (Exception ex)
            {
                Debug.WriteLine("Exception: " + ex.ToString());
            }

 
        }

        private void Doc_info_tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            NavigationService.Navigate(new Uri("/doctor_info.xaml?DeviceId=" + Email, UriKind.Relative));
	
			// TODO: Add event handler implementation here.
        }

        private void help_tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
        	NavigationService.Navigate(new Uri("/InstructionPage.xaml",UriKind.Relative));
        }

        private void about_tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
        	NavigationService.Navigate(new Uri("/AboutPage.xaml",UriKind.Relative));
			// TODO: Add event handler implementation here.
        }

        private void logout_button_click(object sender, EventArgs e)
        {
            IsolatedStorageSettings settings = IsolatedStorageSettings.ApplicationSettings;
            string cookie = settings[Email].ToString();
            while (this.NavigationService.BackStack.Any())
            {
                this.NavigationService.RemoveBackEntry();


            }
            settings[Email] = "";
            settings[cookie] = 0;
            settings.Remove(Email);
            settings.Remove(cookie);
            settings.Clear();
            Application.Current.Terminate();


        }
    }
}