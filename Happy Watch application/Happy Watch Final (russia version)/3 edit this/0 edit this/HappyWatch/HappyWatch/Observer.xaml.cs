﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using HappyWatch.HWwcf;
using System.Diagnostics;
using System.Collections.ObjectModel;
using System.IO.IsolatedStorage;
namespace HappyWatch
{
    public partial class Observer : PhoneApplicationPage
    {
        public HappyWatchServiceClient proxy;
        public string Email { get; set; }
       
      
        public Observer()
        {
            InitializeComponent();
            proxy = new HappyWatchServiceClient("BasicHttpBinding_IHappyWatchService");
            progress_bar.IsIndeterminate = true;
            makeList();
             

        }
        protected override void OnBackKeyPress(System.ComponentModel.CancelEventArgs e)
        {
            //back press
            MessageBoxResult m = MessageBox.Show("Are you sure?", "exit", MessageBoxButton.OKCancel);
            if (m == MessageBoxResult.Cancel)
            {
                e.Cancel = true;
            }
            else
            {
                Application.Current.Terminate();
            }
        }
          protected override void OnNavigatedTo(System.Windows.Navigation.NavigationEventArgs e)
          {
              base.OnNavigatedTo(e);
              var title = NavigationContext.QueryString["Email"];
              Email = (String)title;
              Debug.WriteLine("email: " + Email);
              makeList();
          }
        public void makeList()
        {
            Debug.WriteLine("LIST");
            proxy.ObservedListCompleted += new EventHandler<ObservedListCompletedEventArgs>(proxy_ObservedListCompleted);
            proxy.ObservedListAsync(Email);

        }

        void proxy_ObservedListCompleted(object sender, ObservedListCompletedEventArgs e)
        {
           
            // peopleList = new ObservableCollection<people>();
            Debug.WriteLine("LIST1");
            peoplesList.ItemsSource = e.Result.ToList();
            List<ObPatient> oblist = new List<ObPatient>();
            oblist = e.Result.ToList();
            foreach (ObPatient var in oblist)
            {
                Debug.WriteLine("Name: "+var.pName);
            }
            progress_bar.IsIndeterminate = false;      
            
        }
       
        private void listBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                ObPatient ob = (ObPatient)peoplesList.SelectedItem;
                //Debug.WriteLine("Mail is : " + ob.pMail);
                string pMail = ob.pMail + "," + ob.pName;

                NavigationService.Navigate(new Uri("/Observer1.xaml?Email=" + pMail, UriKind.Relative));
            }
            catch (Exception ex)
            {
                Debug.WriteLine("Exception: "+ex.ToString());
            }

 
        }

        private void Observer_Info(object sender, System.Windows.Input.GestureEventArgs e)
        {

            NavigationService.Navigate(new Uri("/Observer_Info.xaml?Email=" + Email, UriKind.Relative));

        }

        private void request_button_tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            NavigationService.Navigate(new Uri("/Observer_request.xaml?Email="+Email, UriKind.Relative));
        }

        private void help_tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            NavigationService.Navigate(new Uri("/InstructionPage.xaml", UriKind.Relative));

        }

        private void MyContextMenu_Opened(object sender, RoutedEventArgs e)
        {
            //khali thakbe
        }

        private void delete_patient_clicked(object sender, RoutedEventArgs e)
        {
            //code boshbe
            MessageBox.Show("clicked");
        }

        private void logout_button_click(object sender, EventArgs e)
        {
            IsolatedStorageSettings settings = IsolatedStorageSettings.ApplicationSettings;
            string cookie = settings[Email].ToString();
            while (this.NavigationService.BackStack.Any())
            {
                this.NavigationService.RemoveBackEntry();


            }
            settings[Email] = "";
            settings[cookie] = 0;
            settings.Remove(Email);
            settings.Remove(cookie);
            settings.Clear();
            Application.Current.Terminate();


        }

        private void about_us(object sender, System.Windows.Input.GestureEventArgs e)
        {
            NavigationService.Navigate(new Uri("/AboutPage.xaml", UriKind.Relative));
        }

    }
}