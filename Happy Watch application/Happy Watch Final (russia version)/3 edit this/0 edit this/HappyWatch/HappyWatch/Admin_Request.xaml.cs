﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using HappyWatch.HWwcf;
using System.Diagnostics;

namespace HappyWatch
{
    public partial class Admin_Request : PhoneApplicationPage
    {
        public HappyWatchServiceClient proxy;
        public string Email { get; set; }
        public Admin_Request()
        {
            proxy = new HappyWatchServiceClient("BasicHttpBinding_IHappyWatchService");
            InitializeComponent();
            progress_bar.IsIndeterminate = true;
           
        }
        protected override void OnNavigatedTo(System.Windows.Navigation.NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            var title = NavigationContext.QueryString["Email"];
            Email = (String)title;
            proxy = new HappyWatchServiceClient("BasicHttpBinding_IHappyWatchService");
            SignUpRequestList();
            
        }
        private void SignUpRequestList()
        {
            progress_bar.IsIndeterminate = true;
            proxy.SignUprequestsCompleted += new EventHandler<SignUprequestsCompletedEventArgs>(proxy_SignUprequestsCompleted);
            proxy.SignUprequestsAsync(Email);
        }

        void proxy_SignUprequestsCompleted(object sender, SignUprequestsCompletedEventArgs e)
        {
            Debug.WriteLine("Making sign up list: ");
            List<DocSignup> ob = new List<DocSignup>();
            ob = e.Result.ToList();
            List<Requests> docNames = new List<Requests>();
            foreach (DocSignup temp in ob)
            {
                Requests t = new Requests();
                t.name = temp.dName;
                t.mail = temp.dMail;
                docNames.Add(t);
            }
            signUpList.ItemsSource = docNames;
            progress_bar.IsIndeterminate = false;

        }

        private void AddRequestList()
        {
            progress_bar.IsIndeterminate = true;
            proxy.AddRequestsCompleted += new EventHandler<AddRequestsCompletedEventArgs>(proxy_AddRequestsCompleted);
            proxy.AddRequestsAsync(Email);
        }

        void proxy_AddRequestsCompleted(object sender, AddRequestsCompletedEventArgs e)
        {
            Debug.WriteLine("Making sign up list again: ");
            List<AddRequest> ob = new List<AddRequest>();
            ob = e.Result.ToList();
            List<Requests> request = new List<Requests>();
            foreach (AddRequest temp in ob)
            {
                Requests tempR = new Requests();
                
                tempR.name = temp.PatientName + "(" + temp.PatientMail + ")" +"\n"+"wants to add\n "+temp.DoctorName + "(" + temp.DoctorMail + ")";
                request.Add(tempR);
            }
            AddList.ItemsSource = request;
            progress_bar.IsIndeterminate = false;



        }
        private void AddSign(string sent)
        {
            progress_bar.IsIndeterminate = true;
            proxy.MakeSignCompleted += new EventHandler<MakeSignCompletedEventArgs>(proxy_MakeSignCompleted);
            proxy.MakeSignAsync(sent);
            
        }

        void proxy_MakeSignCompleted(object sender, MakeSignCompletedEventArgs e)
        {
            Debug.WriteLine("sign up result is :"+e.Result);
            if (e.Result == "true")
            {
                MessageBox.Show("request acepted");
            }
            else
            {
                MessageBox.Show("error while accepting request");
            }
            SignUpRequestList();
            progress_bar.IsIndeterminate = false;
        }
        private void RejectSign(string sent)
        {
            progress_bar.IsIndeterminate = true;
            proxy.RejectSignCompleted += new EventHandler<RejectSignCompletedEventArgs>(proxy_RejectSignCompleted);
            proxy.RejectSignAsync(sent);
            
        }

        void proxy_RejectSignCompleted(object sender, RejectSignCompletedEventArgs e)
        {
            Debug.WriteLine("sign reject result is :" + e.Result);
            if (e.Result == "true")
            {
                MessageBox.Show("request rejected");
            }
            else
            {
                MessageBox.Show("error while rejecting request");
            }
            SignUpRequestList();
            progress_bar.IsIndeterminate=false;
        }

        void MakeMedicalRelation(string sent)
        {
            progress_bar.IsIndeterminate = true;
            proxy.MakeMedRelationCompleted += new EventHandler<MakeMedRelationCompletedEventArgs>(proxy_MakeMedRelationCompleted);
            proxy.MakeMedRelationAsync(sent);
 
        }

        void proxy_MakeMedRelationCompleted(object sender, MakeMedRelationCompletedEventArgs e)
        {
            Debug.WriteLine("e nresult: "+e.Result);
            if (e.Result == true)
            {
                MessageBox.Show("request accepted");
            }
            else
            {
                MessageBox.Show("error while accepting request");
            }
            AddRequestList();
            progress_bar.IsIndeterminate = false;

        }
        void RejectMedicalRelation(string sent)
        {
            progress_bar.IsIndeterminate = true;
            proxy.RejectMedRelationCompleted += new EventHandler<RejectMedRelationCompletedEventArgs>(proxy_RejectMedRelationCompleted);
            proxy.RejectMedRelationAsync(sent);

 
        }

        void proxy_RejectMedRelationCompleted(object sender, RejectMedRelationCompletedEventArgs e)
        {
            if (e.Result == true)
            {
                MessageBox.Show("request rejected");
            }
            else
            {
                MessageBox.Show("error while rejecting request");
            }
            AddRequestList();
            progress_bar.IsIndeterminate = false;

        }
        private void Panoramaa_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            switch (((Panorama)sender).SelectedIndex)
            {
                case 0: // defines the first PanoramaItem
                    //SignUp request
                    progress_bar.IsIndeterminate = true;
                    SignUpRequestList();

                    break;

                case 1: // second one
                    //add request
                    AddRequestList();
                    progress_bar.IsIndeterminate = true;
                    break;

                case 2:
                    ///patient sign up list
                    ///
                    MakePatientHospitalReqList();
                    progress_bar.IsIndeterminate = true;
                    break;
                

            }
        }
        void MakePatientHospitalReqList()
        {
            progress_bar.IsIndeterminate = true;
            proxy.PatientHospitalReqListCompleted += new EventHandler<PatientHospitalReqListCompletedEventArgs>(proxy_PatientHospitalReqListCompleted);
            proxy.PatientHospitalReqListAsync(Email);
        }

        void proxy_PatientHospitalReqListCompleted(object sender, PatientHospitalReqListCompletedEventArgs e)
        {
            Debug.WriteLine("Making patient sign up list: ");
            patient_signup_list.ItemsSource = e.Result.ToList();
            progress_bar.IsIndeterminate = false;

        }
        private void tap_deny_add(object sender, System.Windows.Input.GestureEventArgs e)
        {
            string str = (string)((Button)sender).Tag;
            int i, j;
            i = str.IndexOf("(");
            j = str.IndexOf(")");
            string patientMail, docMail;
            patientMail = str.Substring(i + 1, j - i - 1);
            i = str.LastIndexOf("(");
            j = str.LastIndexOf(")");
            docMail = str.Substring(i + 1, j - i - 1);
            Debug.WriteLine(str);
            Debug.WriteLine(patientMail);
            Debug.WriteLine(docMail);
            string sent = patientMail + '#' + docMail;
            MakeMedicalRelation(sent);
            AddRequestList();
            progress_bar.IsIndeterminate = true;

        }
        private void tap_deny_sign(object sender, System.Windows.Input.GestureEventArgs e)
        {
            progress_bar.IsIndeterminate = true;
            string TargetName = (string)((Button)sender).Tag;
            Debug.WriteLine("Selected item is: " + TargetName);
            string s = TargetName;
            RejectSign(s);
            

        }
        private void tap_accept_add(object sender, System.Windows.Input.GestureEventArgs e)
        {
            progress_bar.IsIndeterminate = true;
            string str = (string)((Button)sender).Tag;
            int i, j;
            i = str.IndexOf("(");
            j = str.IndexOf(")");
            string patientMail, docMail;
            patientMail = str.Substring(i+1,j-i-1);
            i = str.LastIndexOf("(");
            j = str.LastIndexOf(")");
            docMail = str.Substring(i + 1, j - i - 1);
            Debug.WriteLine(str);
            Debug.WriteLine(patientMail);
            Debug.WriteLine(docMail);
            string sent=patientMail+'#'+docMail;
            MakeMedicalRelation(sent);
            AddRequestList();


        }
        private void tap_accept_sign(object sender, RoutedEventArgs e)
        {
            progress_bar.IsIndeterminate = true;
            string TargetName = (string)((Button)sender).Tag;
            Debug.WriteLine("Selected item is: " + TargetName);
            string s =  TargetName;
            AddSign(s);


        }
        public class Requests
        {
            public string name { get; set; }
            public string mail { get; set; }
        }
        public class AddReq
        {
            public string pName { get; set; }
            public string dName { get; set; }
        }


        private void tap_deny_patient(object sender, System.Windows.Input.GestureEventArgs e)
        {
            progress_bar.IsIndeterminate = true;
            string TargetName = (string)((Button)sender).Tag;
            string sent = Email + '#' + TargetName;
            Debug.WriteLine("sent: "+sent);
            proxy.RejectPatientHospitalRequestCompleted += new EventHandler<RejectPatientHospitalRequestCompletedEventArgs>(proxy_RejectPatientHospitalRequestCompleted);
            proxy.RejectPatientHospitalRequestAsync(sent);
            MakePatientHospitalReqList();

        

        }

        void proxy_RejectPatientHospitalRequestCompleted(object sender, RejectPatientHospitalRequestCompletedEventArgs e)
        {
            Debug.WriteLine("result of reject: "+e.Result);
            if (e.Result == true)
            {
                MessageBox.Show("request rejected");
            }
            else
            {
                MessageBox.Show("error while rejecting request");
            }
            progress_bar.IsIndeterminate = false;
        }

        private void tap_accept_patient(object sender, System.Windows.Input.GestureEventArgs e)
        
        {
            progress_bar.IsIndeterminate = true;
            string TargetName = (string)((Button)sender).Tag;
            string sent = Email + '#' + TargetName;
            Debug.WriteLine("sent: " + sent);
            proxy.AcceptPatientHospitalRequestCompleted += new EventHandler<AcceptPatientHospitalRequestCompletedEventArgs>(proxy_AcceptPatientHospitalRequestCompleted);
            proxy.AcceptPatientHospitalRequestAsync(sent);
            MakePatientHospitalReqList();


        }

        void proxy_AcceptPatientHospitalRequestCompleted(object sender, AcceptPatientHospitalRequestCompletedEventArgs e)
        {
            Debug.WriteLine("result of accept: " + e.Result);
            if (e.Result == true)
            {
                MessageBox.Show("request appected");
            }
            else
            {
                MessageBox.Show("error while accepting request");
            }
            MakePatientHospitalReqList();
            progress_bar.IsIndeterminate = false;
            
        }
    }
}