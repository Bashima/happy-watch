﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Microsoft.Xna.Framework;
using System.IO.IsolatedStorage;
using Microsoft.Phone.Scheduler;
using HappyWatch.HWwcf;
using System.Diagnostics;

namespace HappyWatch
{
    public partial class Admin_Doctor : PhoneApplicationPage
    {
        public HappyWatchServiceClient proxy;
        public string Email { get; set; }
        public string Str { get; set; }
        public string[] MyArray = new string[10];

        public Admin_Doctor()
        {
            InitializeComponent();
			this.AddContextMenuWithBinding();
            proxy = new HappyWatchServiceClient("BasicHttpBinding_IHappyWatchService");
            progress_bar.IsIndeterminate = true;
        }
        protected override void OnNavigatedTo(System.Windows.Navigation.NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            var title = NavigationContext.QueryString["Email"];
            
            Str = (String)title;
            MyArray = Str.Split('$');
            Email = MyArray[0];
            proxy = new HappyWatchServiceClient("BasicHttpBinding_IHappyWatchService");
            No_Doctors.Text = MyArray[1];
            MakeDoctorList();
        }
        private void MakeDoctorList()
        {
            proxy.HospitalDoctorListCompleted += new EventHandler<HospitalDoctorListCompletedEventArgs>(proxy_HospitalDoctorListCompleted);
            proxy.HospitalDoctorListAsync(Email);
        }

        void proxy_HospitalDoctorListCompleted(object sender, HospitalDoctorListCompletedEventArgs e)
        {
            Debug.WriteLine("doc list is forming: ");
            
            peoplesList.ItemsSource = e.Result.ToList();
            progress_bar.IsIndeterminate = false;
        }
		private void AddContextMenuWithBinding()
		{
    		ContextMenu contextMenu = new ContextMenu();
    		contextMenu.ItemsSource = new List<string> {"Delete"};
    		ContextMenuService.SetContextMenu(this.ContentPanel, contextMenu);
            
			
		}

        private void MyContextMenu_Opened(object sender, RoutedEventArgs e)
        {
            //khali thakbe
        }

        private void delete_docotor_clicked(object sender, RoutedEventArgs e)
        {
            //code boshbe
            MessageBox.Show("clicked");
            string header = (sender as MenuItem).Header.ToString();
            Debug.WriteLine("doc said: "+header);
          //  ListBoxItem contextMenuListItem = peoplesList.ItemContainerGenerator.ContainerFromItem((sender as ContextMenu).DataContext) as ListBoxItem;
           /* ListBoxItem selectedListBoxItem = this.peoplesList.ItemContainerGenerator.ContainerFromItem((sender as MenuItem).DataContext) as ListBoxItem;
            TempC temp = (TempC)selectedListBoxItem.Content;  
            //string temp= contextMenuListItem.Content.ToString();*/
            ListBoxItem selectedListBoxItem = this.peoplesList.ItemContainerGenerator.ContainerFromItem((sender as MenuItem).DataContext) as ListBoxItem;
            ObPatient temp = (ObPatient)selectedListBoxItem.Content;  
            if (selectedListBoxItem != null)
                Debug.WriteLine("Doc Mail: " + temp.pMail);
            else
                Debug.WriteLine("NULL");
            string sent = Email + "#" + temp.pMail;
            Debug.WriteLine("SENT: "+sent);
            proxy.AdminDeleteDoctorCompleted += new EventHandler<AdminDeleteDoctorCompletedEventArgs>(proxy_AdminDeleteDoctorCompleted);
            proxy.AdminDeleteDoctorAsync(sent);

        }

        void proxy_AdminDeleteDoctorCompleted(object sender, AdminDeleteDoctorCompletedEventArgs e)
        {
            Debug.WriteLine("Result: "+e.Result.ToString());
        }
        
    }
}