﻿using System;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using HappyWatch.Resources;
using HappyWatch.HWwcf;
using System.Diagnostics;
using Microsoft.Xna.Framework;
using System.IO.IsolatedStorage;
using Microsoft.Phone.Scheduler;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.IO.IsolatedStorage;
using Microsoft.Phone.Notification;
using System.Text;
namespace HappyWatch
{
    public partial class MainPage : PhoneApplicationPage
    {
        String Email, Password, sent;
        IsolatedStorageSettings settings = IsolatedStorageSettings.ApplicationSettings;
        
        HappyWatchServiceClient proxy;



        // Constructor
        public MainPage()
        {
            var settings = IsolatedStorageSettings.ApplicationSettings;
            InitializeComponent();
            InitializeSettings();
            loading.Visibility = Visibility.Collapsed;
            
            if (Constants.hasNetworkConnection == false)
            {
                MessageBox.Show("you have to connect to a network to use this app");
            }
            Constants.MyChannel = "NULL";
            loading.Visibility = Visibility.Collapsed;
            ShellTile appTile = ShellTile.ActiveTiles.First();
            if (appTile != null)
            {
                StandardTileData newTile = new StandardTileData
                {
                    Title = "Happy Watch",
                    BackgroundImage = new Uri("Optimized-Happy_Watch_Tile.png", UriKind.Relative),
                    
                    BackTitle = "Happy Watch",
                    //BackBackgroundImage = new Uri("Optimized-PanoramaBackground.png", UriKind.Relative),
                    BackContent = "HAPPY WATCH"
                };
                appTile.Update(newTile);
            }

            this.title_in_anim.Begin();
           try
           {
               proxy = new HappyWatchServiceClient("BasicHttpBinding_IHappyWatchService1");
                //settings.Add("IsRemember","unchecked");
           }
            catch(Exception ex)
            {
              //  Microsoft.Xna.Framework.GamerServices.Guide.BeginShowMessageBox("Warning", "error in network connection", new string[] { "ok" }, 0, Microsoft.Xna.Framework.GamerServices.MessageBoxIcon.Alert, null, null);
                Debug.WriteLine("Exception is: "+ex);
            }






           HttpNotificationChannel pushChannel;
           string channelName = "ToastSampleChannel";
           InitializeComponent();
           pushChannel = HttpNotificationChannel.Find(channelName);
           //System.Diagnostics.Debug.WriteLine(pushChannel.ChannelUri.ToString());
           Debug.WriteLine("here channel: " + pushChannel);

           try
           {
               if (pushChannel == null)
               {
                   pushChannel = new HttpNotificationChannel(channelName);


                   // Register for all the events before attempting to open the channel.
                   Debug.WriteLine("calling");
                   pushChannel.ChannelUriUpdated += new EventHandler<NotificationChannelUriEventArgs>(PushChannel_ChannelUriUpdated);
                   pushChannel.ErrorOccurred += new EventHandler<NotificationChannelErrorEventArgs>(PushChannel_ErrorOccurred);

                   // Register for this notification only if you need to receive the notifications while your application is running.
                   pushChannel.ShellToastNotificationReceived += new EventHandler<NotificationEventArgs>(PushChannel_ShellToastNotificationReceived);

                   pushChannel.Open();
                   //   Debug.WriteLine("there channel: " + pushChannel.ChannelUri.ToString());
                   //MessageBox.Show(pushChannel.ChannelUri.ToString());
                   Constants.MyChannel = pushChannel.ChannelUri.ToString();

                   // Bind this new channel for toast events.
                   pushChannel.BindToShellToast();

               }
               else
               {
                   // The channel was already open, so just register for all the events.
                   pushChannel.ChannelUriUpdated += new EventHandler<NotificationChannelUriEventArgs>(PushChannel_ChannelUriUpdated);
                   pushChannel.ErrorOccurred += new EventHandler<NotificationChannelErrorEventArgs>(PushChannel_ErrorOccurred);

                   // Register for this notification only if you need to receive the notifications while your application is running.
                   pushChannel.ShellToastNotificationReceived += new EventHandler<NotificationEventArgs>(PushChannel_ShellToastNotificationReceived);

                   // Display the URI for testing purposes. Normally, the URI would be passed back to your web service at this point.
                   System.Diagnostics.Debug.WriteLine("here again: " + pushChannel.ChannelUri.ToString());
                   Constants.MyChannel = pushChannel.ChannelUri.ToString();
                   // MessageBox.Show(String.Format("Channel Uri is {0}",
                   //   pushChannel.ChannelUri.ToString()));

               }

           }
           catch (Exception ex)
           {
               Debug.WriteLine("exceptions is :"+ex.ToString());
           }

        }

        private void sign_up_button(object sender, System.Windows.Input.GestureEventArgs e)
        {
            if (Constants.hasNetworkConnection == false)
            {
                MessageBox.Show("no internet connection");

            }
            else
            {
                NavigationService.Navigate(new Uri("/SignUpPage.xaml", UriKind.Relative));
            }
        }
        private void Sign_In_Button_Click(object sender, RoutedEventArgs e)
        {
            if (Constants.hasNetworkConnection == false)
            {
                MessageBox.Show("no internet connection");

            }
            else
            {
                Email = email_box.Text;
                Password = password_box.Password;
                if (Email.Equals(""))
                {
                    Microsoft.Xna.Framework.GamerServices.Guide.BeginShowMessageBox("Warning", "insert email address", new string[] { "ok" }, 0, Microsoft.Xna.Framework.GamerServices.MessageBoxIcon.Alert, null, null);
                }
                else if (IsValidEmail(Email) == false)
                {
                    Microsoft.Xna.Framework.GamerServices.Guide.BeginShowMessageBox("Warning", "insert valid email address", new string[] { "ok" }, 0, Microsoft.Xna.Framework.GamerServices.MessageBoxIcon.Alert, null, null);
                }
                else if (Password.Equals(""))
                {
                    Microsoft.Xna.Framework.GamerServices.Guide.BeginShowMessageBox("Warning", "insert password", new string[] { "ok" }, 0, Microsoft.Xna.Framework.GamerServices.MessageBoxIcon.Alert, null, null);
                }
                    
                else
                {
                    sent = Email + "," + Password + "," + "active";
                    proxy.VerifyIdCompleted += new EventHandler<VerifyIdCompletedEventArgs>(proxy_VerifyIdCompleted);
                    Debug.WriteLine("SENT: " + sent);
                    loading.Visibility = Visibility.Visible;
                    prog.IsIndeterminate = true;
                    proxy.VerifyIdAsync( sent);
                }
            }
        }
        private void InitializeSettings()
        {
            string temp;
            string[]temp1= new string[5];
            /*if (settings.Contains("emailFlag"))
            {
                temp = (string)settings["emailFlag"];
                Debug.WriteLine("ekhane:          "+temp);
                temp1 = temp.Split(',');
                email_box.Text=temp1[0];
                password_box.Password = temp1[1];
            }
            else settings.Add("emailFlag", "");*/
        }

        void NavigationService_Navigating(object sender, NavigatingCancelEventArgs e)
        {
            loading.Visibility = Visibility.Collapsed;
        }
        void proxy_VerifyIdCompleted(object sender, VerifyIdCompletedEventArgs e)//verifyig if the id is valid means not exusts in the database already
        {
            
            if (e.Result.Equals("N/A"))
            {
                loading.Visibility = Visibility.Collapsed;
                MessageBox.Show("invalid email address or password");
            }
            else
            {
                
                string a = Email + "," + Password;
                if (sign_in_device_id_remember.IsChecked == true)
                {
                    settings["emailFlag"] = a;
                    Debug.WriteLine("adfgdsfgds............... " + a);
                }
                Debug.WriteLine("inside");
                Debug.WriteLine("Patient: " + e.Result.ToString());
                
                Constants.results = e.Result.ToString().Split(' ');
                Debug.WriteLine("em: "+Email);
                string cookie = Constants.results[2];
                settings["UserMail"] = Email;
                settings[Email]=cookie;
                settings[cookie] = DateTime.Now.Ticks;
                settings["designation"] = Constants.results[0];
                if (Constants.results[0].Equals("patient"))
                {
                    NavigationService.Navigate(new Uri("/MenuPage.xaml?Email=" + Email, UriKind.Relative));
                }
                else if (Constants.results[0].Equals("doctor"))
                {
                    NavigationService.Navigate(new Uri("/doctor.xaml?Email=" + Email, UriKind.Relative));
                }
                else if (Constants.results[0].Equals("observer"))
                {
                    NavigationService.Navigate(new Uri("/Observer.xaml?Email=" + Email, UriKind.Relative));
                }
                else if (Constants.results[0].Equals("admin"))
                {
                    NavigationService.Navigate(new Uri("/Admin_Main.xaml?Email=" + Email, UriKind.Relative));

                }

            }
        }

        private void Password_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
        	password_box.SelectAll();
        }

        private void help_button_click(object sender, System.EventArgs e)
        {
        	NavigationService.Navigate(new Uri("/InstructionPage.xaml", UriKind.Relative));
 
			// TODO: Add event handler implementation here.
        }
        public static bool IsValidEmail(string strIn)
        {
            // Return true if strIn is in valid e-mail format.
            return Regex.IsMatch(strIn,
                   @"^(?("")("".+?""@)|(([0-9a-zA-Z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-zA-Z])@))" +
                   @"(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-zA-Z][-\w]*[0-9a-zA-Z]\.)+[a-zA-Z]{2,6}))$");
        }





        void PushChannel_ChannelUriUpdated(object sender, NotificationChannelUriEventArgs e)
        {
            Debug.WriteLine("Channle is here");

            Dispatcher.BeginInvoke(() =>
            {
                // Display the new URI for testing purposes.   Normally, the URI would be passed back to your web service at this point.
                Constants.MyChannel = e.ChannelUri.ToString();
                System.Diagnostics.Debug.WriteLine(e.ChannelUri.ToString());
                
               // MessageBox.Show(String.Format("Channel Uri is {0}",
                 //   e.ChannelUri.ToString()));

            });
        }

        void PushChannel_ErrorOccurred(object sender, NotificationChannelErrorEventArgs e)
        {
            // Error handling logic for your particular application would be here.
            Dispatcher.BeginInvoke(() =>
                MessageBox.Show(String.Format("A push notification {0} error occurred.  {1} ({2}) {3}",
                    e.ErrorType, e.Message, e.ErrorCode, e.ErrorAdditionalData))
                    );
        }

        void PushChannel_ShellToastNotificationReceived(object sender, NotificationEventArgs e)
        {
            StringBuilder message = new StringBuilder();
            string relativeUri = string.Empty;

            message.AppendFormat("Received Toast {0}:\n", DateTime.Now.ToShortTimeString());

            // Parse out the information that was part of the message.
            foreach (string key in e.Collection.Keys)
            {
                message.AppendFormat("{0}: {1}\n", key, e.Collection[key]);

                if (string.Compare(
                    key,
                    "wp:Param",
                    System.Globalization.CultureInfo.InvariantCulture,
                    System.Globalization.CompareOptions.IgnoreCase) == 0)
                {
                    relativeUri = e.Collection[key];
                }
            }

            // Display a dialog of all the fields in the toast.
            Dispatcher.BeginInvoke(() => MessageBox.Show(message.ToString()));

        }
        
    }
}