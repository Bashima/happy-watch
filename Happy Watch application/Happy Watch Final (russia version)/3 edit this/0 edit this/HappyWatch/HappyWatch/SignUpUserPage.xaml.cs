﻿using System;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.Diagnostics;
using Microsoft.Xna.Framework;
using HappyWatch.HWwcf;
using Microsoft.Phone.Scheduler;

namespace HappyWatch
{
    public partial class SignUpUserPage : PhoneApplicationPage
    {
        String UName, DeviceId, Email, Dob, Gender, Contact, Password, UCondition, Hospital, Status, Designation, sent,Location;
        public HappyWatchServiceClient proxy;

        public SignUpUserPage()
        {
           
            InitializeComponent();
            loading.Visibility = Visibility.Collapsed;
        }

        private void Ok_Button_Click(object sender, RoutedEventArgs e)
        {

        }

        private void Sign_up_button_tao_patient(object sender, RoutedEventArgs e)
        {
            Debug.WriteLine("in");
            UName = name_box_up.Text;
            DeviceId = device_id_box.Text;
            Email = email_box_up.Text;
            Location = address_box_up.Text;
            Debug.WriteLine("Location is: "+Location);



            if ((password_box_up.Text.Equals(re_password_box_up.Text)))
            {
                Password = password_box_up.Text;
            }
            else
            {
                Microsoft.Xna.Framework.GamerServices.Guide.BeginShowMessageBox("Warning", "retype same password", new string[] { "ok" }, 0, Microsoft.Xna.Framework.GamerServices.MessageBoxIcon.Alert, null, null);
            }
            Dob = String.Format("{0:yyyy-MM-dd}", sign_up_date_picker.Value);
            Contact = contact_box_up.Text;
            Designation = "patient";
            Status = "active";
            // Hospital = hospital_box.Text;
            Hospital = "NONE";
            if (Hospital.Equals(""))
            {
                Hospital = "NONE";
            }
            if (Convert.ToBoolean(sign_up_female_button.IsChecked))
            {
                Gender = "Female";
            }
            else if (Convert.ToBoolean(sign_up_male_button.IsChecked))
            {
                Gender = "Male";
            }
            else if (Convert.ToBoolean(sign_up_other_button.IsChecked))
            {
                Gender = "Other";
            }
            if (pregnant_box.IsChecked == true && autism_box.IsChecked == false)
            {
                UCondition = "pregnant";
            }
            else if (pregnant_box.IsChecked == false && autism_box.IsChecked == true)
            {
                UCondition = "autistic";
            }
            else if (pregnant_box.IsChecked == true && autism_box.IsChecked == true)
            {
                UCondition = "pregnant+autistic";
            }
            else
            {
                UCondition = "None";
            }
            String warning = "";
            if ((email_box_up.Text.Equals("")))
            {
                warning = "insert email address";
            }
            else if (IsValidEmail(Email) == false)
            {
                warning = "insert valid email address";
            }
            else if (UName.Equals(""))
            {
                warning = "insert name";
            }
            else if (contact_box_up.Equals(""))
            {
                warning = "insert contact number";
            }
            else if (password_box_up.Equals(""))
            {
                warning = "insert password";
            }
            else if (re_password_box_up.Equals(""))
            {
                warning = "retype password";
            }
            else if (device_id_box.Equals(""))
            {
                warning = "insert device id";
            }

            if (warning.Equals(""))//verifying if the given deviceid is new
            {
                string temp;
                temp = Email + "," + "active";
                progress_bar.IsIndeterminate = true;
                proxy = new HappyWatchServiceClient("BasicHttpBinding_IHappyWatchService");
                proxy.VerifyEmailCompleted += new EventHandler<VerifyEmailCompletedEventArgs>(proxy_VerifyEmailCompleted);
                proxy.VerifyEmailAsync(temp);
            }
            else
            {
                    Microsoft.Xna.Framework.GamerServices.Guide.BeginShowMessageBox("Warning", warning, new string[] { "ok" }, 0, Microsoft.Xna.Framework.GamerServices.MessageBoxIcon.Alert, null, null);
            }
                Debug.WriteLine(sent);
            
        }
        void proxy_VerifyDeviceIdCompleted(object sender, VerifyDeviceIdCompletedEventArgs e)
        {
            prog.IsIndeterminate = false;
            loading.Visibility = Visibility.Collapsed;
            Debug.WriteLine("Result is: " + e.Result);
            if (e.Result.Equals("false"))
            {
                proxy = new HappyWatchServiceClient("BasicHttpBinding_IHappyWatchService");
                sent = UName + "#" + Password + "#" + Dob + "#" + Gender + "#" + Contact + "#" + Designation + "#" + Email + "#" + Status + "#" + DeviceId + "#" + Hospital + "#" + UCondition + "#"+Location;
                Debug.WriteLine("Sent: " + sent);
                loading.Visibility = Visibility.Visible;
                prog.IsIndeterminate = true;
                sent = sent + "#" + Constants.MyChannel;
                proxy.InsertInfoPatientCompleted += new EventHandler<InsertInfoPatientCompletedEventArgs>(proxy_InsertInfoPatientCompleted);
                proxy.InsertInfoPatientAsync(sent);
            }
            else
            {
                Microsoft.Xna.Framework.GamerServices.Guide.BeginShowMessageBox("Error", "device id already exists", new string[] { "ok" }, 0, Microsoft.Xna.Framework.GamerServices.MessageBoxIcon.Alert, null, null);
                progress_bar.IsIndeterminate = false;
            }
        }
        void proxy_VerifyEmailCompleted(object sender, VerifyEmailCompletedEventArgs e)
        {
            prog.IsIndeterminate = false;
            loading.Visibility = Visibility.Collapsed;
            Debug.WriteLine("Result is: " + e.Result);
            if (e.Result.Equals("N/A"))
            {
                string temp;
                temp = DeviceId + ",active";
                proxy = new HappyWatchServiceClient("BasicHttpBinding_IHappyWatchService");
                proxy.VerifyDeviceIdCompleted += new EventHandler<VerifyDeviceIdCompletedEventArgs>(proxy_VerifyDeviceIdCompleted);
                proxy.VerifyDeviceIdAsync(temp);

            }
            else
            {
                Microsoft.Xna.Framework.GamerServices.Guide.BeginShowMessageBox("Error", "email id already exists", new string[] { "ok" }, 0, Microsoft.Xna.Framework.GamerServices.MessageBoxIcon.Alert, null, null);
                progress_bar.IsIndeterminate = false;
            }
        }
        void proxy_InsertInfoPatientCompleted(object sender, InsertInfoPatientCompletedEventArgs e)
        {
            prog.IsIndeterminate = false;
            loading.Visibility = Visibility.Collapsed;
            Debug.WriteLine("Result is: "+e.Result);
            if (e.Result == "true")
            {
                Debug.WriteLine("signed up");
                NavigationService.Navigate(new Uri("/MenuPage.xaml?Email=" + Email, UriKind.Relative));
            }
            else
                Microsoft.Xna.Framework.GamerServices.Guide.BeginShowMessageBox("Error", "error while signing up", new string[] { "ok" }, 0, Microsoft.Xna.Framework.GamerServices.MessageBoxIcon.Alert, null, null);          
       
        }
        public static bool IsValidEmail(string strIn)
        {
            // Return true if strIn is in valid e-mail format.
            return Regex.IsMatch(strIn,
                   @"^(?("")("".+?""@)|(([0-9a-zA-Z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-zA-Z])@))" +
                   @"(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-zA-Z][-\w]*[0-9a-zA-Z]\.)+[a-zA-Z]{2,6}))$");
        }

    }
}