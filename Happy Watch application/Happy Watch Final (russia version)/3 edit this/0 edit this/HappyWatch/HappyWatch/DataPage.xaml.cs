﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using HappyWatch.HWwcf;
using System.Diagnostics;

namespace HappyWatch
{
    public partial class DataPage : PhoneApplicationPage
    {
        public  HappyWatchServiceClient proxy ;
        public string datasentdaily, datasentmonthly, Email;
        public string datasent { get; set; }
        DateTime date = DateTime.Now;
        int count = 0;
        public DataPage()
        {
            InitializeComponent();
            proxy = new HappyWatchServiceClient("BasicHttpBinding_IHappyWatchService");
            progress_bar.IsIndeterminate = true;
        }
        protected override void OnNavigatedTo(System.Windows.Navigation.NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            var title = NavigationContext.QueryString["DeviceId"];
            Email = (String)title;
            Debug.WriteLine("DATA DEVICE: " + Email);
            proxy = new HappyWatchServiceClient("BasicHttpBinding_IHappyWatchService");

            datacall();

            //  graphCall();


        }
        void datacall()//connecting to the wcf to get info
        {

           // HealthDataServiceClient proxy = new HealthDataServiceClient("BasicHttpBinding_IHealthDataService");
            string datasentdaily, datasentmonthly, datasent,datamonth;
            DateTime date = DateTime.Now;
            datasent = "";
            datamonth = "";
            datasentdaily = Email + "," + string.Format("{0:yyyy-MM-dd}", date);
            datasentmonthly = Email + "," + string.Format("{0:yyyy-MM-dd}", date);
            datasent = datasentdaily + ",daily";
            datamonth = datasentmonthly + ",monthly";
            Debug.WriteLine("datamonth: " + datamonth);
            proxy.FindPulseMaxCompleted += new EventHandler<FindPulseMaxCompletedEventArgs>(proxy_FindPulseMaxCompleted);
            proxy.FindPulseMaxAsync(Email);

            proxy.FindPulseMinCompleted += new EventHandler<FindPulseMinCompletedEventArgs>(proxy_FindPulseMinCompleted);
            proxy.FindPulseMinAsync(Email);

            proxy.FindPulseAvCompleted += new EventHandler<FindPulseAvCompletedEventArgs>(proxy_FindPulseAvCompleted);
            proxy.FindPulseAvAsync(Email);

            proxy.FindPulseMaxMonthCompleted += new EventHandler<FindPulseMaxMonthCompletedEventArgs>(proxy_FindPulseMaxMonthCompleted);
            proxy.FindPulseMaxMonthAsync(Email);

            proxy.FindPulseMinMonthCompleted += new EventHandler<FindPulseMinMonthCompletedEventArgs>(proxy_FindPulseMinMonthCompleted);
            proxy.FindPulseMinMonthAsync(Email);

            proxy.FindPulseAvMonthCompleted += new EventHandler<FindPulseAvMonthCompletedEventArgs>(proxy_FindPulseAvMonthCompleted);
            proxy.FindPulseAvMonthAsync(Email);

        }
        void proxy_FindPulseMaxCompleted(object sender, FindPulseMaxCompletedEventArgs e)
        {
            count++;
            if (count == 6)
            {
                progress_bar.IsIndeterminate = false;
            }
            if (e.Result.Equals("N/A"))
            {
                DailyMax.Text = "N/A";
                Debug.WriteLine("maxxxxxxxxxxxx:           " + e.Result);
            }
            else
            {
                DailyMax.Text = e.Result;
                Debug.WriteLine("maxxxxxxxxxxxx:           " + e.Result);
            }
        }
        void proxy_FindPulseMinCompleted(object sender, FindPulseMinCompletedEventArgs e)
        {
            count++;
            if (count == 6)
            {
                progress_bar.IsIndeterminate = false;
            }
            if (e.Result.Equals("NULL"))
            {
                DailyMin.Text = "N/A";
            }
            else
            {
                DailyMin.Text = e.Result;
                Debug.WriteLine("min: " + e.Result);
            }
        }
        void proxy_FindPulseAvCompleted(object sender, FindPulseAvCompletedEventArgs e)
        {
            count++;
            if (count == 6)
            {
                progress_bar.IsIndeterminate = false;
            }
            if (e.Result.Equals("N/A"))
            {
                DailyAv.Text = "N/A";
            }
            else
            {
                DailyAv.Text = e.Result;
                Debug.WriteLine("AVERAGE: " + e.Result);
            }
        }
        void proxy_FindPulseMaxMonthCompleted(object sender, FindPulseMaxMonthCompletedEventArgs e)
        {
            count++;
            if (count == 6)
            {
                progress_bar.IsIndeterminate = false;
            }
            if (e.Result.Equals("N/A"))
            {
                MonthlyMax.Text = "N/A";
            }
            else
            {
                MonthlyMax.Text = e.Result;
                Debug.WriteLine("maxxxxxxxxxxxx:           " + e.Result);
            }
        }
        void proxy_FindPulseMinMonthCompleted(object sender, FindPulseMinMonthCompletedEventArgs e)
        {
            count++;
            if (count == 6)
            {
                progress_bar.IsIndeterminate = false;
            }
            if (e.Result.Equals("N/A"))
            {
                MonthlyMin.Text = "N/A";
            }
            else
            {
                MonthlyMin.Text = e.Result;
                Debug.WriteLine("min: " + e.Result);
            }
        }
        void proxy_FindPulseAvMonthCompleted(object sender, FindPulseAvMonthCompletedEventArgs e)
        {
            count++;
            if (count == 6)
            {
                progress_bar.IsIndeterminate = false;
            }
            if (e.Result.Equals("N/A"))
            {
                MonthlyAv.Text = "N/A";
            }
            else
            {
                MonthlyAv.Text = e.Result;
                Debug.WriteLine("AVERAGE: " + e.Result);
            }
        }
    }
}