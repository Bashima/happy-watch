﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using HappyWatch.HWwcf;
using System.Diagnostics;

namespace HappyWatch
{
    public partial class graph_page : PhoneApplicationPage
    {
        int count = 0, count1 = 0;
        bool flag;
        double hp;
        List<string> str, str1;
        string Email;
        List<Person> graph1;
        protected override void OnNavigatedTo(System.Windows.Navigation.NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            var title = NavigationContext.QueryString["DeviceId"];

            Email = (String)title;
            Debug.WriteLine("DeviceId: " + Email);
            graphCall();


        }
        public void graphCall()
        {

            HappyWatchServiceClient proxy = new HappyWatchServiceClient("BasicHttpBinding_IHappyWatchService");
            proxy.FindPulseListCompleted += new EventHandler<FindPulseListCompletedEventArgs>(proxy_FindPulseListCompleted);
            proxy.FindPulseListAsync(Email);

        }


        void proxy_FindPulseListCompleted(object sender, FindPulseListCompletedEventArgs es)
        {

            Debug.WriteLine("DeviceId 2nd: " + Email);
            // Debug.WriteLine("graph here::  "+es.Result);
            count = 0;

            str = es.Result.ToList();

            flag = true;
            Debug.WriteLine("flag1: " + flag);
            foreach (string x in str)
            {
                
                Debug.WriteLine("flag2: " + flag);

                Constants.Bloodpulse[count++] = x;

               // Debug.WriteLine(Constants.Bloodpulse[count - 1]);
                Debug.WriteLine("count:  "+count);

                if (count == 60)
                {
                    break;
                }
            }
            flag = false;
            Debug.WriteLine("flag1: " + flag);
            Constants.CON = count;
            makeGraph();

        }
        public void makeGraph()
        {
            Constants.c = 1;
            graph1 = new List<Person>();
            Debug.WriteLine("CON: " + Constants.CON);
            for (int i = 0; i < Constants.CON &&Constants.Bloodpulse.Length>0; i++)
            {
                try
                {
                    Debug.WriteLine("Persons: ");
                    graph1.Add(new Person { time = (Constants.c).ToString(), bloodpulse = int.Parse(Constants.Bloodpulse[i]) });
                    Constants.c += 1;
                }
                catch (Exception ex)
                {
 
                }
                // Debug.WriteLine("matai: "+ " " + Class1.Time[i].ToString());


            }
            try
            {
                Constants.CON = 0;
                mychart.DataSource = graph1;
                progress_bar.IsIndeterminate = false;
            }

            catch (Exception ex)
            {
                Debug.WriteLine("erroor" + ex);

            }

        }

        public graph_page()
        {
            InitializeComponent();
            progress_bar.IsIndeterminate = true;

        }
    }
}