﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using HappyWatch.HWwcf;
using System.Diagnostics;

namespace HappyWatch
{
    public partial class Admin_Patients : PhoneApplicationPage
    {
        HappyWatchServiceClient proxy;
        public string Email { get; set; }
        public string Str { get; set; }
        public string[] MyArray = new string[10];
        public Admin_Patients()
        {

            InitializeComponent();
            proxy = new HappyWatchServiceClient("BasicHttpBinding_IHappyWatchService");
            progress_bar.IsIndeterminate = true;
        }
        protected override void OnNavigatedTo(System.Windows.Navigation.NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            var title = NavigationContext.QueryString["Email"];
            Str = (String)title;
            MyArray = Str.Split('$');
            Email = MyArray[0];
            No_Patients.Text = MyArray[1];
            proxy = new HappyWatchServiceClient("BasicHttpBinding_IHappyWatchService");
            MakePatientList();
        }
        private void MakePatientList()
        {
            proxy.HospitalPatientListCompleted += new EventHandler<HospitalPatientListCompletedEventArgs>(proxy_HospitalPatientListCompleted);
            proxy.HospitalPatientListAsync(Email);
 
        }

        void proxy_HospitalPatientListCompleted(object sender, HospitalPatientListCompletedEventArgs e)
        {
            progress_bar.IsIndeterminate = false;
            Debug.WriteLine("making patientlist: ");
            peoplesList.ItemsSource = e.Result.ToList();
        }

        private void MyContextMenu_Opened(object sender, RoutedEventArgs e)
        {
            //khali thakbe
        }

        private void delete_patient_clicked(object sender, RoutedEventArgs e)
        {
            //ekhane code boshbe
            MessageBox.Show("clicked");
            MessageBox.Show("clicked");
            string header = (sender as MenuItem).Header.ToString();
            Debug.WriteLine("doc said: " + header);
            ListBoxItem selectedListBoxItem = this.peoplesList.ItemContainerGenerator.ContainerFromItem((sender as MenuItem).DataContext) as ListBoxItem;
            ObPatient temp = (ObPatient)selectedListBoxItem.Content;
            if (selectedListBoxItem != null)
                Debug.WriteLine("Doc Mail: " + temp.pMail);
            else
                Debug.WriteLine("NULL");
            string sent = Email + "#" + temp.pMail;
            Debug.WriteLine("SENT: " + sent);
            proxy.AdminDeletePatientCompleted += new EventHandler<AdminDeletePatientCompletedEventArgs>(proxy_AdminDeletePatientCompleted);
            proxy.AdminDeletePatientAsync(sent);
        }

        void proxy_AdminDeletePatientCompleted(object sender, AdminDeletePatientCompletedEventArgs e)
        {
            Debug.WriteLine("Result: " + e.Result.ToString());
        }
    }
}