﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using HappyWatch.HWwcf;
using System.Diagnostics;
using System.IO.IsolatedStorage;

namespace HappyWatch
{
    public partial class Admin_Main : PhoneApplicationPage
    {
        public HappyWatchServiceClient proxy;
        public string Email { get; set; }
        bool pop_up_flag = false;
        string sent;
        public Admin_Main()
        {
            InitializeComponent();
            proxy = new HappyWatchServiceClient("BasicHttpBinding_IHappyWatchService");
            progress_bar.IsIndeterminate = true;
        }
        protected override void OnNavigatedTo(System.Windows.Navigation.NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            var title = NavigationContext.QueryString["Email"];
            Email = (String)title;
            proxy = new HappyWatchServiceClient("BasicHttpBinding_IHappyWatchService");
            HospitalInfo();
        }
        private void HospitalInfo()
        {
            proxy.HospitalInfoGetCompleted += new EventHandler<HospitalInfoGetCompletedEventArgs>(proxy_HospitalInfoGetCompleted);
            proxy.HospitalInfoGetAsync(Email);
            
        }

        void proxy_HospitalInfoGetCompleted(object sender, HospitalInfoGetCompletedEventArgs e)
        {
            progress_bar.IsIndeterminate = false;
            string[] ar = new string[15];
            ar = e.Result.ToString().Split('$');
            Debug.WriteLine("Hospital Info: ");
            Debug.WriteLine(e.Result.ToString());
            if (e.Result.ToString().Equals("N/A"))
            {
                Hospital_Name_text.Text = ar[0];
                Email_text.Text = "...";
                Address_text.Text = "...";
                Doctor_No_text.Text = "...";
                Patient_No_text.Text = "...";
                
            }
            else {
                Hospital_Name_text.Text = ar[0];
                Email_text.Text = ar[1];
                Address_text.Text = ar[2];
                Doctor_No_text.Text = ar[4];
                Patient_No_text.Text = ar[3];
                Password_text.Text = ar[5];
            }

        }
		private void request_button_click(object sender, System.Windows.Input.GestureEventArgs e)
        {
            
            NavigationService.Navigate(new Uri("/Admin_Request.xaml?Email="+Email, UriKind.Relative));
        }
        private void help_button_click(object sender, System.Windows.Input.GestureEventArgs e)
        {
            NavigationService.Navigate(new Uri("/InstructionPage.xaml", UriKind.Relative));
        }
		private void doctor_button_click(object sender, System.Windows.Input.GestureEventArgs e)
        {
            NavigationService.Navigate(new Uri("/Admin_Doctor.xaml?Email=" + Email+"$"+Doctor_No_text.Text.ToString(), UriKind.Relative));
        }
        private void patient_button_click(object sender, System.Windows.Input.GestureEventArgs e)
        {
            NavigationService.Navigate(new Uri("/Admin_Patients.xaml?Email=" + Email + "$" + Patient_No_text.Text.ToString(), UriKind.Relative));
        }
		private void about_button_click(object sender, System.Windows.Input.GestureEventArgs e)
		{
			NavigationService.Navigate(new Uri("/AboutPage.xaml", UriKind.Relative));

		}

		private void update_button_click(object sender, System.EventArgs e)
		{
			this.update_in.Begin();
            pop_up_flag = true;
		}

		private void update_done_button_tap(object sender, System.Windows.Input.GestureEventArgs e)
		{
            string address_update, password_update;
            address_update = address_box_up.Text;
            password_update = password_box_up.Text;
            if (address_update.Equals(""))
                address_update = Address_text.Text;
            if (password_update.Equals(""))
                password_update = Password_text.Text;
            sent = Email + "$" + address_update + "$" + password_update;
            progress_bar.IsIndeterminate = true;
            proxy.UpdateAdminInfoCompleted += new EventHandler<UpdateAdminInfoCompletedEventArgs>(proxy_UpdateAdminInfoCompleted);
            Debug.WriteLine("Mail: " + Email);
            proxy.UpdateAdminInfoAsync(sent); this.update_out.Begin();
            pop_up_flag = false;
		}
        void proxy_UpdateAdminInfoCompleted(object sender, UpdateAdminInfoCompletedEventArgs e)//update completed
        {
            progress_bar.IsIndeterminate = false;
            if (e.Result == true)
            {
                MessageBox.Show("update completed");
            }
            else
            {
                MessageBox.Show("update failed");
            }
            HospitalInfo();
        }
		private void update_cancel_button_tap(object sender, System.Windows.Input.GestureEventArgs e)
		{
			this.update_out.Begin();
            pop_up_flag = false;
		}
        protected override void OnBackKeyPress(System.ComponentModel.CancelEventArgs e)
        {
            //back key press
            MessageBoxResult m = MessageBox.Show("Are you sure?", "exit", MessageBoxButton.OKCancel);
            if (m == MessageBoxResult.Cancel)
            {
                e.Cancel = true;
            }
            else
            {
                if (pop_up_flag == true)
                {
                    this.update_out.Begin();
                    pop_up_flag = false;
                    e.Cancel = true;
                }
                else
                {
                    Application.Current.Terminate();
                }
            }
        }

        private void logout_button_click(object sender, EventArgs e)
        {

            IsolatedStorageSettings settings = IsolatedStorageSettings.ApplicationSettings;
            string cookie = settings[Email].ToString();
            while (this.NavigationService.BackStack.Any())
            {
                this.NavigationService.RemoveBackEntry();


            }
            settings[Email] = "";
            settings[cookie] = 0;
            settings.Remove(Email);
            settings.Remove(cookie);
            settings.Clear();
            Application.Current.Terminate();

        }
    }
}