﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using HappyWatch.UserInfoService;

namespace HappyWatch
{
    public partial class UserPage : PhoneApplicationPage
    {
        bool edit_state=false;
        string DeviceId;
        public UserPage()
        {
            InitializeComponent();
        }
        protected override void OnNavigatedTo(System.Windows.Navigation.NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            var title = NavigationContext.QueryString["DeviceId"];
            DeviceId = (String)title;
        }
        private void edit_button_click(object sender, System.EventArgs e)//press edit button to pop up the edit user info grid
        {
            this.popup_entry_anim.Begin();
            edit_state = true;
        }
        private void add_button_click(object sender, System.EventArgs e)//press update button to update info
        {
            this.pop_exit_anim.Begin();
            edit_state = false;
        }

        private void cancel_button_click(object sender, System.Windows.RoutedEventArgs e)//press cancel to exit edit pop up
        {
        	this.pop_out.Begin();
            edit_state = false;
        }
		protected override void OnBackKeyPress(System.ComponentModel.CancelEventArgs e)//handle edit pop using back button
        {
            if (edit_state==true)
            {
                this.pop_out.Begin();
                edit_state = false;
                e.Cancel = true;

            }
            
        }
        void userinfocall()//connecting to the wcf to get info
        {
            UserInfoService1Client proxy = new UserInfoService1Client();

            proxy.FindUserIdCompleted += new EventHandler<FindUserIdCompletedEventArgs>(proxy_FindUserIdCompleted);
            proxy.FindUserIdAsync(DeviceId);


            proxy.FindUserNameCompleted += new EventHandler<FindUserNameCompletedEventArgs>(proxy_FindUserNameCompleted);
            proxy.FindUserNameAsync(DeviceId);


            proxy.FindUserDobCompleted += new EventHandler<FindUserDobCompletedEventArgs>(proxy_FindUserDobCompleted);
            proxy.FindUserDobAsync(DeviceId);

            proxy.FindGenderCompleted += new EventHandler<FindGenderCompletedEventArgs>(proxy_FindGenderCompleted);
            proxy.FindGenderAsync(DeviceId);

            proxy.FindPConditionCompleted += new EventHandler<FindPConditionCompletedEventArgs>(proxy_FindPConditionCompleted);
            proxy.FindPConditionAsync(DeviceId);

        }
        void proxy_FindUserIdCompleted(object sender, FindUserIdCompletedEventArgs e)
        {
            DeviceId_text.Text = e.Result;

        }

        void proxy_FindUserNameCompleted(object sender, FindUserNameCompletedEventArgs e)
        {
            Name_text.Text = e.Result;

        }

        void proxy_FindUserDobCompleted(object sender, FindUserDobCompletedEventArgs e)
        {
            string dob = e.Result;
            Date_text.Text = dob;
        }
        void proxy_FindGenderCompleted(object sender, FindGenderCompletedEventArgs e)
        {
            Gender_text.Text = e.Result;

        }
        void proxy_FindPConditionCompleted(object sender, FindPConditionCompletedEventArgs e)
        {
            Condition_text.Text = e.Result;

        }
        
    }
}