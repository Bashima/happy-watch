﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using HappyWatch.HWwcf;
using System.Diagnostics;

namespace HappyWatch
{
    public partial class Observer_Info : PhoneApplicationPage
    {
        public HappyWatchServiceClient proxy;
        public string[] ar = new string[10];
        public string Email { get; set; }
        bool pop_up_flag = false;
        string sent;
        public Observer_Info()
        {
            InitializeComponent();
            proxy = new HappyWatchServiceClient("BasicHttpBinding_IHappyWatchService");
            progress_bar.IsIndeterminate = true;
         //   UserInfoCall();

        }
        protected override void OnNavigatedTo(System.Windows.Navigation.NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            var title = NavigationContext.QueryString["Email"];
            Email = title;
            UserInfoCall();
        }
        void UserInfoCall()
        {
            progress_bar.IsIndeterminate = true;
            proxy.ObserverInfoCompleted += new EventHandler<ObserverInfoCompletedEventArgs>(proxy_ObserverInfoCompleted);
            Debug.WriteLine("Email is: "+Email);
            proxy.ObserverInfoAsync(Email);
            
 
        }

        void proxy_ObserverInfoCompleted(object sender, ObserverInfoCompletedEventArgs e)
        {
            string res = e.Result;
            Debug.WriteLine("info :"+res);
            ar = res.Split('#');
            Name_text.Text = ar[0];
            Email_text.Text = ar[1];
            Date_text.Text = ar[2];
            Gender_text.Text = ar[3];
            contact_text.Text = ar[4];
            password_text.Text = ar[5];
            progress_bar.IsIndeterminate = false;
            //update_date_picker.Value = Convert.ToDateTime(ar[2]);
        }

        private void update_button_click(object sender, System.EventArgs e)
        {
        	// TODO: Add event handler implementation here.
            this.update_in.Begin();
            pop_up_flag = true;
        }

        private void cancell_button_click(object sender, System.Windows.RoutedEventArgs e)
        {
        	// TODO: Add event handler implementation here.
            this.update_out.Begin();
            pop_up_flag = false;
        }

        private void update_done_button_click(object sender, RoutedEventArgs e)
        {
            string up_name, up_password, up_contact, up_date;
            up_name = update_name_box.Text;
            up_password = update_password_box.Text;
            up_contact = update_contact_box.Text;
            up_date = String.Format("{0:yyyy-MM-dd}", update_date_picker.Value);
            if (up_date.Equals(String.Format("{0:yyyy-MM-dd}", DateTime.Now)))
            {
                up_date = Date_text.Text;
                Debug.WriteLine("here:::" + up_date);
            }
            if (up_name.Equals(""))
                up_name = Name_text.Text;
            if (up_password.Equals(""))
                up_password = password_text.Text;
            if (up_contact.Equals(""))
                up_contact = contact_text.Text;

            sent = Email+"$"+up_name + "$" + up_password + "$" + up_contact + "$" + up_date;
            progress_bar.IsIndeterminate = true;
            proxy.UpdateObserverInfoCompleted += new EventHandler<UpdateObserverInfoCompletedEventArgs>(proxy_UpdateObserverInfoCompleted);
            Debug.WriteLine("Mail: " + Email);
            proxy.UpdateObserverInfoAsync(sent);
            this.update_out.Begin();
            pop_up_flag = false;
        }

        void proxy_UpdateObserverInfoCompleted(object sender, UpdateObserverInfoCompletedEventArgs e)//update completed
        {
            progress_bar.IsIndeterminate = false;
            if (e.Result == true)
            {
                MessageBox.Show("update completed");
            }
            else
            {
                MessageBox.Show("update failed");
            }
            UserInfoCall();
        }
        protected override void OnBackKeyPress(System.ComponentModel.CancelEventArgs e)
        {
            if (pop_up_flag == true)
            {
                this.update_out.Begin();
                pop_up_flag = false;
                e.Cancel = true;
            }
        }
    }
}