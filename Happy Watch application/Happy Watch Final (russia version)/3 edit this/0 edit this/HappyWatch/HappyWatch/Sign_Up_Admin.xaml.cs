﻿using System;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Microsoft.Xna.Framework;
using System.IO.IsolatedStorage;
using Microsoft.Phone.Scheduler;
using System.Diagnostics;
using HappyWatch.HWwcf;

namespace HappyWatch
{
    public partial class Sign_Up_Admin : PhoneApplicationPage
    {
		string AName, Email,Password,Address, Designation, sent, warning,RePass;
        public HappyWatchServiceClient proxy;
        public Sign_Up_Admin()
        {
            InitializeComponent();
            loading.Visibility = Visibility.Collapsed;
        }

        private void sign_up_admin(object sender, System.Windows.Input.GestureEventArgs e)
        {
            warning = "";
            AName = name_box_up.Text;
            Email = email_box_up.Text;
            String Pword = password_box_up.Text;
            RePass = re_password_box_up.Text;
            if ((Pword.Equals(RePass)))
            {
                Password = password_box_up.Text;
            }
            else
            {
                Password = "";
                Microsoft.Xna.Framework.GamerServices.Guide.BeginShowMessageBox("Warning", "retype same password", new string[] { "ok" }, 0, Microsoft.Xna.Framework.GamerServices.MessageBoxIcon.Alert, null, null);
            }
            Address = address_box_up.Text;
            Designation = "admin";
            if (AName.Equals(""))
            {
                warning = "insert name";

            }
            else if (IsValidEmail(Email) == false)
            {
                warning = "insert valid email address";
            }
            else if (Email.Equals(""))
            {
                warning = "insert email address";

            }
            else if (Address.Equals(""))
            {
                warning = "insert address";
            }
            else if (Password.Equals(""))
            {
                warning = "enter password";
            }
            else if (RePass.Equals(""))
            {
                warning = "retype password";
            }
            if (warning.Equals(""))
            {
                string temp;
                temp = Email + "," + "active";
                prog.IsIndeterminate = true;
                loading.Visibility = Visibility.Visible;
                proxy = new HappyWatchServiceClient("BasicHttpBinding_IHappyWatchService");
                proxy.VerifyEmailCompleted += new EventHandler<VerifyEmailCompletedEventArgs>(proxy_VerifyEmailCompleted);
                proxy.VerifyEmailAsync(temp);

                
            }
            else//verifying if the given deviceid is new
            {
                Microsoft.Xna.Framework.GamerServices.Guide.BeginShowMessageBox("Warning", warning, new string[] { "ok" }, 0, Microsoft.Xna.Framework.GamerServices.MessageBoxIcon.Alert, null, null);
            }
        }
        void proxy_VerifyEmailCompleted(object sender, VerifyEmailCompletedEventArgs e)
        {
            prog.IsIndeterminate = false;
            loading.Visibility = Visibility.Collapsed;
            Debug.WriteLine("Result is: " + e.Result);
            if (e.Result.Equals("N/A"))
            {
                proxy = new HappyWatchServiceClient("BasicHttpBinding_IHappyWatchService");
                sent = AName + "$" + Email + "$" + Address + "$" + Password;
                Debug.WriteLine(sent + "sdfa");
                loading.Visibility = Visibility.Visible;
                prog.IsIndeterminate = true;
                proxy.InsertInfoAdminCompleted += new EventHandler<InsertInfoAdminCompletedEventArgs>(proxy_InsertInfoAdminCompleted);
                proxy.InsertInfoAdminAsync(sent);

            }
            else
            {
                prog.IsIndeterminate = false;
                loading.Visibility = Visibility.Collapsed;
                Microsoft.Xna.Framework.GamerServices.Guide.BeginShowMessageBox("Error", "email id already exists", new string[] { "ok" }, 0, Microsoft.Xna.Framework.GamerServices.MessageBoxIcon.Alert, null, null);
                //     progress_bar.IsIndeterminate = false;

            }
        }
		void proxy_InsertInfoAdminCompleted(object sender, InsertInfoAdminCompletedEventArgs e)
        {
            prog.IsIndeterminate = false;
            loading.Visibility = Visibility.Collapsed;
            Debug.WriteLine(e.Result);
            if (e.Result.Equals("true"))
            {
                Debug.WriteLine("signed up");
                NavigationService.Navigate(new Uri("/Admin_Main.xaml?Email=" + Email, UriKind.Relative));
            }
            else
                Microsoft.Xna.Framework.GamerServices.Guide.BeginShowMessageBox("Error", "error while signing up", new string[] { "ok" }, 0, Microsoft.Xna.Framework.GamerServices.MessageBoxIcon.Alert, null, null);
        }
        public static bool IsValidEmail(string strIn)
        {
            // Return true if strIn is in valid e-mail format.
            return Regex.IsMatch(strIn,
                   @"^(?("")("".+?""@)|(([0-9a-zA-Z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-zA-Z])@))" +
                   @"(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-zA-Z][-\w]*[0-9a-zA-Z]\.)+[a-zA-Z]{2,6}))$");
        }
    }
}