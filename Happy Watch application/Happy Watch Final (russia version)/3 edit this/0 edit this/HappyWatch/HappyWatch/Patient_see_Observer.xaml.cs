﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using HappyWatch.HWwcf;
using System.Diagnostics;

namespace HappyWatch
{
    public partial class Patient_see_Observer : PhoneApplicationPage
    {
        public HappyWatchServiceClient proxy = new HappyWatchServiceClient("BasicHttpBinding_IHappyWatchService");
        public string Email { get; set; }
        public Patient_see_Observer()
        {
            InitializeComponent();
            progress_bar.IsIndeterminate = true;
            MakeList();
        }
        protected override void OnNavigatedTo(System.Windows.Navigation.NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            var title = NavigationContext.QueryString["Email"];

            Email = (String)title;
            Debug.WriteLine("Email: " + Email);
            Constants.PEmail = Email;
            progress_bar.IsIndeterminate = true;
            MakeList();
            


        }
        public void MakeAllObList()
        {
            Debug.WriteLine("called");
            progress_bar.IsIndeterminate = true;
            proxy.AllObserversListCompleted += new EventHandler<AllObserversListCompletedEventArgs>(proxy_AllObserversListCompleted);
            proxy.AllObserversListAsync(Email);
        }

        void proxy_AllObserversListCompleted(object sender, AllObserversListCompletedEventArgs e)
        {
            Debug.WriteLine("Making all observer list");
            ObserverList.ItemsSource = e.Result.ToList();
            List<string> obs = new List<string>();
            foreach (ObPatient item in e.Result.ToList())
            {
                obs.Add(item.pMail);
            }
            auto_box.ItemsSource = obs;
            progress_bar.IsIndeterminate = false;

        }
       

        private void Panorama_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            switch (((Panorama)sender).SelectedIndex)
            {
                case 0: // defines the first PanoramaItem
                    //making own observerlist
                    progress_bar.IsIndeterminate = true;
                    MakeList();
                    break;

                case 1: // second one
                    //making all observer list
                    progress_bar.IsIndeterminate = true;
                    MakeAllObList();
                    break;

            }
        }
        public void MakeList()
        {
            progress_bar.IsIndeterminate = true;
            proxy.ObserverListCompleted += new EventHandler<ObserverListCompletedEventArgs>(proxy_ObserverListCompleted);
            proxy.ObserverListAsync(Email);
        }

        private void proxy_ObserverListCompleted(object sender, ObserverListCompletedEventArgs e)
        {
            Debug.WriteLine("Making List:");
            peoplesList.ItemsSource = e.Result.ToList();
            progress_bar.IsIndeterminate = false;
        }

        private void hint_tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
        	string temp;
			temp= auto_box.Text;
			if(temp.Equals("search by email"))
				auto_box.Text="";
        }


        private void AddButton_Click(object sender, RoutedEventArgs e)
        {
            progress_bar.IsIndeterminate = true;
            string ObMail=auto_box.Text;
            Debug.WriteLine("String is: " + ObMail);
            string sent = Email + "#" + ObMail + "#" + "Relation";
            proxy.MakeRelationObRequestCompleted += new EventHandler<MakeRelationObRequestCompletedEventArgs>(proxy_MakeRelationObRequestCompleted);
            proxy.MakeRelationObRequestAsync(sent);

            

        }

        private void delete_observer_clicked(object sender, System.EventArgs e)
        {
            //ekhane code boshbe
           // MessageBox.Show("clicked");
            //MessageBox.Show("clicked");
            string header = (sender as MenuItem).Header.ToString();
            Debug.WriteLine("doc said: " + header);
            ListBoxItem selectedListBoxItem = this.peoplesList.ItemContainerGenerator.ContainerFromItem((sender as MenuItem).DataContext) as ListBoxItem;
            ObPatient temp = (ObPatient)selectedListBoxItem.Content;
            if (selectedListBoxItem != null)
                Debug.WriteLine("Doc Mail: " + temp.pMail);
            else
                Debug.WriteLine("NULL");
            string sent = Email + "#" + temp.pMail;
            Debug.WriteLine("SENT: " + sent);
            proxy.PateintDeleteObserverCompleted += new EventHandler<PateintDeleteObserverCompletedEventArgs>(proxy_PateintDeleteObserverCompleted);
            proxy.PateintDeleteObserverAsync(sent);
        }

        void proxy_PateintDeleteObserverCompleted(object sender, PateintDeleteObserverCompletedEventArgs e)
        {
            Debug.WriteLine("Result: " + e.Result.ToString());
        }
        void proxy_MakeRelationObRequestCompleted(object sender, MakeRelationObRequestCompletedEventArgs e)
        {
            Debug.WriteLine("Result of make relatio\n:" + e.Result.ToString());
            progress_bar.IsIndeterminate = false;

            if (e.Result == true)
            {
                MessageBox.Show("request sent");

            }
            else
            {
                MessageBox.Show("error while sending request");
 
            }
            progress_bar.IsIndeterminate = false;
        }

        private void MyContextMenu_Opened(object sender, RoutedEventArgs e)
        {
            //khali thakbe
        }

        private void add_observer_clicked(object sender, System.EventArgs e)
        {
            string header = (sender as MenuItem).Header.ToString();
            Debug.WriteLine("doc said: " + header);
            ListBoxItem selectedListBoxItem = this.ObserverList.ItemContainerGenerator.ContainerFromItem((sender as MenuItem).DataContext) as ListBoxItem;
            ObPatient temp = (ObPatient)selectedListBoxItem.Content;
            if (selectedListBoxItem != null)
                Debug.WriteLine("Doc Mail: " + temp.pMail);
            else
                Debug.WriteLine("NULL");
            string sent = Email + "#" + temp.pMail+"#" + "Relation";;
            Debug.WriteLine("SENT: " + sent);
            proxy.MakeRelationObRequestCompleted += new EventHandler<MakeRelationObRequestCompletedEventArgs>(proxy_MakeRelationObRequestCompleted);
            proxy.MakeRelationObRequestAsync(sent);

 
        }
    }
}