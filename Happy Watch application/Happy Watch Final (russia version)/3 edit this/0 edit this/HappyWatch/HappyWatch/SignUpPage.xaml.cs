﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;

namespace HappyWatch
{
    public partial class SignUpPage : PhoneApplicationPage
    {
        public SignUpPage()
        {
            InitializeComponent();
        }

        private void tap_next_button(object sender, System.Windows.Input.GestureEventArgs e)
        {
        	// TODO: Add event handler implementation here.
            if (Convert.ToBoolean(device_user.IsChecked))
			NavigationService.Navigate(new Uri("/SignUpUserPage.xaml", UriKind.Relative));
			if (Convert.ToBoolean(observer_box.IsChecked))
			NavigationService.Navigate(new Uri("/SignUpObserverPage.xaml", UriKind.Relative));
			if (Convert.ToBoolean(doctor_box.IsChecked))
			NavigationService.Navigate(new Uri("/SignUpDoctorPage.xaml", UriKind.Relative));
            if (Convert.ToBoolean(admin_box.IsChecked))
                NavigationService.Navigate(new Uri("/Sign_Up_Admin.xaml", UriKind.Relative));

        }
    }
}