﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.Collections.ObjectModel;
using System.IO;
using System.IO.IsolatedStorage;
using System.Diagnostics;
namespace HappyWatch
{
    public partial class PeoplePage : PhoneApplicationPage
    {
        IsolatedStorageFile myIsolatedStorage = IsolatedStorageFile.GetUserStoreForApplication();
        
        string line;
        string[] str = new string[3];
        ObservableCollection<People> peopleList;
           

        
        string[] names = { "Sajid Hasan","Trina Mukharjee","Raina Islam","Liana Binte Rashid","Ifteqar Asad"};
        string[] phones = {"01711529426","01911256128","01674303571","01914252354","01819324057" };
        string[] email = {"sajid_hasan@live.com","t_mukharjee@rocketmail.com","iriana@gmail.com","l.b.rashid@yahoo.com","asad2013@hotmail.com" };
        public PeoplePage()
        {
            InitializeComponent();
          
            
           // generateFakeData();
        }
        protected override void OnNavigatedTo(System.Windows.Navigation.NavigationEventArgs e)
        {
          
            IsolatedStorageFile myIsolatedStorage = IsolatedStorageFile.GetUserStoreForApplication();
            IsolatedStorageFileStream fileStream = myIsolatedStorage.OpenFile("myFile.txt", FileMode.OpenOrCreate, FileAccess.Read);
            using (StreamReader reader = new StreamReader(fileStream))
            {    //Visualize the text data in a TextBlock text
                peopleList = new ObservableCollection<People>();

                while ((line = reader.ReadLine()) != null)
                {
                    People aPeople = new People();
                      
                   
                    str = line.Split(',');
                    Debug.WriteLine("People: "+str[0]+" "+str[1]+" "+str[2]);
                    aPeople.name = str[0];
                    aPeople.phone = str[1];
                    aPeople.email = str[2];
                    Debug.WriteLine("Counter: "+Constants.counter+" "+Constants.AddCounter);
                    
                    
                        peopleList.Add(aPeople);
                        Console.WriteLine(line);
                        Constants.counter++;
                    
                }

                
            }
            this.peoplesList.ItemsSource = peopleList;
            Debug.WriteLine(Constants.counter);
            
 
        }

		
		private void generateFakeData()
		{
            for (int i = 0; i < 5; i++)
            {
                People aPeople = new People();
                aPeople.name = names[i];
                aPeople.phone=phones[i];
                aPeople.email = email[i];
                peopleList.Add(aPeople);
            }
		}

        private void add_click(object sender, EventArgs e)
        {
            NavigationService.Navigate(new Uri("/AddPeople.xaml", UriKind.Relative));
        }

        private void ApplicationBarMenuItem_Click_1(object sender, EventArgs e)
        {
            NavigationService.Navigate(new Uri("/About Us Page.xaml", UriKind.Relative));
        }

        private void ApplicationBarMenuItem_Click_2(object sender, EventArgs e)
        {
            NavigationService.Navigate(new Uri("/InstructionPage.xaml", UriKind.Relative));
        }

        private void ApplicationBarMenuItem_Click_3(object sender, EventArgs e)
        {
            NavigationService.Navigate(new Uri("/SettingPage.xaml", UriKind.Relative));
        }

       
    }

    public class People
    {
        public string name { get; set; }
        public string phone { get; set; }
        public string email { get; set; }
    }
}