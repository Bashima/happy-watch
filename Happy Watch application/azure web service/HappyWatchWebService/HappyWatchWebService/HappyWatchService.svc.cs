﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using System.Net;
using System.IO;
using System.Security.Cryptography;
namespace HappyWatchWebService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "HappyWatchService" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select HappyWatchService.svc or HappyWatchService.svc.cs at the Solution Explorer and start debugging.
    public class HappyWatchService : IHappyWatchService
    {
        public string InsertInfoPatient(string uid)
        {

            try
            {
                using (HappyWatchDataDataContext context = new HappyWatchDataDataContext())
                {                    
                    string[] ar = new string[20];
                    ar = uid.Split('#');
                  /*  var verify = context.Peoples.Any(r => r.Email.Equals(ar[6]) && r.Status.Equals("inactive"));
                    if (verify)
                    {
                        var people_is = ((from r in context.Peoples
                                          where r.Email.Equals(uid)
                                          select r
                                ).Take(1)).Single();
                        people_is.Status = "active";
                        context.SubmitChanges();
                        var peopleId = ((from r in context.Peoples
                                         where r.Email.Equals(uid)
                                         select r.id
                       ).Take(1)).Single();
                        var patientId = ((from r in context.Patients
                                          where r.Person_id == peopleId
                                          select r.id
                       ).Take(1)).Single();
                        var resd = context.PatientVSDoctors.Any(r => r.PatientPatientId == patientId);
                        if (resd)
                        {
                            var patientvsdoctor = ((from r in context.PatientVSDoctors
                                                    where patientId == r.PatientPatientId
                                                    select r
                       ).Take(1)).Single();
                            patientvsdoctor.PDStatus = "active";
                            context.SubmitChanges();
                        }
                        var res1 = context.PatientVSHospitals1s.Any(r => r.PatientPatientId == patientId);
                        if (res1)
                        {
                            var patientvsHospital = ((from r in context.PatientVSHospitals1s
                                                      where r.PatientPatientId == patientId
                                                      select r
                       ).Take(1)).Single();
                            patientvsHospital.PHStatus = "active";
                            context.SubmitChanges();
                        }

                        var res2 = context.PatientVSObservers.Any(r => r.PatientPatientId == patientId);
                        if (res2)
                        {
                            var patientvsObserver = ((from r in context.PatientVSObservers
                                                      where r.PatientPatientId == patientId
                                                      select r
                      ).Take(1)).Single();
                            patientvsObserver.POStatus = "active";
                            context.SubmitChanges();
                        }
                        var verifydevice = context.Patients.Any(r => r.DeviceId.Equals(ar[8]));
                        if (verifydevice)
                        {
                            ;
                        }
                        else
                        {
                            var patient_is = ((from r in context.Patients
                                               where r.Person_id == peopleId
                                               select r
                       ).Take(1)).Single();
                            patient_is.DeviceId = ar[8];
                            context.SubmitChanges();

                        }


                    }
                    else*/
                    {
                        var hospitalId = 0;
                        People data = new People();
                        data.Name = ar[0];
                        data.Password = ar[1];
                        data.BirthDay = Convert.ToDateTime(ar[2]);
                        data.Gender = ar[3];
                        data.ContactNumber = ar[4];
                        data.Designation = ar[5];
                        data.Email = ar[6];
                        data.Status = ar[7];
                        data.Channel = ar[8];
                        data.Address = ar[11];
                        context.Peoples.InsertOnSubmit(data);
                        context.SubmitChanges();

                        var pepId = (from r in context.Peoples
                                     where (r.Email.Equals(data.Email) && r.Password.Equals(data.Password))
                                     select r.id).Single();
                        try
                        {
                            hospitalId = ((from r in context.Hospitals
                                           where r.Name.Equals(ar[9])
                                           select r.id).Take(1)).Single();
                        }
                        catch (Exception ex)
                        {
                            hospitalId = 0;
                        }

                        Patient temp = new Patient();
                        temp.DeviceId = ar[8];

                        temp.PhysicalCondition = ar[10];
                        temp.Person_id = pepId;
                        temp.Isactive = data.Status;

                        context.Patients.InsertOnSubmit(temp);
                        context.SubmitChanges();

                    }

                } 
                return "true";
            }
            catch(Exception ex)
            {
                return ex.ToString();
            }

        }
        
        public Boolean InsertInfoObserver(string uid)
        {
            try
            {
                using (HappyWatchDataDataContext context = new HappyWatchDataDataContext())
                {
                    string[] ar = new string[10];
                    ar = uid.Split(',');
                    People data = new People();
                    data.Name = ar[0];
                    data.Password = ar[1];
                    data.BirthDay = Convert.ToDateTime(ar[2]);
                    data.Gender = ar[3];
                    data.ContactNumber = ar[4];
                    data.Designation = ar[5];
                    data.Email = ar[6];
                    data.Status = ar[7];
                    data.Channel = ar[8];
                    context.Peoples.InsertOnSubmit(data);
                    context.SubmitChanges();
                    var obId = (from r in context.Peoples
                                where (r.Email.Equals(data.Email) && r.Password.Equals(data.Password))
                                select r.id).Single();

                    Observer temp = new Observer();
                    temp.Person_id = obId;
                    context.Observers.InsertOnSubmit(temp);
                    context.SubmitChanges();

                } return true;
            }
            catch
            {
                return false;
            }


        }

        public string InsertInfoDoctor(string uid)
        {
            try
            {
                using (HappyWatchDataDataContext context = new HappyWatchDataDataContext())
                {
                    string[] ar = new string[10];
                    var hospitalId=0;
                    ar = uid.Split(',');
                    People data = new People();
                    data.Name = ar[0];
                    data.Password = ar[1];
                    data.BirthDay = Convert.ToDateTime(ar[2]);
                    data.Gender = ar[3];
                    data.ContactNumber = ar[4];
                    data.Designation = ar[5];
                    data.Email = ar[6];
                    data.Status = ar[7];
                    data.Channel = ar[8];
                    context.Peoples.InsertOnSubmit(data);
                    context.SubmitChanges();

                    Doctor doc = new Doctor();
                    doc.HospitalName = ar[9];
                    doc.LisenceId = ar[8];
                    doc.Specialist = ar[10];
                    try
                    {
                        hospitalId = ((from r in context.Hospitals
                                       where r.Name.Equals(ar[9])
                                       select r.id).Take(1)).Single();
                    }
                    catch (Exception ex)
                    {
                        hospitalId = 1;
                    }
                    doc.HospitalId = hospitalId;
                    var docPepId = (from r in context.Peoples 
                                    where (r.Email.Equals(data.Email) && r.Password.Equals(data.Password)) 
                                    select r.id).Single();
                    doc.Person_id = docPepId;
                    doc.DoctorStatus = "pending";
                    context.Doctors.InsertOnSubmit(doc);
                    context.SubmitChanges();

                    DocSignReq req = new DocSignReq();
                    req.Status = "pending";

                    var docId=((from r in context.Doctors 
                                where r.Person_id==docPepId 
                                select r.id).Take(1)).Single();
                    req.Doctor_id = docId;
                    req.HospitalId = hospitalId;
                    context.DocSignReqs.InsertOnSubmit(req);
                    context.SubmitChanges();





                   

                } return "true";
            }
            catch(Exception ex)
            {
                return ex.ToString();
            }


        }


        public string InsertInfoAdmin(string uid)
        {
            string a="null";
            string[] ar = new string[10];
            try
            {
                HappyWatchDataDataContext context = new HappyWatchDataDataContext();
                
                ar = uid.Split('$');
                Hospital hosp = new Hospital();
                hosp.Name = ar[0];
                hosp.Mail = ar[1];
                hosp.Address = ar[2];
                hosp.Password = ar[3];
                context.Hospitals.InsertOnSubmit(hosp);
                
                context.SubmitChanges();
                a = hosp.id.ToString();
                return "true";
                             
                            
            }
            catch (Exception ex)
            {
                return a;
            }

        }

        public string VerifyEmail(string uid)
        {
            string[] str = new string[5];
            using (HappyWatchDataDataContext context = new HappyWatchDataDataContext())
            {
                try
                {
                    str = uid.Split(',');
                    var res = ((from r in context.Peoples
                                where ((r.Email.Equals(str[0]))&& ((r.Status).Equals(str[1])))
                                select r.Designation).Take(1)).Single();
                    return res.ToString();
                }
                catch
                {
                    str = uid.Split(',');
                    /* var res1 = ((from r in context.Hospitals
                                  where (r.Mail.Equals(str[0])) && (r.Password.Equals(str[1]))
                                  select r.id).Take(1)).Single();*/
                    //return "admin";
                    var res = context.Hospitals.Any(r => r.Mail.Equals(str[0]));
                    if (res)
                        return "admin";
                    else
                        return "N/A";



                }
            }
        }
        public string VerifyDeviceId(string uid)
        {
            string[] str = new string[5];
            str = uid.Split(',');
            using (HappyWatchDataDataContext context = new HappyWatchDataDataContext())
            {

                    var res = context.Patients.Any(r => (r.DeviceId.Equals(str[0]) && r.Isactive.Equals("active")));
                    if (res)
                        return "true";
                    else
                        return "false";



                
            }
        }
        public string VerifyDoctorId(string uid)
        {
            string[] str = new string[5];
            str = uid.Split(',');
            using (HappyWatchDataDataContext context = new HappyWatchDataDataContext())
            {

                var res = context.Doctors.Any(r => (r.LisenceId.Equals(str[0]) && r.DoctorStatus.Equals(str[1])));
                if (res)
                    return "true";
                else
                    return "false";




            }
        }
        public string VerifyId(string uid)
        {
            bool flag=false;
            string[] str = new string[10]; 
            using (HappyWatchDataDataContext context = new HappyWatchDataDataContext())
            {

                DateTime time = DateTime.Now;
                
                    try
                    {
                        str = uid.Split(',');
                        var res = ((from r in context.Peoples
                                    where ((r.Email.Equals(str[0])) && ((r.Password).Equals(str[1])) && ((r.Status).Equals(str[2])))
                                    select r.Designation).Take(1)).Single();


                        return res.ToString()+" cookie: "+GenerateMD5(time.ToLongDateString());
                    }
                    catch
                    {
                        str = uid.Split(',');
                       /* var res1 = ((from r in context.Hospitals
                                     where (r.Mail.Equals(str[0])) && (r.Password.Equals(str[1]))
                                     select r.id).Take(1)).Single();*/
                        //return "admin";
                        var res = context.Hospitals.Any(r => r.Mail.Equals(str[0]) && r.Password.Equals(str[1]));
                        if (res)
                            return "admin"+" cookie: "+GenerateMD5(time.ToLongDateString());
                        else
                            return "N/A";
                       
                        

                    }
                    

               
                
            }
        }

        public List<ObPatient> ObservedList(string uid)
        {
            HappyWatchDataDataContext context = new HappyWatchDataDataContext();
            
            List<ObPatient> observed = new List<ObPatient>();
            try
            {

                //  string[] str = new string[10];
                //str = uid.Split(',');


                var oPeopleId = ((from r in context.Peoples
                                 where ((r.Email.Equals(uid)))
                                 select r.id).Take(1)).Single();

                var oObserverId = ((from r in context.Observers where (r.Person_id == oPeopleId) select r.id).Take(1)).Single();

                var patientIdList = from r in context.PatientVSObservers where (r.ObserverObserverId == oObserverId && r.POStatus.Equals("active")) select r.PatientPatientId;

                foreach (var pId in patientIdList)
                {
                    ObPatient tempOb = new ObPatient();
                    var pPeopleId = ((from r in context.Patients where (r.id == pId) select r.Person_id).Take(1)).Single();
                    tempOb.pName = ((from r in context.Peoples where (r.id == pPeopleId) select r.Name).Take(1)).Single();
                    tempOb.pMail = ((from r in context.Peoples where (r.id == pPeopleId) select r.Email).Take(1)).Single();
                    observed.Add(tempOb);

                }
            }
            catch (Exception ex)
            {
                ////////////////////////////////put code here
            }
            return observed.ToList();

        }

        public List<ObPatient> OwnPatientList(string uid)
        {
            List<ObPatient> pat = new List<ObPatient>();
            try
            {
                HappyWatchDataDataContext context = new HappyWatchDataDataContext();
                var pepId = ((from r in context.Peoples
                              where r.Email.Equals(uid)
                              select r.id).Take(1)).Single();
                var docId=((from r in context.Doctors
                              where r.Person_id==pepId
                              select r.id).Take(1)).Single();
                var PatientIds = from r in context.PatientVSDoctors
                                 where (r.DoctorDoctorId == docId && r.PDStatus.Equals("active"))
                                 select r.PatientPatientId;

                foreach (var pId in PatientIds)
                {
                    ObPatient tempOb = new ObPatient();
                    var pPeopleId = (from r in context.Patients where (r.id == pId) select r.Person_id).Single();
                    tempOb.pName = (from r in context.Peoples where (r.id == pPeopleId) select r.Name).Single();
                    tempOb.pMail = (from r in context.Peoples where (r.id == pPeopleId) select r.Email).Single();
                    pat.Add(tempOb);

                }

            }
            catch (Exception ex)
            {
                ////////do some
            }
            return pat.ToList();
        }

        public List<ObPatient> ObserverList(string uid)
        {
            HappyWatchDataDataContext context = new HappyWatchDataDataContext();
            ObPatient tempOb = new ObPatient();
            List<ObPatient> observed = new List<ObPatient>();
            try
            {

                string[] str = new string[10];
                str = uid.Split(',');


                var pPeopleId = (from r in context.Peoples
                                 where (r.Email.Equals(str[0]))
                                 select r.id).Single();

                var pPatientId = (from r in context.Patients where (r.Person_id == pPeopleId) select r.id).Single();

                var observerIdList = from r in context.PatientVSObservers
                                     where (r.PatientPatientId == pPatientId && r.POStatus.Equals("active"))
                                     select r.ObserverObserverId;

                foreach (var pId in observerIdList)
                {
                    var pObserverId = (from r in context.Observers where (r.id == pId) select r.Person_id).Single();
                    tempOb.pName = (from r in context.Peoples where (r.id == pObserverId) select r.Name).Single();
                    tempOb.pMail = (from r in context.Peoples where (r.id == pObserverId) select r.Email).Single();
                    observed.Add(tempOb);

                }
            }
            catch (Exception ex)
            {
                ////////////////////////////////put code here
            }
            return observed.ToList();


        }
        public string GetPulse(string uid)
        {
            try
            {
                HappyWatchDataDataContext context = new HappyWatchDataDataContext();


                var pepId = (from r in context.Peoples where r.Email.Equals(uid) select r.id).Single();
                var patientId = (from r in context.Patients where r.Person_id == pepId select r.id).Single();
                var bloodPulse = ((from r in context.HealthDatas
                                   orderby r.id descending
                                   where r.PatientPatientId == patientId
                                   select r.PulseRate).Take(1)).Single();

                return bloodPulse;
            }
            catch (Exception ex)
            {
                return "N/A";
            }


        }

        public string FindPulseMax(string uid)
        {
            DateTime x = DateTime.Now;
            try
            {
                HappyWatchDataDataContext context = new HappyWatchDataDataContext();

                var pepId = (from r in context.Peoples where r.Email.Equals(uid) select r.id).Single();
                var patientId = (from r in context.Patients where r.Person_id == pepId select r.id).Single();
                var res = from r in context.HealthDatas
                          where (r.PatientPatientId == patientId && ((r.Time.Value.Day).Equals(x.Day)) && ((r.Time.Value.Month).Equals(x.Month)) && ((r.Time.Value.Year).Equals(x.Year)))
                          select Convert.ToDouble(r.PulseRate);
                // var res = (from r in context.HealthDatas where r.PatientPatientId == patientId select r.Time.Day).Single();
                var resMax = res.Max();
                return resMax.ToString();
                //return uid;

            }
            catch (Exception e)
            {
                return "N/A";

            }
        }
        public string FindPulseMin(string uid)
        {
            DateTime x = DateTime.Now;
            try
            {
                HappyWatchDataDataContext context = new HappyWatchDataDataContext();

                var pepId = (from r in context.Peoples where r.Email.Equals(uid) select r.id).Single();
                var patientId = (from r in context.Patients where r.Person_id == pepId select r.id).Single();
                var res = from r in context.HealthDatas
                          where (r.PatientPatientId == patientId && ((r.Time.Value.Day).Equals(x.Day)) && ((r.Time.Value.Month).Equals(x.Month)) && ((r.Time.Value.Year).Equals(x.Year)))
                          select Convert.ToDouble(r.PulseRate);
                var resMin = res.Min();
                return resMin.ToString();

            }
            catch (Exception e)
            {
                return "N/A";

            }
        }

        public string FindPulseAv(string uid)
        {
            DateTime x = DateTime.Now;
            try
            {
                HappyWatchDataDataContext context = new HappyWatchDataDataContext();

                var pepId = (from r in context.Peoples where r.Email.Equals(uid) select r.id).Single();
                var patientId = (from r in context.Patients where r.Person_id == pepId select r.id).Single();
                var res = from r in context.HealthDatas
                          where (r.PatientPatientId == patientId && ((r.Time.Value.Day).Equals(x.Day)) && ((r.Time.Value.Month).Equals(x.Month)) && ((r.Time.Value.Year).Equals(x.Year)))
                          select Convert.ToDouble(r.PulseRate);
                double resAv = res.Average();
                return resAv.ToString();

            }
            catch (Exception e)
            {
                return "N/A";

            }
        }

        public string FindPulseMaxMonth(string uid)
        {
            DateTime x = DateTime.Now;
            try
            {
                HappyWatchDataDataContext context = new HappyWatchDataDataContext();

                var pepId = (from r in context.Peoples where r.Email.Equals(uid) select r.id).Single();
                var patientId = (from r in context.Patients where r.Person_id == pepId select r.id).Single();
                var res = from r in context.HealthDatas
                          where (r.PatientPatientId == patientId && ((r.Time.Value.Month).Equals(x.Month)) && ((r.Time.Value.Year).Equals(x.Year)))
                          select Convert.ToDouble(r.PulseRate);
                double resMax = res.Max();
                return resMax.ToString();

            }
            catch (Exception e)
            {
                return "N/A";

            }

        }

        public string FindPulseMinMonth(string uid)
        {
            DateTime x = DateTime.Now;
            try
            {
                HappyWatchDataDataContext context = new HappyWatchDataDataContext();

                var pepId = (from r in context.Peoples where r.Email.Equals(uid) select r.id).Single();
                var patientId = (from r in context.Patients where r.Person_id == pepId select r.id).Single();
                var res = from r in context.HealthDatas
                          where (r.PatientPatientId == patientId && ((r.Time.Value.Month).Equals(x.Month)) && ((r.Time.Value.Year).Equals(x.Year)))
                          select Convert.ToDouble(r.PulseRate);
                double resMin = res.Min();
                return resMin.ToString();

            }
            catch (Exception e)
            {
                return "N/A";

            }

        }

        public string FindPulseAvMonth(string uid)
        {
            DateTime x = DateTime.Now;
            try
            {
                HappyWatchDataDataContext context = new HappyWatchDataDataContext();

                var pepId = (from r in context.Peoples where r.Email.Equals(uid) select r.id).Single();
                var patientId = (from r in context.Patients where r.Person_id == pepId select r.id).Single();
                var res = from r in context.HealthDatas
                          where (r.PatientPatientId == patientId && ((r.Time.Value.Month).Equals(x.Month)) && ((r.Time.Value.Year).Equals(x.Year)))
                          select Convert.ToDouble(r.PulseRate);
                double resAv = res.Average();
                return resAv.ToString();

            }
            catch (Exception e)
            {
                return "N/A";

            }

        }


        public string ObserverInfo(string uid)
        {

            try
            {
                string send;
                HappyWatchDataDataContext context = new HappyWatchDataDataContext();
                var pepId = ((from r in context.Peoples
                             where (r.Email.Equals(uid))
                             select r.id).Take(1)).Single();
                var obId = ((from r in context.Observers
                             where r.Person_id == pepId
                             select r.id).Take(1)).Single();
                var oName = ((from r in context.Peoples where r.id== pepId  select r.Name).Take(1)).Single();

                var oMail = ((from r in context.Peoples where r.id == pepId select r.Email).Take(1)).Single();

                var oDob = ((from r in context.Peoples where r.id == pepId select r.BirthDay).Take(1)).Single();

                var oGender = ((from r in context.Peoples where r.id == pepId select r.Gender).Take(1)).Single();

                var oContact = ((from r in context.Peoples where r.id == pepId select r.ContactNumber).Take(1)).Single();

                var oPass = ((from r in context.Peoples where r.id == pepId select r.Password).Take(1)).Single();


                send = oName.ToString() + "#" + oMail.ToString() + "#" + oDob.ToString() + "#" + oGender.ToString() + "#" + oContact.ToString()+"#" +oPass.ToString();

                return send;

            }
            catch (Exception e)
            {
                return "N/A#N/A#N/A#N/A#N/A#N/A#N/A#N/A#N/A";
                //return e.ToString();
            }



        }

        //public List<string> PatientInfo(string uid)
        public string PatientInfo(string uid)
        {
            string send;
            try
            {



                HappyWatchDataDataContext context = new HappyWatchDataDataContext();
                var pepId = ((from r in context.Peoples
                             where (r.Email.Equals(uid))
                              select r.id).Take(1)).Single();
                var pName = ((from r in context.Peoples where pepId == r.id select r.Name).Take(1)).Single();

                var pPass = ((from r in context.Peoples where pepId == r.id select r.Password).Take(1)).Single();

                var pMail = ((from r in context.Peoples where pepId == r.id select r.Email).Take(1)).Single();

                var pDob = ((from r in context.Peoples where pepId == r.id select r.BirthDay).Take(1)).Single();

                var pGender = ((from r in context.Peoples where pepId == r.id select r.Gender).Take(1)).Single();

                var pContact = ((from r in context.Peoples where pepId == r.id select r.ContactNumber).Take(1)).Single();

                var pDeviceId = ((from r in context.Patients where r.Person_id == pepId select r.DeviceId).Take(1)).Single();

                var pCondition = ((from r in context.Patients where r.Person_id == pepId select r.PhysicalCondition).Take(1)).Single();

                send = pName.ToString() + "#" + pMail.ToString() + "#" + pDob.ToString() + "#" + pGender.ToString() + "#" + pContact.ToString() + "#" + pDeviceId.ToString() + "#" + pCondition.ToString()+"#"+pPass.ToString();

                return send;

            }
            catch (Exception e)
            {

                /////////////////////////////////
                return e.ToString();
            }



        }

        public List<string> FindPulseList(string uid)
        {
            List<string> error = new List<string>();
            error.Add("error");
            try
            {
                HappyWatchDataDataContext context = new HappyWatchDataDataContext();
                var pepId = (from r in context.Peoples where r.Email.Equals(uid) select r.id).Single();
                var patientId = (from r in context.Patients where r.Person_id == pepId select r.id).Single();
                var pulseList = from r in context.HealthDatas orderby r.id descending where r.PatientPatientId == patientId select r.PulseRate;
                return pulseList.ToList();
            }
            catch (Exception ex)
            {
                ////////////do something
                return error;
            }

        }

        public string GetLocation(string uid)
        {
            try
            {
                HappyWatchDataDataContext context = new HappyWatchDataDataContext();
                var pepId = ((from r in context.Peoples
                              where (r.Email.Equals(uid))
                              select r.id).Take(1)).Single();

                var UnLoc = ((from r in context.Peoples
                              where r.id == pepId
                              select r.Address).Take(1)).Single();
                string ULoc = UnLoc.ToString();
                string[] ar = new string[100];
                ar = ULoc.Split(',');
                return ar[1];
            }
            catch (Exception ex)
            {
                return "N/A";
            }
        }

        public string GetTime(String uid)
        {
            //string a="null";
            try
            {
                HappyWatchDataDataContext context = new HappyWatchDataDataContext();
                var pepId = (from r in context.Peoples where r.Email.Equals(uid) select r.id).Single();
               
                var patientId = (from r in context.Patients where r.Person_id == pepId select r.id).Single();
               var time=((from r in context.HealthDatas
                                   orderby r.id descending
                                   where r.PatientPatientId == patientId
                                   select r.Time).Take(1)).Single();
               // var time = (from r in context.HealthDatas where r.PatientPatientId == patientId select r.Time).Single();
               
                return time.ToString();
            }
            catch (Exception ex)
            {
                return "N/A";
            }
        }

        public List<ObPatient> AllObserversList(string uid)
        {
            HappyWatchDataDataContext context = new HappyWatchDataDataContext();
            var pPepId = ((from r in context.Peoples
                          where r.Email.Equals(uid)
                          select r.id).Take(1)).Single();
            var PatId = ((from r in context.Patients
                          where r.Person_id == pPepId
                          select r.id).Take(1)).Single();

            
            var pepId = from r in context.Observers select r.Person_id;

            List<ObPatient> ob = new List<ObPatient>();
            foreach (var id in pepId)
            {
                var obId = ((from r in context.Observers
                             where r.Person_id == id
                             select r.id).Take(1)).Single();
                var res = context.PatientVSObservers.Any(r => r.ObserverObserverId==obId&&r.PatientPatientId==PatId && r.POStatus.Equals("active"));

                if (res == false)
                {
                    ObPatient temp = new ObPatient();
                    var obName = (from r in context.Peoples
                                  where (r.id == id && r.Status.Equals("active"))
                                  select r.Name).Single();
                    var obMail = (from r in context.Peoples
                                  where (r.id == id && r.Status.Equals("active"))
                                  select r.Email).Single();
                    temp.pName = obName.ToString();
                    temp.pMail = obMail.ToString();

                    ob.Add(temp);
                }



            }

            return ob.ToList();
        }

        public bool MakeRelationObRequest(string uid)
        {
            try
            {
                PatientVSObserver data = new PatientVSObserver();
                string[] str = new string[10];
                str = uid.Split('#');
                HappyWatchDataDataContext context = new HappyWatchDataDataContext();
                var patientPeopleId = ((from r in context.Peoples
                                        where r.Email.Equals(str[0])
                                        select r.id).Take(1)).Single();
                var observerPeopleId = ((from r in context.Peoples
                                         where r.Email.Equals(str[1])
                                         select r.id).Take(1)).Single();
                var patientId = ((from r in context.Patients
                                  where r.Person_id == patientPeopleId
                                  select r.id).Take(1)).Single();
                var observerId = ((from r in context.Observers
                                   where r.Person_id == observerPeopleId
                                   select r.id).Take(1)).Single();
                data.PatientPatientId = patientId;
                data.ObserverObserverId = observerId;
                data.POStatus = "pending";
                data.Relation = str[2];
                context.PatientVSObservers.InsertOnSubmit(data);
                context.SubmitChanges();
                return true;
            }
            catch (Exception e)
            {
                return false;
            }




        }

        /*    public List<ObPatient> PatientList(string uid)
            {
                HappyWatchDataDataContext context = new HappyWatchDataDataContext();
                ObPatient tempOb = new ObPatient();
                List<ObPatient> observed = new List<ObPatient>();
                try
                {

                    //  string[] str = new string[10];
                    //str = uid.Split(',');


                    var dPeopleId = (from r in context.Peoples
                                     where ((r.Email.Equals(uid)))
                                     select r.PeopleId).Single();

                    var dDocId = (from r in context.Doctors where (r.Person_PeopleId == dPeopleId) select r.DoctorId).Single();

                    var patientIdList = from r in context.PatientVSDoctors where (r.DoctorDoctorId == dDocId) select r.PatientPatientId;

                    foreach (var pId in patientIdList)
                    {
                        var pPeopleId = (from r in context.Patients where (r.PatientId == pId) select r.Person_PeopleId).Single();
                        tempOb.pName = (from r in context.Peoples where (r.PeopleId == pPeopleId) select r.Name).Single();
                        tempOb.pMail = (from r in context.Peoples where (r.PeopleId == pPeopleId) select r.Email).Single();
                        observed.Add(tempOb);

                    }
                }
                catch (Exception ex)
                {
                    ////////////////////////////////put code here
                }
                return observed.ToList();
 
            }*/


        public bool MakeRelationRequest(string uid)
        {
            try
            {
                DocVSpatientReq data = new DocVSpatientReq();
                PatientVSDoctor data1 = new PatientVSDoctor();
                
                string[] str = new string[10];
                str = uid.Split(',');
                HappyWatchDataDataContext context = new HappyWatchDataDataContext();
                var patientPeopleId = ((from r in context.Peoples
                                        where r.Email.Equals(str[0])
                                        select r.id).Take(1)).Single();
                var docPeopleId = ((from r in context.Peoples
                                         where r.Email.Equals(str[1])
                                    select r.id).Take(1)).Single();
                var patientId = ((from r in context.Patients
                                  where r.Person_id == patientPeopleId
                                  select r.id).Take(1)).Single();
                var docId = ((from r in context.Doctors
                              where r.Person_id == docPeopleId
                              select r.id).Take(1)).Single();
                data.PatientPatientId= patientId;
                data.DoctorDoctorId = docId;
                data.Status = "pending";

                context.DocVSpatientReqs.InsertOnSubmit(data);
                context.SubmitChanges();

                data1.PatientPatientId = patientId;
                data1.DoctorDoctorId = docId;
                data1.PDStatus = "pending";
                data1.Relation = "med";


                context.PatientVSDoctors.InsertOnSubmit(data1);
                context.SubmitChanges();
                
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
 
        }

        public List<ObPatient> HospitalDoctorList(string uid)
        {
            List<ObPatient> docList = new List<ObPatient>();
            try
            {
                HappyWatchDataDataContext context = new HappyWatchDataDataContext();



                var hospitalId = ((from r in context.Hospitals
                                where r.Mail.Equals(uid)
                                select r.id).Take(1)).Single();
                

                var docIdList = from r in context.Doctors
                                where r.HospitalId == hospitalId && r.DoctorStatus.Equals("active")
                                select r.id;

                foreach (var id in docIdList)
                {
                    ObPatient temp = new ObPatient();
                    var docPepId = ((from r in context.Doctors
                                     where r.id == id
                                     select r.Person_id).Take(1)).Single();

                    temp.pName = ((from r in context.Peoples
                                   where r.id == docPepId
                                   select r.Name).Take(1)).Single();


                    temp.pMail = ((from r in context.Peoples
                                   where r.id == docPepId
                                   select r.Email).Take(1)).Single();
                    docList.Add(temp);



                }
            }
            catch (Exception ex)
            {
                //////////////
            }
            return docList.ToList();
        }

        public List<ObPatient> HospitalPatientList(string uid)
        {
            List<ObPatient> patientList = new List<ObPatient>();
            try
            {
                HappyWatchDataDataContext context = new HappyWatchDataDataContext();



                var hospitalId = ((from r in context.Hospitals
                                where r.Mail.Equals(uid)
                                select r.id).Take(1)).Single();
                
                var patientIdList = from r in context.PatientVSHospitals1s
                                    where ( r.HospitalId == hospitalId && r.PHStatus.Equals("active"))
                                    select r.PatientPatientId;

                foreach(var id in patientIdList)
                {
                    ObPatient temp = new ObPatient();
                    var patientPepId = ((from r in context.Patients
                                         where r.id == id
                                         select r.Person_id).Take(1)
                                           ).Single();

                    temp.pName = ((from r in context.Peoples
                                   where r.id == patientPepId
                                   select r.Name).Take(1)).Single();

                    temp.pMail = ((from r in context.Peoples
                                   where r.id == patientPepId
                                   select r.Email).Take(1)).Single();

                    patientList.Add(temp);

                }

            }
            catch (Exception ex)
            {
                //
            }
            return patientList.ToList();


  
        }

        public string HospitalInfoGet(string uid)
        {
            try
            {
                HappyWatchDataDataContext context = new HappyWatchDataDataContext();
                var hospitalId = ((from r in context.Hospitals
                             where r.Mail.Equals(uid)
                              select r.id).Take(1)).Single();
               
                string name = ((from r in context.Hospitals
                                where r.id == hospitalId
                                select r.Name).Take(1)).Single();

                string address = ((from r in context.Hospitals
                                   where r.id == hospitalId
                                select r.Address).Take(1)).Single();

                string mail = ((from r in context.Hospitals
                                where r.id == hospitalId
                                select r.Mail).Take(1)).Single();
                string password = ((from r in context.Hospitals
                                    where r.id == hospitalId
                                    select r.Password).Take(1)).Single();
                var patients = from r in context.PatientVSHospitals1s where( r.HospitalId == hospitalId && r.PHStatus.Equals("active")) select r.PatientPatientId;
                int countPatient=0;
                int countDoc=0;
                foreach(var p in patients)
                {
                    countPatient++;

                }
                var doctors=from r in context.Doctors where r.HospitalId== hospitalId && r.DoctorStatus.Equals("active")
                            select r.id;

                foreach(var d in doctors)
                {
                    countDoc++;
                }
                string res=name+"$"+mail+"$"+address+"$"+countPatient.ToString()+"$"+countDoc.ToString()+"$"+password;
                return res;
                

            }
            catch (Exception ex)
            {
                return "N/A";
            }

 
        }

        public List<DocSignup> SignUprequests(string uid)
        {
            List<DocSignup> req=new List<DocSignup>();
            try
            {
                HappyWatchDataDataContext context = new HappyWatchDataDataContext();
                var hospitalId = ((from r in context.Hospitals
                                where r.Mail.Equals(uid)
                                select r.id).Take(1)).Single();
               
                DocSignReq request = new DocSignReq();
                var reqs=from r in context.DocSignReqs
                         where (r.HospitalId==hospitalId && r.Status.Equals("pending"))
                         select r.Doctor_id;
                

                foreach (var res in reqs)
                {
                    DocSignup temp = new DocSignup();
                    var pepId = ((from r in context.Doctors
                                  where r.id == res
                                  select r.Person_id).Take(1)).Single();
                    temp.dName = ((from r in context.Peoples
                                   where r.id == pepId
                             select r.Name).Take(1)).Single();

                    temp.dMail = ((from r in context.Peoples
                                   where r.id == pepId
                                   select r.Email).Take(1)).Single();

                    temp.dLicense = ((from r in context.Doctors
                                      where r.Person_id == pepId
                                      select r.LisenceId).Take(1)).Single();
                    temp.specialist = ((from r in context.Doctors
                                        where r.Person_id == pepId
                                        select r.Specialist).Take(1)).Single();

                    req.Add(temp);
                    
                }

                         
            }
            catch (Exception ex)
            {
                ////
            }
            return req.ToList();
 
        }

        public List<AddRequest> AddRequests(string uid)
        {
            List<AddRequest> reqs = new List<AddRequest>();
            try
            {
                HappyWatchDataDataContext context = new HappyWatchDataDataContext();
                var hospitalId = ((from r in context.Hospitals
                                where r.Mail.Equals(uid)
                                select r.id).Take(1)).Single();

                
             
                var reqIds = from r in context.DocVSpatientReqs
                             where( r.HospitalId == hospitalId && r.Status.Equals("pending"))
                             select r.id;
                foreach (var ids in reqIds)
                {
                    AddRequest ob = new AddRequest();
                    var patientId = ((from r in context.DocVSpatientReqs
                                      where r.id == ids
                                      select r.PatientPatientId).Take(1)).Single();
                    var docId = ((from r in context.DocVSpatientReqs
                                  where r.id == ids
                                  select r.DoctorDoctorId).Take(1)).Single();
                    var patientPepId=((from r in context.Patients
                                       where r.id == patientId
                                       select r.Person_id).Take(1)).Single();
                    var docPepId = ((from r in context.Doctors
                                     where r.id == docId
                                     select r.Person_id).Take(1)).Single();
                    ob.PatientName = ((from r in context.Peoples
                                       where r.id == patientPepId
                                       select r.Name).Take(1)).Single();
                    ob.PatientMail = ((from r in context.Peoples
                                       where r.id == patientPepId
                                       select r.Email).Take(1)).Single();
                    ob.DoctorName = ((from r in context.Peoples
                                      where r.id == docPepId
                                      select r.Name).Take(1)).Single();
                    ob.DoctorMail = ((from r in context.Peoples
                                      where r.id == docPepId
                                      select r.Email).Take(1)).Single();
                    reqs.Add(ob);
                    


                }




            }
            catch (Exception ex)
            {
                ////
            }
            return reqs.ToList();

 
        }

       /* public Boolean MakeRelation(string uid)
        {
            try
            {
                string[] ar = new string[10];
                HappyWatchDataDataContext context = new HappyWatchDataDataContext();
                ar = uid.Split(',');

            }
            catch (Exception ex)
            {
 
            }

        }*/

        public string MakeSign(string uid)
        {
           
            try
            {
                HappyWatchDataDataContext context = new HappyWatchDataDataContext();
               

                var docPepId = ((from r in context.Peoples
                              where r.Email.Equals(uid) && r.Designation.Equals("doctor")
                                 select r.id).Take(1)).Single();

                var docId = ((from r in context.Doctors
                              where r.Person_id == docPepId
                              select r.id).Take(1)).Single();

                var req = ((from r in context.DocSignReqs
                            where r.Doctor_id == docId
                            select r).Take(1)).Single();

                req.Status = "active";
                
                context.SubmitChanges();

                var pep=((from r in context.Peoples
                          where r.id == docPepId
                              select r).Take(1)).Single();
                pep.Status = "active";
                
                context.SubmitChanges();


                var docTable = ((from r in context.Doctors
                                 where r.Person_id == docPepId
                                 select r
                                    ).Take(1)).Single();
                docTable.DoctorStatus = "active";
                context.SubmitChanges();
                return "true";

                
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }

        public string RejectSign(string uid)
        {
            try
            {
                HappyWatchDataDataContext context = new HappyWatchDataDataContext();


                var docPepId = ((from r in context.Peoples
                                 where r.Email.Equals(uid) && r.Designation.Equals("doctor")
                                 select r.id).Take(1)).Single();

                var docId = ((from r in context.Doctors
                              where r.Person_id == docPepId
                              select r.id).Take(1)).Single();

                var req = ((from r in context.DocSignReqs
                            where r.Doctor_id == docId
                            select r).Take(1)).Single();

                req.Status = "reject";

                context.SubmitChanges();

                var pep = ((from r in context.Peoples
                            where r.id == docPepId
                            select r).Take(1)).Single();
                pep.Status = "reject";

                context.SubmitChanges();


                var docTable = ((from r in context.Doctors
                                 where r.Person_id == docPepId
                                 select r
                                    ).Take(1)).Single();
                docTable.DoctorStatus = "reject";
                context.SubmitChanges();
                return "true";


            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
 
        }

        public Boolean MakeMedRelation(string uid)
        {
            string[] ar = new string[10];
            try
            {
                HappyWatchDataDataContext context = new HappyWatchDataDataContext();
                ar = uid.Split('#');
                var patientPepId = ((from r in context.Peoples
                                     where r.Email.Equals(ar[0])
                                     select r.id).Take(1)).Single();
                var docPepId = ((from r in context.Peoples
                                 where r.Email.Equals(ar[1])
                                 select r.id).Take(1)).Single();
                var PatientId = ((from r in context.Patients
                                  where r.Person_id == patientPepId
                                  select r.id).Take(1)).Single();
                var DocId = ((from r in context.Doctors
                              where r.Person_id == docPepId
                              select r.id).Take(1)).Single();

                var reqRow = ((from r in context.DocVSpatientReqs
                               where (r.DoctorDoctorId == DocId && r.PatientPatientId == PatientId)
                               select r).Take(1)).Single();
                reqRow.Status = "active";
                context.SubmitChanges();
                var relRow=((from r in context.PatientVSDoctors
                                 where (r.DoctorDoctorId == DocId && r.PatientPatientId == PatientId)
                               select r).Take(1)).Single();
                relRow.PDStatus = "active";
                context.SubmitChanges();



                return true;

            }
            catch (Exception ex)
            {
                return false;

            }
        }

        public Boolean RejectMedRelation(string uid)
        {
            string[] ar = new string[10];
            try
            {
                HappyWatchDataDataContext context = new HappyWatchDataDataContext();
                ar = uid.Split('#');
                var patientPepId = ((from r in context.Peoples
                                     where r.Email.Equals(ar[0])
                                     select r.id).Take(1)).Single();
                var docPepId = ((from r in context.Peoples
                                 where r.Email.Equals(ar[1])
                                 select r.id).Take(1)).Single();
                var PatientId = ((from r in context.Patients
                                  where r.Person_id == patientPepId
                                  select r.id).Take(1)).Single();
                var DocId = ((from r in context.Doctors
                              where r.Person_id == docPepId
                              select r.id).Take(1)).Single();

                var reqRow = ((from r in context.DocVSpatientReqs
                               where (r.DoctorDoctorId == DocId && r.PatientPatientId == PatientId)
                               select r).Take(1)).Single();
                reqRow.Status = "inactive";
                context.SubmitChanges();
                var relRow = ((from r in context.PatientVSDoctors
                               where (r.DoctorDoctorId == DocId && r.PatientPatientId == PatientId)
                               select r).Take(1)).Single();
                relRow.PDStatus = "inactive";
                context.SubmitChanges();



                return true;

            }
            catch (Exception ex)
            {
                return false;

            }
 
        }

        public List<ObPatient> MakeOwnDocList(string uid)
        {
            List<ObPatient> docList = new List<ObPatient>();
            try
            {
                HappyWatchDataDataContext context = new HappyWatchDataDataContext();
                var pepId = ((from r in context.Peoples
                              where r.Email.Equals(uid)
                              select r.id).Take(1)).Single();
                var patientId = ((from r in context.Patients
                                  where r.Person_id == pepId
                                  select r.id).Take(1)).Single();
                var docids = from r in context.PatientVSDoctors
                             where (r.PatientPatientId == patientId && r.PDStatus.Equals("active"))
                             select r.DoctorDoctorId;
                foreach (var id in docids)
                {
                    ObPatient temp = new ObPatient();
                    var docPepId = ((from r in context.Doctors
                                     where r.id == id
                                     select r.Person_id).Take(1)).Single();
                    temp.pName = ((from r in context.Peoples
                                   where r.id == docPepId
                                   select r.Name).Take(1)).Single();
                    temp.pMail = ((from r in context.Peoples
                                   where r.id == docPepId
                                   select r.Email).Take(1)).Single();
                    docList.Add(temp);

                    
                }
            }
            catch (Exception ex)
            {
                ////do something plz
            }

            return docList.ToList();
        }

        public string MakeDocRequest(string uid)
        {
            string[] ar = new string[10];
            try
            {
                HappyWatchDataDataContext context=new HappyWatchDataDataContext();
                DocVSpatientReq req = new DocVSpatientReq();
                PatientVSDoctor pVd = new PatientVSDoctor();
                ar = uid.Split('#');
                var docPepId = ((from r in context.Peoples
                                 where r.Email.Equals(ar[1])
                                 select r.id
                                   ).Take(1)).Single();
                var docId = ((from r in context.Doctors
                              where r.Person_id == docPepId
                              select r.id).Take(1)).Single();

                var patientPepId = ((from r in context.Peoples
                                     where r.Email.Equals(ar[0])
                                     select r.id).Take(1)).Single();
                var patId = ((from r in context.Patients
                              where r.Person_id == patientPepId
                              select r.id).Take(1)).Single();
                var hospitalId = ((from r in context.Doctors
                                   where r.Person_id == docPepId
                                   select r.HospitalId).Take(1)).Single();

                req.DoctorDoctorId = docId;
                req.PatientPatientId = patId;
                req.HospitalId = hospitalId;
                req.Status = "pending";

                context.DocVSpatientReqs.InsertOnSubmit(req);
                context.SubmitChanges();

                pVd.DoctorDoctorId = docId;
                pVd.PatientPatientId = patId;
                pVd.PDStatus = "pending";
                pVd.Relation = "med";
               
                context.PatientVSDoctors.InsertOnSubmit(pVd);
                context.SubmitChanges();
                return "true";




            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }

        public List<ObPatient> MakeAvailableDocList(string uid)
        {
            List<ObPatient> docList = new List<ObPatient>();
            try
            {
                HappyWatchDataDataContext context=new HappyWatchDataDataContext();
                var patPepId = ((from r in context.Peoples
                                 where r.Email.Equals(uid)
                                 select r.id).Take(1)).Single();
                var patId = ((from r in context.Patients
                              where r.Person_id == patPepId
                              select r.id).Take(1)).Single();
                var hospitalIds = from r in context.PatientVSHospitals1s
                                  where( r.PatientPatientId == patId && r.PHStatus.Equals("active"))
                                  select r.HospitalId;
                foreach(var hospitalId in hospitalIds)
                {
                    var docIds = from r in context.Doctors
                                 where (r.HospitalId == hospitalId  && r.DoctorStatus.Equals("active"))
                                 select r.id;
                    foreach(var docId in docIds)
                    {
                        var res = context.PatientVSDoctors.Any(r => r.DoctorDoctorId==docId && r.PatientPatientId==patId && r.PDStatus.Equals("active"));
                        if (res == false)
                        {
                            ObPatient temp = new ObPatient();
                            var pepId = ((from r in context.Doctors
                                          where r.id == docId
                                          select r.Person_id).Take(1)).Single();
                            temp.pName = ((from r in context.Peoples
                                           where r.id == pepId
                                           select r.Name).Take(1)).Single();
                            temp.pMail = ((from r in context.Peoples
                                           where r.id == pepId
                                           select r.Email).Take(1)).Single();
                            docList.Add(temp);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                /////make something here
            }
            return docList.ToList();
        }

        public List<ObPatient> RelationReqs(string uid)
        {
            List<ObPatient> ob = new List<ObPatient>();
            try
            {
                HappyWatchDataDataContext context=new HappyWatchDataDataContext();
                var pepId = ((from r in context.Peoples
                              where r.Email.Equals(uid) && r.Designation.Equals("observer")
                              select r.id).Take(1)).Single();
                var obId = ((from r in context.Observers
                             where r.Person_id == pepId
                             select r.id).Take(1)).Single();
                var reqIds = from r in context.PatientVSObservers
                             where r.ObserverObserverId == obId
                             select r.PatientPatientId;
                foreach (var req in reqIds)
                {
                    ObPatient temp = new ObPatient();
                    var PatPepId = ((from r in context.Patients
                                     where r.id == req
                                     select r.Person_id).Take(1)).Single();
                    temp.pName = ((from r in context.Peoples
                                   where r.id == PatPepId
                                   select r.Name).Take(1)).Single();
                    temp.pMail = ((from r in context.Peoples
                                   where r.id == PatPepId
                                    select r.Email).Take(1)).Single();
                    ob.Add(temp);

 


                }

            }
            catch (Exception ex)
            {
                /////do something
            }
            return ob.ToList();
        }

        public Boolean ObserverAcceptRelation(string uid)
        {
            string[] ar = new string[10];
            try
            {
                HappyWatchDataDataContext context = new HappyWatchDataDataContext();
                ar = uid.Split('#');
                var obPepId = ((from r in context.Peoples
                                where r.Email.Equals(ar[0])
                                select r.id).Take(1)).Single();
                var patPepId = ((from r in context.Peoples
                                 where r.Email.Equals(ar[1])
                                 select r.id).Take(1)).Single();
                var obId = ((from r in context.Observers
                             where r.Person_id == obPepId
                             select r.id).Take(1)).Single();
                var patId = ((from r in context.Patients
                              where r.Person_id == patPepId
                              select r.id).Take(1)).Single();

                var rel = ((from r in context.PatientVSObservers
                            where (r.PatientPatientId == patId && r.ObserverObserverId == obId)
                            select r).Take(1)).Single();
                rel.POStatus = "active";
                context.SubmitChanges();
                return true;


            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public Boolean ObserverRejectRelation(string uid)
        {
            string[] ar = new string[10];
            try
            {
                HappyWatchDataDataContext context = new HappyWatchDataDataContext();
                ar = uid.Split('#');
                var obPepId = ((from r in context.Peoples
                                where r.Email.Equals(ar[0])
                                select r.id).Take(1)).Single();
                var patPepId = ((from r in context.Peoples
                                 where r.Email.Equals(ar[1])
                                 select r.id).Take(1)).Single();
                var obId = ((from r in context.Observers
                             where r.Person_id == obPepId
                             select r.id).Take(1)).Single();
                var patId = ((from r in context.Patients
                              where r.Person_id == patPepId
                              select r.id).Take(1)).Single();

                var rel = ((from r in context.PatientVSObservers
                            where (r.PatientPatientId == patId && r.ObserverObserverId == obId)
                            select r).Take(1)).Single();
                rel.POStatus = "inactive";
                context.SubmitChanges();
                return true;


            }
            catch (Exception ex)
            {
                return false;
            }
 
        }

        public List<HospitalMod> MakeAllHosptialList()
        {
            List<HospitalMod> hosp = new List<HospitalMod>();
            try
            {
                HappyWatchDataDataContext context = new HappyWatchDataDataContext();
                var hospitalIds = from r in context.Hospitals select r.id;
                foreach (var id in hospitalIds)
                {
                    HospitalMod temp = new HospitalMod();

                    temp.Name = ((from r in context.Hospitals
                                  where r.id == id
                                  select r.Name).Take(1)).Single();
                    temp.Mail = ((from r in context.Hospitals
                                  where r.id == id
                                  select r.Mail).Take(1)).Single();
                    temp.Address = ((from r in context.Hospitals
                                     where r.id == id
                                     select r.Address).Take(1)).Single();
                    if (temp.Name.Equals("NONE"))
                    {
                    }
                    else
                    {
                        hosp.Add(temp);
                    }
                }
            }
            catch (Exception ex)
            {
                /////do something
            }
            return hosp.ToList();
        }

        public List<HospitalMod> MakeOwnHosptialList(string uid)
        {
            List<HospitalMod> hosp = new List<HospitalMod>();
            try
            {
                HappyWatchDataDataContext context = new HappyWatchDataDataContext();
                var patPepId=((from r in context.Peoples
                                   where r.Email.Equals(uid)
                               select r.id).Take(1)).Single();
                var patId=((from r in context.Patients
                            where r.Person_id == patPepId
                            select r.id).Take(1)).Single();
                var hospitalIds = from r in context.PatientVSHospitals1s
                                  where (r.PatientPatientId == patId && r.PHStatus.Equals("active"))
                                  select r.HospitalId;
                foreach (var id in hospitalIds)
                {
                    HospitalMod temp = new HospitalMod();
                    temp.Name = ((from r in context.Hospitals
                                  where r.id == id
                                  select r.Name).Take(1)).Single();

                    temp.Mail = ((from r in context.Hospitals
                                  where r.id == id
                                  select r.Mail).Take(1)).Single();

                    temp.Address = ((from r in context.Hospitals
                                     where r.id == id
                                  select r.Address).Take(1)).Single();
                    hosp.Add(temp);
 
                }
                

            }
            catch (Exception ex)
            {
                //
            }
            return hosp.ToList();

        }

        public Boolean MakeHospitalReq(string uid)
        {
            string[] ar = new string[10];
            try
            {
                ar = uid.Split('#');
                HappyWatchDataDataContext context = new HappyWatchDataDataContext();
                var patPepId = ((from r in context.Peoples
                                 where r.Email.Equals(ar[0])
                                 select r.id).Take(1)).Single();
                var patId = ((from r in context.Patients
                              where r.Person_id == patPepId
                              select r.id).Take(1)).Single();
                var hospitalId = ((from r in context.Hospitals
                                   where r.Mail.Equals(ar[1])
                                   select r.id).Take(1)).Single();
                PatientVSHospitals1 req = new PatientVSHospitals1();
                req.PatientPatientId = patId;
                req.HospitalId = hospitalId;
                req.PHStatus = "pending";
                context.PatientVSHospitals1s.InsertOnSubmit(req);
                context.SubmitChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }

        }

        public List<ObPatient> ObserverVsPatientRequest(string uid)
        {
            List<ObPatient> obs = new List<ObPatient>();
            try
            {
                HappyWatchDataDataContext context = new HappyWatchDataDataContext();
                var pepId = ((from r in context.Peoples
                              where r.Email.Equals(uid)
                              select r.id).Take(1)).Single();
                var obId = ((from r in context.Observers
                             where r.Person_id == pepId
                             select r.id).Take(1)).Single();
                var patientIds = from r in context.PatientVSObservers
                                 where (r.ObserverObserverId == obId && r.POStatus.Equals("pending"))
                                 select r.PatientPatientId;
                foreach (var id in patientIds)
                {
                    ObPatient temp = new ObPatient();
                    var patPepId = ((from r in context.Patients
                                     where r.id == id
                                     select r.Person_id).Take(1)).Single();
                    temp.pName = ((from r in context.Peoples
                                   where r.id == patPepId
                                   select r.Name).Take(1)).Single();
                    temp.pMail = ((from r in context.Peoples
                                   where r.id == patPepId
                                   select r.Email).Take(1)).Single();
                    obs.Add(temp);

                }


            }
            catch (Exception ex)
            {
                //do some
            }
            return obs.ToList();
        }

        public Boolean AcceptPatientReq(string uid)
        {
            string[] ar = new string[10];
            try
            {
                HappyWatchDataDataContext context = new HappyWatchDataDataContext();
                ar = uid.Split('#');
                var ObPepId = ((from r in context.Peoples
                                where r.Email.Equals(ar[0])
                                select r.id).Take(1)).Single();
                var PatPepId = ((from r in context.Peoples
                                 where r.Email.Equals(ar[1])
                                 select r.id).Take(1)).Single();
                var ObId = ((from r in context.Observers
                             where r.Person_id == ObPepId
                             select r.id).Take(1)).Single();
                var PatId = ((from r in context.Patients
                              where r.Person_id == PatPepId
                              select r.id).Take(1)).Single();
                var req = ((from r in context.PatientVSObservers
                            where( r.PatientPatientId == PatId && r.ObserverObserverId == ObId)
                            select r).Take(1)).Single();
                req.POStatus = "active";
                context.SubmitChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public Boolean RejecttPatientReq(string uid)
        {
            string[] ar = new string[10];
            try
            {
                HappyWatchDataDataContext context = new HappyWatchDataDataContext();
                ar = uid.Split('#');
                var ObPepId = ((from r in context.Peoples
                                where r.Email.Equals(ar[0])
                                select r.id).Take(1)).Single();
                var PatPepId = ((from r in context.Peoples
                                 where r.Email.Equals(ar[1])
                                 select r.id).Take(1)).Single();
                var ObId = ((from r in context.Observers
                             where r.Person_id == ObPepId
                             select r.id).Take(1)).Single();
                var PatId = ((from r in context.Patients
                              where r.Person_id == PatPepId
                              select r.id).Take(1)).Single();
                var req = ((from r in context.PatientVSObservers
                            where (r.PatientPatientId == PatId && r.ObserverObserverId == ObId)
                            select r).Take(1)).Single();
                req.POStatus = "inactive";
                context.SubmitChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
 
        }
        public Boolean AcceptPatientHospitalRequest(string uid)
        {
            string[] ar = new string[10];
            try
            {
                ar = uid.Split('#');
                HappyWatchDataDataContext context = new HappyWatchDataDataContext();
                var HospitalId = ((from r in context.Hospitals
                                   where r.Mail.Equals(ar[0])
                                   select r.id).Take(1)).Single();
                var PatPepId = ((from r in context.Peoples
                                 where r.Email.Equals(ar[1])
                                 select r.id).Take(1)).Single();
                var PatId = ((from r in context.Patients
                              where r.Person_id == PatPepId
                              select r.id).Take(1)).Single();
                var req = ((from r in context.PatientVSHospitals1s
                            where (r.PatientPatientId == PatId && r.HospitalId == HospitalId && r.PHStatus.Equals("pending"))
                            select r).Take(1)).Single();
                req.PHStatus = "active";
                context.SubmitChanges();
                return true;

            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public Boolean RejectPatientHospitalRequest(string uid)
        {
            string[] ar = new string[10];
            try
            {
                ar = uid.Split('#');
                HappyWatchDataDataContext context = new HappyWatchDataDataContext();
                var HospitalId = ((from r in context.Hospitals
                                   where r.Mail.Equals(ar[0])
                                   select r.id).Take(1)).Single();
                var PatPepId = ((from r in context.Peoples
                                 where r.Email.Equals(ar[1])
                                 select r.id).Take(1)).Single();
                var PatId = ((from r in context.Patients
                              where r.Person_id == PatPepId
                              select r.id).Take(1)).Single();
                var req = ((from r in context.PatientVSHospitals1s
                            where (r.PatientPatientId == PatId && r.HospitalId == HospitalId && r.PHStatus.Equals("pending"))
                            select r).Take(1)).Single();
                req.PHStatus = "inactive";
                context.SubmitChanges();
                return true;

            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public List<ObPatient> PatientHospitalReqList(string uid)
        {
            List<ObPatient> obs = new List<ObPatient>();
            try
            {
                HappyWatchDataDataContext context = new HappyWatchDataDataContext();
                var HospitalId = ((from r in context.Hospitals
                                   where r.Mail.Equals(uid)
                                   select r.id).Take(1)).Single();
                var patientIds = from r in context.PatientVSHospitals1s
                                 where (r.HospitalId == HospitalId && r.PHStatus.Equals("pending"))
                                 select r.PatientPatientId;
                foreach (var id in patientIds)
                {
                    ObPatient temp = new ObPatient();
                    var patpepid = ((from r in context.Patients
                                     where r.id == id
                                     select r.Person_id).Take(1)).Single();
                    temp.pName = ((from r in context.Peoples
                                   where r.id == patpepid
                                   select r.Name).Take(1)).Single();
                    temp.pMail = ((from r in context.Peoples
                                   where r.id == patpepid
                                   select r.Email).Take(1)).Single();
                    obs.Add(temp);
                    
 
                }
            }
            catch (Exception ex)
            {
                //do some
            }
            return obs.ToList();
        }

        public string GetLastPrescription(string uid)
        {
            string[] ar=new string[10];
            try
            {
                HappyWatchDataDataContext context=new HappyWatchDataDataContext();
               
                var patPepId = ((from r in context.Peoples
                                 where r.Email.Equals(uid)
                                 select r.id).Take(1)).Single();
               
                var patId = ((from r in context.Patients
                              where r.Person_id == patPepId
                              select r.id).Take(1)).Single();
               


                var prescript = ((from r in context.Prescriptions
                                  orderby r.id descending
                                  where (r.PatientPatientId == patId )
                                  select r.Prescribed).Take(1)).Single();

                var dateTime = ((from r in context.Prescriptions
                                 orderby r.id descending
                                 where (r.PatientPatientId == patId)
                                 select r.Time).Take(1)).Single();

                var docid = ((from r in context.Prescriptions
                              orderby r.id descending
                              where (r.PatientPatientId == patId)
                              select r.DoctorDoctorId).Take(1)).Single();

                var pepId = ((from r in context.Doctors
                              where r.id == docid
                              select r.Person_id).Take(1)).Single();
                var name = ((from r in context.Peoples
                             where r.id == pepId
                             select r.Name).Take(1)).Single();
                
                string sent;
                sent =name.ToString()+"#" +prescript.ToString()+"#"+dateTime.ToString();

                return sent;
            }
            catch (Exception ex)
            {
                return "N/A";
            }

        }

        public string GetDocInfo(string uid)
        {
            try
            {
                HappyWatchDataDataContext context = new HappyWatchDataDataContext();
                var pepId = ((from r in context.Peoples
                              where r.Email.Equals(uid)
                              select r.id).Take(1)).Single();
                var docId = ((from r in context.Doctors
                              where r.Person_id == pepId
                              select r.id).Take(1)).Single();
                var docName = ((from r in context.Peoples
                                where r.id == pepId
                                select r.Name).Take(1)).Single();
                var docMail = ((from r in context.Peoples
                                where r.id == pepId
                                select r.Email).Take(1)).Single();
                var docPass = ((from r in context.Peoples
                                where r.id == pepId
                                select r.Password).Take(1)).Single();
                var docCon = ((from r in context.Peoples
                               where r.id == pepId
                               select r.ContactNumber).Take(1)).Single();
                var docGen = ((from r in context.Peoples
                               where r.id == pepId
                               select r.Gender).Take(1)).Single();

                var docDob = ((from r in context.Peoples
                               where r.id == pepId
                               select r.BirthDay).Take(1)).Single();
                var docSpeciality = ((from r in context.Doctors
                                      where r.id == docId
                                      select r.Specialist).Take(1)).Single();
                string sent = docName.ToString() + '#' + docMail.ToString() + '#' + docPass.ToString() + '#' + docCon.ToString() + '#' + docGen.ToString() + '#' + docDob.ToString()+ '#'+docSpeciality.ToString();

                return sent;
            }
            catch (Exception ex)
            {
                return "N/A#N/A#N/A#N/A#N/A#N/A#N/A#N/A#N/A#N/A#N/A#N/A";
 
            }
        }

        public string InsertPrescription(string uid)
        {
            string[] ar = new string[10];
            try
            {
                HappyWatchDataDataContext context = new HappyWatchDataDataContext();
                ar = uid.Split('#');
                var docPepId = ((from r in context.Peoples
                                 where r.Email.Equals(ar[0])
                                 select r.id).Take(1)).Single();
                var patPepId = ((from r in context.Peoples
                                 where r.Email.Equals(ar[1])
                                 select r.id).Take(1)).Single();
                var docId = ((from r in context.Doctors
                              where r.Person_id == docPepId
                              select r.id).Take(1)).Single();
                var patId = ((from r in context.Patients
                              where r.Person_id == patPepId
                              select r.id).Take(1)).Single();
                Prescription presc = new Prescription();
                presc.DoctorDoctorId = docId;
                presc.PatientPatientId = patId;
                presc.Prescribed = ar[2];
                context.Prescriptions.InsertOnSubmit(presc);


                context.SubmitChanges();
                return "true";

            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }

        public Boolean PateintDeleteObserver(string uid)
        {
            string[] ar = new string[10];
            try
            {
                HappyWatchDataDataContext context = new HappyWatchDataDataContext();
                ar = uid.Split('#');
                var PatPepId = ((from r in context.Peoples
                                 where r.Email.Equals(ar[0])
                                 select r.id).Take(1)).Single();
                var PatId = ((from r in context.Patients
                              where r.Person_id == PatPepId
                              select r.id).Take(1)).Single();
                var ObPepId = ((from r in context.Peoples
                                 where r.Email.Equals(ar[1])
                                select r.id).Take(1)).Single();
                var ObId = ((from r in context.Observers
                             where r.Person_id == ObPepId
                             select r.id).Take(1)).Single();
                var req = ((from r in context.PatientVSObservers
                            where (r.PatientPatientId == PatId && r.ObserverObserverId == ObId)
                            select r).Take(1)).Single();
                req.POStatus = "inactive";
                context.SubmitChanges();
                return true;


            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public Boolean AdminDeletePatient(string uid)
        {
            string[] ar = new string[10];
            try
            {
                HappyWatchDataDataContext context = new HappyWatchDataDataContext();
                ar = uid.Split('#');
                var HospitalId = ((from r in context.Hospitals
                                   where r.Mail.Equals(ar[0])
                                   select r.id).Take(1)).Single();
                var PatPepId = ((from r in context.Peoples
                                 where r.Email.Equals(ar[1])
                                 select r.id).Take(1)).Single();
                var PatId = ((from r in context.Patients
                              where r.Person_id == PatPepId
                              select r.id).Take(1)).Single();
                var req = ((from r in context.PatientVSHospitals1s
                            where (r.HospitalId == HospitalId && r.PatientPatientId == PatId)
                            select r
                              ).Take(1)).Single();
                req.PHStatus = "inactive";
                context.SubmitChanges();

                var docids = from r in context.PatientVSDoctors
                             where r.PatientPatientId == PatId
                             select r;
                foreach (var id in docids)
                {
                    id.PDStatus = "inactive";
                }
                return true;
                

            }
            catch (Exception ex)
            {
                return false;
            }

        }
        public String InsertData(string uid)
        {
            using (HappyWatchDataDataContext context = new HappyWatchDataDataContext())
            {
                try
                {
                    string[] ar = new string[10];
                    HealthData h = new HealthData();
                    h.DeviceId = "D432";
                    h.Location = "Dhaka";
                    h.PatientPatientId = 8;
                    h.PulseRate = "100";
                    context.HealthDatas.InsertOnSubmit(h);
                    context.SubmitChanges();
                    return "true";
                }
                catch
                {
                    return "false";
                }
            }
           
        }
        public Boolean AdminDeleteDoctor(string uid)
        {
            string[] ar = new string[10];
            try
            {
                HappyWatchDataDataContext context = new HappyWatchDataDataContext();
                ar = uid.Split('#');
                var HospitalId = ((from r in context.Hospitals
                                   where r.Mail.Equals(ar[0])
                                   select r.id).Take(1)).Single();
                var DocPepId = ((from r in context.Peoples
                                 where r.Email.Equals(ar[1])
                                 select r.id).Take(1)).Single();
                var DocId = ((from r in context.Doctors
                              where r.Person_id == DocPepId
                              select r.id).Take(1)).Single();
                var req = ((from r in context.Doctors
                            where r.id == DocId && r.HospitalId == HospitalId
                            select r).Take(1)).Single();
               
                req.DoctorStatus = "inactive";
                context.SubmitChanges();
                var pepid = ((from r in context.Peoples
                              where r.Email.Equals(ar[1])
                              select r).Take(1)).Single();
                pepid.Status = "inactive";
                context.SubmitChanges();
                var PatIds = from r in context.PatientVSDoctors
                             where r.DoctorDoctorId == DocId
                             select r;
                foreach (var ids in PatIds)
                {
                    ids.PDStatus = "inactive";
                    context.SubmitChanges();
                }
                return true;


            }
            catch (Exception ex)
            {
                return false;
            }
 
        }
        public Boolean UpdateDoctorInfo(string uid)
        { 
            string [] ar=new string[10];
            try
            {
               
                HappyWatchDataDataContext context = new HappyWatchDataDataContext();
                ar = uid.Split(',');
                var row = ((from r in context.Peoples
                            where r.Email.Equals(ar[0])
                            select r
               ).Take(1)).Single();
                row.Name = ar[1];
                row.Password = ar[2];
                row.BirthDay = Convert.ToDateTime(ar[3]);
                row.ContactNumber = ar[4];
                context.SubmitChanges();
               var pepId = ((from r in context.Peoples
                              where r.Email.Equals(ar[0])
                              select r.id).Take(1)).Single();

                var docRow = ((from r in context.Doctors
                               where r.Person_id == pepId
                               select r).Take(1)).Single();
                docRow.Specialist = ar[5];
                context.SubmitChanges();
                return true;

            }
            catch (Exception e)
            {
                return false;
            }
        }
       public Boolean DeactivePatient(string uid)
        {
            try
            {
                HappyWatchDataDataContext context = new HappyWatchDataDataContext();
                var people_is = ((from r in context.Peoples
                               where r.Email.Equals(uid)
                               select r
               ).Take(1)).Single();
                people_is.Status = "inactive";
                context.SubmitChanges();
                var peopleId = ((from r in context.Peoples
                                 where r.Email.Equals(uid)
                                 select r.id
               ).Take(1)).Single();
                var patientId = ((from r in context.Patients
                                 where r.Person_id==peopleId
                                 select r.id
               ).Take(1)).Single();
                var patRow = ((from r in context.Patients
                               where r.Person_id == peopleId
                               select r
               ).Take(1)).Single();
                patRow.Isactive = "inactive";
                var res = context.PatientVSDoctors.Any(r => r.PatientPatientId==patientId);
                if (res)
                {
                    var docs = from r in context.PatientVSDoctors
                               where r.PatientPatientId == patientId
                               select r;
                    foreach (var doc in docs)
                    {
                        doc.PDStatus = "inactive";
                        context.SubmitChanges();
                    }

                }
                var res1 = context.PatientVSHospitals1s.Any(r => r.PatientPatientId == patientId);
                if (res1)
                {
                    var hosps = from r in context.PatientVSHospitals1s
                                where r.PatientPatientId == patientId
                                select r;
                    foreach (var hosp in hosps)
                    {
                        hosp.PHStatus = "inactive";
                        context.SubmitChanges();
                    }

                }

                var res2 = context.PatientVSObservers.Any(r => r.PatientPatientId == patientId);
                if (res2)
                {
                    var obs = from r in context.PatientVSObservers
                              where r.PatientPatientId == patientId
                              select r;
                    foreach (var ob in obs)
                    {
                        ob.POStatus = "inactive";
                        context.SubmitChanges();
                    }
                    

                }
                
               
                
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public Boolean UpdateAdminInfo(string uid)
        {
            string[] ar = new string[5];
            try
            {
                HappyWatchDataDataContext context = new HappyWatchDataDataContext();
                ar = uid.Split('$');
                var row = ((from r in context.Hospitals
                            where r.Mail.Equals(ar[0])
                            select r
               ).Take(1)).Single();
                row.Address = ar[1];
                row.Password = ar[2];
                context.SubmitChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public Boolean UpdateObserverInfo(string uid)
        {
            string[] ar = new string[10];
            try
            {
                HappyWatchDataDataContext context = new HappyWatchDataDataContext();
                ar = uid.Split('$');
                var row = ((from r in context.Peoples
                            where r.Email.Equals(ar[0])
                            select r
               ).Take(1)).Single();
                row.Name = ar[1];
                row.Password = ar[2];
                row.ContactNumber = ar[3];
                row.BirthDay = Convert.ToDateTime(ar[4]);
                context.SubmitChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }

        }
        public Boolean UpdateUserInfo(string uid)
        {
            string[] ar = new string[15];
            try
            {
                HappyWatchDataDataContext context = new HappyWatchDataDataContext();
                ar = uid.Split('#');
                var row = ((from r in context.Peoples
                            where r.Email.Equals(ar[0])
                            select r
                ).Take(1)).Single();
                row.Name = ar[1];
                row.Password = ar[2];

                row.BirthDay = Convert.ToDateTime(ar[3]);

                context.SubmitChanges();
                var pepId = ((from r in context.Peoples
                              where r.Email.Equals(ar[0])
                              select r.id).Take(1)).Single();

                var patRow = ((from r in context.Patients
                               where r.Person_id == pepId
                               select r).Take(1)).Single();
                patRow.PhysicalCondition = ar[4];
                context.SubmitChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public Boolean PatientDeleteDoc(string uid)
        {
            string[] ar = new string[10];
            try
            {
                HappyWatchDataDataContext context = new HappyWatchDataDataContext();
                ar = uid.Split('#');
                var patpepId = ((from r in context.Peoples
                                 where r.Email.Equals(ar[0])
                                 select r.id).Take(1)).Single();
                var patid = ((from r in context.Patients
                              where r.Person_id == patpepId
                              select r.id).Take(1)).Single();
                var docpepid = ((from r in context.Peoples
                                 where r.Email.Equals(ar[1])
                                 select r.id).Take(1)).Single();
                var docid = ((from r in context.Doctors
                              where r.Person_id == docpepid
                              select r.id).Take(1)).Single();
                var rel = ((from r in context.PatientVSDoctors
                            where (r.DoctorDoctorId == docid && r.PatientPatientId == patid)
                            select r).Take(1)).Single();
                rel.PDStatus = "inactive";
                context.SubmitChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public Boolean PatientDelteHospital(string uid)
        {
            string[] ar = new string[10];
            try
            {
                ar = uid.Split('#');
                HappyWatchDataDataContext context = new HappyWatchDataDataContext();
                var patpepId = ((from r in context.Peoples
                                 where r.Email.Equals(ar[0])
                                 select r.id).Take(1)).Single();
                var patid = ((from r in context.Patients
                              where r.Person_id == patpepId
                              select r.id).Take(1)).Single();
                var hospitalId = ((from r in context.Hospitals
                                   where r.Mail.Equals(ar[1])
                                   select r.id).Take(1)).Single();
                var req = ((from r in context.PatientVSHospitals1s
                            where (r.PatientPatientId == patid && r.HospitalId == hospitalId)
                            select r).Take(1)).Single();
                req.PHStatus = "inactive";
                context.SubmitChanges();
                return true;

                
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public Boolean DocSendPrecription(string uid)
        {
            DateTime x = DateTime.Now;
            
            string[] ar = new string[10];
           
            try
            {
                HappyWatchDataDataContext context=new HappyWatchDataDataContext();
                ar = uid.Split('#');
                var docPepId = ((from r in context.Peoples
                                 where r.Email.Equals(ar[0])
                                 select r.id).Take(1)).Single();
                var docId = ((from r in context.Doctors
                              where r.Person_id == docPepId
                              select r.id).Take(1)).Single();
                var patpepId = ((from r in context.Peoples
                                 where r.Email.Equals(ar[1])
                                 select r.id).Take(1)).Single();
                var patid = ((from r in context.Patients
                              where r.Person_id == patpepId
                              select r.id).Take(1)).Single();
                Prescription req = new Prescription();
                req.PatientPatientId = patid;
                req.DoctorDoctorId = docId;
                req.Prescribed = ar[2];
                req.Time = x;
                context.Prescriptions.InsertOnSubmit(req);
                context.SubmitChanges();
                return true;

            }
            catch (Exception ex)
            {

                return false;
            }
        }

        public string SendDemoData(string uid)
        {
            string[] ar = new string[10];
            try
            {
                HappyWatchDataDataContext context = new HappyWatchDataDataContext();
                ar = uid.Split('$');
                var patId = ((from r in context.Patients
                              where r.DeviceId == ar[0]
                              select r.id).Take(1)).Single();
                var patPepId=((from r in context.Patients
                                  where r.id==patId
                                  select r.Person_id).Take(1)).Single();
                var patName = ((from r in context.Peoples
                                where r.id == patPepId
                                select r.Name).Take(1)).Single();

                HealthData data = new HealthData();
                data.DeviceId = ar[0];
                data.PulseRate = ar[1];
                data.Location = "Dhaka";
                data.PatientPatientId = patId;
                context.HealthDatas.InsertOnSubmit(data);
                context.SubmitChanges();

                int dBp = Convert.ToInt32(ar[1]);

                if (dBp > 95 || dBp < 55)
                {
                    var resD = context.PatientVSDoctors.Any(r => r.PatientPatientId == patId && r.PDStatus.Equals("active"));

                    if (resD == true)
                    {
                        var DocIds = from r in context.PatientVSDoctors
                                     where (r.PatientPatientId == patId && r.PDStatus.Equals("active"))
                                     select r.DoctorDoctorId;
                        foreach (var doc in DocIds)
                        {
                            var docPepId = ((from r in context.Doctors
                                             where r.id == doc
                                             select r.Person_id).Take(1)).Single();
                            var channelV = ((from r in context.Peoples
                                             where r.id == docPepId
                                             select r.Channel).Take(1)).Single();
                            string channel = channelV.ToString();

                            string subscriptionUri = channel;
                            HttpWebRequest sendNotificationRequest = (HttpWebRequest)WebRequest.Create(subscriptionUri);
                            sendNotificationRequest.Method = "POST";
                            string toastMessage = "<?xml version=\"1.0\" encoding=\"utf-8\"?>" +
                               "<wp:Notification xmlns:wp=\"WPNotification\">" +
                                  "<wp:Toast>" +
                                       "<wp:Text1>" + "WARNING: " + patName.ToString() + "</wp:Text1>" +
                                       "<wp:Text2>" + ar[1] + " BPM !!!" + "</wp:Text2>" +
                                       "<wp:Param>/Page2.xaml?NavigatedFrom=Toast Notification</wp:Param>" +
                                  "</wp:Toast> " +
                               "</wp:Notification>";
                            byte[] notificationMessage = Encoding.Default.GetBytes(toastMessage);

                            // Set the web request content length.
                            sendNotificationRequest.ContentLength = notificationMessage.Length;
                            sendNotificationRequest.ContentType = "text/xml";
                            sendNotificationRequest.Headers.Add("X-WindowsPhone-Target", "toast");
                            sendNotificationRequest.Headers.Add("X-NotificationClass", "2");


                            using (Stream requestStream = sendNotificationRequest.GetRequestStream())
                            {
                                requestStream.Write(notificationMessage, 0, notificationMessage.Length);
                            }

                            // Send the notification and get the response.
                            HttpWebResponse response = (HttpWebResponse)sendNotificationRequest.GetResponse();
                            string notificationStatus = response.Headers["X-NotificationStatus"];
                            string notificationChannelStatus = response.Headers["X-SubscriptionStatus"];
                            string deviceConnectionStatus = response.Headers["X-DeviceConnectionStatus"];

                        }
                    }

                    var resO = context.PatientVSObservers.Any(r => r.PatientPatientId == patId && r.POStatus.Equals("active"));
                    if (resO == true)
                    {

                        var ObIds = from r in context.PatientVSObservers
                                    where (r.PatientPatientId == patId && r.POStatus.Equals("active"))
                                    select r.ObserverObserverId;

                        foreach (var ob in ObIds)
                        {
                            var obPepId = ((from r in context.Observers
                                            where r.id == ob
                                            select r.Person_id).Take(1)).Single();
                            var channelO = ((from r in context.Peoples
                                             where r.id == obPepId
                                             select r.Channel).Take(1)).Single();
                            string Ochannel = channelO.ToString();
                            string subscriptionUri = Ochannel;
                            HttpWebRequest sendNotificationRequest = (HttpWebRequest)WebRequest.Create(subscriptionUri);
                            sendNotificationRequest.Method = "POST";
                            string toastMessage = "<?xml version=\"1.0\" encoding=\"utf-8\"?>" +
                               "<wp:Notification xmlns:wp=\"WPNotification\">" +
                                  "<wp:Toast>" +
                                       "<wp:Text1>" + "WARNING: " + patName.ToString() + "</wp:Text1>" +
                                       "<wp:Text2>" + ar[1] + " BPM !!!" + "</wp:Text2>" +
                                       "<wp:Param>/Page2.xaml?NavigatedFrom=Toast Notification</wp:Param>" +
                                  "</wp:Toast> " +
                               "</wp:Notification>";
                            byte[] notificationMessage = Encoding.Default.GetBytes(toastMessage);

                            // Set the web request content length.
                            sendNotificationRequest.ContentLength = notificationMessage.Length;
                            sendNotificationRequest.ContentType = "text/xml";
                            sendNotificationRequest.Headers.Add("X-WindowsPhone-Target", "toast");
                            sendNotificationRequest.Headers.Add("X-NotificationClass", "2");


                            using (Stream requestStream = sendNotificationRequest.GetRequestStream())
                            {
                                requestStream.Write(notificationMessage, 0, notificationMessage.Length);
                            }

                            // Send the notification and get the response.
                            HttpWebResponse response = (HttpWebResponse)sendNotificationRequest.GetResponse();
                            string notificationStatus = response.Headers["X-NotificationStatus"];
                            string notificationChannelStatus = response.Headers["X-SubscriptionStatus"];
                            string deviceConnectionStatus = response.Headers["X-DeviceConnectionStatus"];
                        }


                    }
                }
                return "true";
                
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }

        public List<string> GetAllPrescriptions(string uid)
        {
            string[] ar = new string[10];
            List<string> prescriptList = new List<string>();
            try
            {
                HappyWatchDataDataContext context = new HappyWatchDataDataContext();
                ar = uid.Split('#');
                var docPepId = ((from r in context.Peoples
                                 where r.Email.Equals(ar[0])
                                 select r.id).Take(1)).Single();
               
                var patPepId = ((from r in context.Peoples
                                 where r.Email.Equals(ar[1])
                                 select r.id).Take(1)).Single();
                var docId = ((from r in context.Doctors
                              where r.Person_id == docPepId
                              select r.id).Take(1)).Single();
                var patId = ((from r in context.Patients
                              where r.Person_id == patPepId
                              select r.id).Take(1)).Single();
                var prescriptions = from r in context.Prescriptions
                                    orderby r.id descending
                                    where (r.PatientPatientId == patId && r.DoctorDoctorId == docId)
                                    select r;
                foreach (var pres in prescriptions)
                {

                    prescriptList.Add(pres.Time.ToString() + '\n' + pres.Prescribed.ToString() + '\n');
 
                }

            }
            catch (Exception ex)
            {
 
                //do something
            }
            return prescriptList.ToList();
        }

        public List<string> PatientGetAllPrescriptions(string uid)
        {
             string[] ar=new string[10];
             List<string> prescriptList = new List<string>();
             try
             {
                 HappyWatchDataDataContext context = new HappyWatchDataDataContext();

                 var patPepId = ((from r in context.Peoples
                                  where r.Email.Equals(uid)
                                  select r.id).Take(1)).Single();

                 var patId = ((from r in context.Patients
                               where r.Person_id == patPepId
                               select r.id).Take(1)).Single();

                 var prescript = from r in context.Prescriptions
                                   orderby r.id descending
                                   where (r.PatientPatientId == patId)
                                   select r;
                 foreach (var pres in prescript)
                 {
                     string prescribe;
                     var docId = ((from r in context.Doctors
                                   where r.id == pres.DoctorDoctorId
                                   select r.Person_id).Take(1)).Single();
                     var dN = ((from r in context.Peoples
                                where r.id == docId
                                select r.Name).Take(1)).Single();
                     prescribe = dN.ToString() + "#" + pres.Prescribed.ToString() + "#" + pres.Time.ToString();
                     prescriptList.Add(prescribe);
                     
                 }
             }
             catch (Exception ex)
             {
                 prescriptList.Add("N/A");
                 
             }
             return prescriptList.ToList();
 
        }

        public string GetObName(string uid)
        {
            try
            {
                HappyWatchDataDataContext context=new HappyWatchDataDataContext();
                var pepName = ((from r in context.Peoples
                              where r.Email.Equals(uid)
                              select r.Name).Take(1)).Single();
                return pepName.ToString(); 

            }
            catch (Exception ex)
            {
                return "N/A";
            }
        }

        public string PrecriptionInfo(string uid)
        {
            string info;
            try {
                HappyWatchDataDataContext context = new HappyWatchDataDataContext();
                var pepGender=((from r in context.Peoples
                                where r.Email.Equals(uid)
                                select r.Gender).Take(1)).Single();
                var pepId = ((from r in context.Peoples
                                where r.Email.Equals(uid)
                                select r.id).Take(1)).Single();
                var pepCond = ((from r in context.Patients
                                where r.Person_id == pepId
                                select r.PhysicalCondition).Take(1)).Single();
                var pepBirth = ((from r in context.Peoples
                                 where r.Email.Equals(uid)
                                 select r.BirthDay).Take(1)).Single();
                string sent = pepGender.ToString() + '#' + pepCond.ToString()+'#'+pepBirth.ToString();
                return sent;

 
            }catch(Exception ex)
            {
                return "N/A#N/A#N/A#N/A#";
            }
        }

        public string GenerateMD5(string yourString)
        {
            return string.Join("", MD5.Create().ComputeHash(
                                Encoding.ASCII.GetBytes(yourString)).Select(s => s.ToString("x2")));
            //return "NULL";
        }
    }
}
