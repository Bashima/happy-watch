﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HappyWatchWebService
{
    public class AddRequest
    {
        public string PatientName { get; set; }
        public string PatientMail { get; set; }
        public string DoctorName { get; set; }
        public string DoctorMail { get; set; }
    }
}