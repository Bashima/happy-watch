﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HappyWatchWebService
{
    public class HospitalMod
    {
        public string Name { get; set; }
        public string Mail { get; set; }
        public string Address { get; set; }
    }
}