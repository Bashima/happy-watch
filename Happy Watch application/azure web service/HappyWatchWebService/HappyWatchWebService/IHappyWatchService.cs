﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Security.Cryptography;
namespace HappyWatchWebService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IHappyWatchService" in both code and config file together.
    [ServiceContract]
    public interface IHappyWatchService
    {
        [OperationContract]
        string InsertData(string uid);

        [OperationContract]
        Boolean UpdateAdminInfo(string uid);

        [OperationContract]
        Boolean UpdateObserverInfo(string uid);

        [OperationContract]
        Boolean UpdateDoctorInfo(string uid);

        [OperationContract]
        string VerifyEmail(string uid);

        [OperationContract]
        string VerifyDeviceId(string uid);

        [OperationContract]
        string VerifyDoctorId(string uid);

        [OperationContract]
        string InsertInfoDoctor(string uid);

        [OperationContract]
        Boolean InsertInfoObserver(string uid);


        [OperationContract]
        string InsertInfoPatient(string uid);

        [OperationContract]
        string InsertInfoAdmin(string uid);

        [OperationContract]
        string VerifyId(string uid);

        [OperationContract]
        List<ObPatient> ObservedList(string uid);

        [OperationContract]
        List<ObPatient> OwnPatientList(string uid);

        [OperationContract]
        string GetPulse(string uid);

        [OperationContract]
        string ObserverInfo(string uid);

        [OperationContract]
        string PatientInfo(string uid);


        [OperationContract]
        List<ObPatient> ObserverList(string uid);

        [OperationContract]
        string FindPulseMax(string uid);

        [OperationContract]
        string FindPulseMin(string uid);

        [OperationContract]
        string FindPulseAv(string uid);

        [OperationContract]
        string FindPulseMaxMonth(string uid);

        [OperationContract]
        string FindPulseMinMonth(string uid);

        [OperationContract]
        string FindPulseAvMonth(string uid);

        [OperationContract]
        List<string> FindPulseList(string uid);

        [OperationContract]
        string GetLocation(string uid);

        [OperationContract]
        string GetTime(String uid);

        [OperationContract]
        List<ObPatient> AllObserversList(string uid);

        [OperationContract]
        bool MakeRelationObRequest(string uid);

        // [OperationContract]
        //List<ObPatient> PatientList(string uid);

        [OperationContract]
        List<ObPatient> RelationReqs(string uid);

        [OperationContract]
        Boolean ObserverAcceptRelation(string uid);

        [OperationContract]
        Boolean ObserverRejectRelation(string uid);

        [OperationContract]
        bool MakeRelationRequest(string uid);

        [OperationContract]
        List<ObPatient> HospitalDoctorList(string uid);

        [OperationContract]
        List<ObPatient> HospitalPatientList(string uid);

        [OperationContract]
        string HospitalInfoGet(string uid);

        [OperationContract]
        List<DocSignup> SignUprequests(string uid);

        [OperationContract]
        List<AddRequest> AddRequests(string uid);

        //[OperationContract]
        //Boolean MakeRelation(string uid);

        [OperationContract]
        string MakeSign(string uid);

        [OperationContract]
        string RejectSign(string uid);

        [OperationContract]
        Boolean MakeMedRelation(string uid);

        [OperationContract]
        Boolean RejectMedRelation(string uid);

        [OperationContract]
        List<ObPatient> MakeOwnDocList(string uid);

        [OperationContract]
        string MakeDocRequest(string uid);

        [OperationContract]
        List<ObPatient> MakeAvailableDocList(string uid);

        [OperationContract]
       
        List<HospitalMod> MakeAllHosptialList();

        [OperationContract]
        List<HospitalMod> MakeOwnHosptialList(string uid);

        [OperationContract]
        Boolean MakeHospitalReq(string uid);

        [OperationContract]
        List<ObPatient> ObserverVsPatientRequest(string uid);

        [OperationContract]
        Boolean AcceptPatientReq(string uid);

        [OperationContract]
        Boolean RejecttPatientReq(string uid);

        [OperationContract]
        Boolean AcceptPatientHospitalRequest(string uid);

        [OperationContract]
        Boolean RejectPatientHospitalRequest(string uid);

        [OperationContract]
        List<ObPatient> PatientHospitalReqList(string uid);

        [OperationContract]
        string GetLastPrescription(string uid);

        [OperationContract]
        string GetDocInfo(string uid);

        [OperationContract]
        string InsertPrescription(string uid);

        [OperationContract]
        Boolean PateintDeleteObserver(string uid);

        [OperationContract]
        Boolean AdminDeletePatient(string uid);

        [OperationContract]
        Boolean AdminDeleteDoctor(string uid);

        [OperationContract]
        Boolean UpdateUserInfo(string uid);

        [OperationContract]
        Boolean PatientDeleteDoc(string uid);

        [OperationContract]
        Boolean PatientDelteHospital(string uid);

        [OperationContract]
        Boolean DocSendPrecription(string uid);

        [OperationContract]
        [WebInvoke(UriTemplate = "/DemoInsert/{uid}",Method="GET")]
        string SendDemoData(string uid);

        [OperationContract]
        Boolean DeactivePatient(string uid);

        [OperationContract]
        List<string> GetAllPrescriptions(string uid);

        [OperationContract]
        List<string> PatientGetAllPrescriptions(string uid);

        [OperationContract]
        string GetObName(string uid);

        [OperationContract]
        string PrecriptionInfo(string uid);

        [OperationContract]
        string GenerateMD5(string yourString);
    }
}
