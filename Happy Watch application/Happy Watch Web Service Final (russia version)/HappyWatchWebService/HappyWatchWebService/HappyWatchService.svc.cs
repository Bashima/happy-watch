﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Data.SqlClient;
using System.Data;
namespace HappyWatchWebService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "HappyWatchService" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select HappyWatchService.svc or HappyWatchService.svc.cs at the Solution Explorer and start debugging.
    public class HappyWatchService : IHappyWatchService
    {
        public string InsertInfoPatient(string uid)
        {

            try
            {
                using (HappyWatchDataDataContext context = new HappyWatchDataDataContext())
                {
                    string[] ar = new string[20];
                    ar = uid.Split(',');
                     var hospitalId =0;

                    People data = new People();
                    data.Name = ar[0];
                    data.Password = ar[1];
                    data.BirthDay = Convert.ToDateTime(ar[2]);
                    data.Gender = ar[3];
                    data.ContactNumber = ar[4];
                    data.Designation = ar[5];
                    data.Email = ar[6];
                    data.Status = ar[7];
                    context.Peoples.InsertOnSubmit(data);
                    context.SubmitChanges();

                    var pepId = (from r in context.Peoples
                                 where (r.Email.Equals(data.Email) && r.Password.Equals(data.Password))
                                 select r.PeopleId).Single();
                    try
                    {
                        hospitalId = ((from r in context.Hospitals 
                                           where r.Name.Equals(ar[9]) 
                                           select r.Id).Take(1)).Single();
                    }
                    catch (Exception ex)
                    {
                        hospitalId = 0;
                    }

                    Patient temp = new Patient();
                    temp.DeviceId = ar[8];
                    
                    temp.PhysicalCondition = ar[10];
                    temp.Person_PeopleId = pepId;
                    temp.Isactive = data.Status;
                   
                    context.Patients.InsertOnSubmit(temp);
                    context.SubmitChanges();



                } 
                return "true";
            }
            catch(Exception ex)
            {
                return ex.ToString();
            }

        }


        public Boolean InsertInfoObserver(string uid)
        {
            try
            {
                using (HappyWatchDataDataContext context = new HappyWatchDataDataContext())
                {
                    string[] ar = new string[10];
                    ar = uid.Split(',');
                    People data = new People();
                    data.Name = ar[0];
                    data.Password = ar[1];
                    data.BirthDay = Convert.ToDateTime(ar[2]);
                    data.Gender = ar[3];
                    data.ContactNumber = ar[4];
                    data.Designation = ar[5];
                    data.Email = ar[6];
                    data.Status = ar[7];
                    context.Peoples.InsertOnSubmit(data);
                    context.SubmitChanges();
                    var obId = (from r in context.Peoples
                                where (r.Email.Equals(data.Email) && r.Password.Equals(data.Password))
                                select r.PeopleId).Single();

                    Observer temp = new Observer();
                    temp.Person_PeopleId = obId;
                    context.Observers.InsertOnSubmit(temp);
                    context.SubmitChanges();

                } return true;
            }
            catch
            {
                return false;
            }


        }

        public string InsertInfoDoctor(string uid)
        {
            try
            {
                using (HappyWatchDataDataContext context = new HappyWatchDataDataContext())
                {
                    string[] ar = new string[10];
                    var hospitalId=0;
                    ar = uid.Split(',');
                    People data = new People();
                    data.Name = ar[0];
                    data.Password = ar[1];
                    data.BirthDay = Convert.ToDateTime(ar[2]);
                    data.Gender = ar[3];
                    data.ContactNumber = ar[4];
                    data.Designation = ar[5];
                    data.Email = ar[6];
                    data.Status = ar[7];
                    context.Peoples.InsertOnSubmit(data);
                    context.SubmitChanges();

                    Doctor doc = new Doctor();
                    doc.HospitalName = ar[9];
                    doc.LisenceId = ar[8];
                    doc.Specialist = ar[10];
                    try
                    {
                        hospitalId = ((from r in context.Hospitals
                                       where r.Name.Equals(ar[9])
                                       select r.Id).Take(1)).Single();
                    }
                    catch (Exception ex)
                    {
                        hospitalId = 1;
                    }
                    doc.HospitalId = hospitalId;
                    var docPepId = (from r in context.Peoples 
                                    where (r.Email.Equals(data.Email) && r.Password.Equals(data.Password)) 
                                    select r.PeopleId).Single();
                    doc.Person_PeopleId = docPepId;
                    doc.DoctorStatus = "pending";
                    context.Doctors.InsertOnSubmit(doc);
                    context.SubmitChanges();

                    DocSignReq req = new DocSignReq();
                    req.Status = "pending";

                    var docId=((from r in context.Doctors 
                                where r.Person_PeopleId==docPepId 
                                select r.DoctorId).Take(1)).Single();
                    req.Doctor_DoctorId = docId;
                    req.HospitalId = hospitalId;
                    context.DocSignReqs.InsertOnSubmit(req);
                    context.SubmitChanges();





                   

                } return "true";
            }
            catch(Exception ex)
            {
                return ex.ToString();
            }


        }


        public string InsertInfoAdmin(string uid)
        {
            string[] ar = new string[10];
            try
            {
                HappyWatchDataDataContext context = new HappyWatchDataDataContext();
                
                ar = uid.Split('$');
                Hospital hosp = new Hospital();
                hosp.Name = ar[0];
                hosp.Mail = ar[1];
                hosp.Address = ar[2];
                Admin ad = new Admin();
                ad.Name = ar[1];
                ad.Password = ar[3];
                context.Admins.InsertOnSubmit(ad);
                context.SubmitChanges();
                var adminId = ((from r in context.Admins
                                where (r.Name.Equals(ar[1]) && r.Password.Equals(ar[3]))
                                select r.Id).Take(1)).Single();
                hosp.Admin_Id = adminId;
                context.Hospitals.InsertOnSubmit(hosp);
                context.SubmitChanges();
                return "true";
                             
                            
            }
            catch (Exception ex)
            {
                return ar[1];
            }

        }


        public string VerifyId(string uid)
        {
            bool flag=false;
            string[] str = new string[10]; 
            using (HappyWatchDataDataContext context = new HappyWatchDataDataContext())
            {

                

                    try
                    {
                        str = uid.Split(',');
                        var res = ((from r in context.Peoples
                                    where ((r.Email.Equals(str[0])) && ((r.Password).Equals(str[1])) && ((r.Status).Equals(str[2])))
                                    select r.Designation).Take(1)).Single();


                        return res.ToString();
                    }
                    catch
                    {
                        str = uid.Split(',');
                        var res1 = ((from r in context.Admins
                                     where (r.Name.Equals(str[0])) && (r.Password.Equals(str[1]))
                                     select r.Id).Take(1)).Single();
                        return "admin";
                       
                        

                    }
                    

                
                
            }
        }

        public List<ObPatient> ObservedList(string uid)
        {
            HappyWatchDataDataContext context = new HappyWatchDataDataContext();
            
            List<ObPatient> observed = new List<ObPatient>();
            try
            {

                //  string[] str = new string[10];
                //str = uid.Split(',');


                var oPeopleId = ((from r in context.Peoples
                                 where ((r.Email.Equals(uid)))
                                 select r.PeopleId).Take(1)).Single();

                var oObserverId = ((from r in context.Observers where (r.Person_PeopleId == oPeopleId) select r.ObserverId).Take(1)).Single();

                var patientIdList = from r in context.PatientVSObservers where (r.ObserverObserverId == oObserverId && r.POStatus.Equals("active")) select r.PatientPatientId;

                foreach (var pId in patientIdList)
                {
                    ObPatient tempOb = new ObPatient();
                    var pPeopleId = ((from r in context.Patients where (r.PatientId == pId) select r.Person_PeopleId).Take(1)).Single();
                    tempOb.pName = ((from r in context.Peoples where (r.PeopleId == pPeopleId) select r.Name).Take(1)).Single();
                    tempOb.pMail = ((from r in context.Peoples where (r.PeopleId == pPeopleId) select r.Email).Take(1)).Single();
                    observed.Add(tempOb);

                }
            }
            catch (Exception ex)
            {
                ////////////////////////////////put code here
            }
            return observed.ToList();

        }

        public List<ObPatient> OwnPatientList(string uid)
        {
            List<ObPatient> pat = new List<ObPatient>();
            try
            {
                HappyWatchDataDataContext context = new HappyWatchDataDataContext();
                var pepId = ((from r in context.Peoples
                              where r.Email.Equals(uid)
                              select r.PeopleId).Take(1)).Single();
                var docId=((from r in context.Doctors
                              where r.Person_PeopleId==pepId
                              select r.DoctorId).Take(1)).Single();
                var PatientIds = from r in context.PatientVSDoctors
                                 where (r.DoctorDoctorId == docId && r.PDStatus.Equals("active"))
                                 select r.PatientPatientId;

                foreach (var pId in PatientIds)
                {
                    ObPatient tempOb = new ObPatient();
                    var pPeopleId = (from r in context.Patients where (r.PatientId == pId) select r.Person_PeopleId).Single();
                    tempOb.pName = (from r in context.Peoples where (r.PeopleId == pPeopleId) select r.Name).Single();
                    tempOb.pMail = (from r in context.Peoples where (r.PeopleId == pPeopleId) select r.Email).Single();
                    pat.Add(tempOb);

                }

            }
            catch (Exception ex)
            {
                ////////do some
            }
            return pat.ToList();
        }

        public List<ObPatient> ObserverList(string uid)
        {
            HappyWatchDataDataContext context = new HappyWatchDataDataContext();
            ObPatient tempOb = new ObPatient();
            List<ObPatient> observed = new List<ObPatient>();
            try
            {

                string[] str = new string[10];
                str = uid.Split(',');


                var pPeopleId = (from r in context.Peoples
                                 where (r.Email.Equals(str[0]))
                                 select r.PeopleId).Single();

                var pPatientId = (from r in context.Patients where (r.Person_PeopleId == pPeopleId) select r.PatientId).Single();

                var observerIdList = from r in context.PatientVSObservers
                                     where (r.PatientPatientId == pPatientId && r.POStatus.Equals("active"))
                                     select r.ObserverObserverId;

                foreach (var pId in observerIdList)
                {
                    var pObserverId = (from r in context.Observers where (r.ObserverId == pId) select r.Person_PeopleId).Single();
                    tempOb.pName = (from r in context.Peoples where (r.PeopleId == pObserverId) select r.Name).Single();
                    tempOb.pMail = (from r in context.Peoples where (r.PeopleId == pObserverId) select r.Email).Single();
                    observed.Add(tempOb);

                }
            }
            catch (Exception ex)
            {
                ////////////////////////////////put code here
            }
            return observed.ToList();


        }
        public string GetPulse(string uid)
        {
            try
            {
                HappyWatchDataDataContext context = new HappyWatchDataDataContext();


                var pepId = (from r in context.Peoples where r.Email.Equals(uid) select r.PeopleId).Single();
                var patientId = (from r in context.Patients where r.Person_PeopleId == pepId select r.PatientId).Single();
                var bloodPulse = ((from r in context.HealthDatas
                                   orderby r.DataId descending
                                   where r.PatientPatientId == patientId
                                   select r.PulseRate).Take(1)).Single();

                return bloodPulse;
            }
            catch (Exception ex)
            {
                return "N/A";
            }


        }

        public string FindPulseMax(string uid)
        {
            DateTime x = DateTime.Now;
            try
            {
                HappyWatchDataDataContext context = new HappyWatchDataDataContext();

                var pepId = (from r in context.Peoples where r.Email.Equals(uid) select r.PeopleId).Single();
                var patientId = (from r in context.Patients where r.Person_PeopleId == pepId select r.PatientId).Single();
                var res = from r in context.HealthDatas
                          where (r.PatientPatientId == patientId && ((r.Time.Value.Day).Equals(x.Day)) && ((r.Time.Value.Month).Equals(x.Month)) && ((r.Time.Value.Year).Equals(x.Year)))
                          select Convert.ToDouble(r.PulseRate);
                // var res = (from r in context.HealthDatas where r.PatientPatientId == patientId select r.Time.Day).Single();
                var resMax = res.Max();
                return resMax.ToString();
                //return uid;

            }
            catch (Exception e)
            {
                return "N/A";

            }
        }
        public string FindPulseMin(string uid)
        {
            DateTime x = DateTime.Now;
            try
            {
                HappyWatchDataDataContext context = new HappyWatchDataDataContext();

                var pepId = (from r in context.Peoples where r.Email.Equals(uid) select r.PeopleId).Single();
                var patientId = (from r in context.Patients where r.Person_PeopleId == pepId select r.PatientId).Single();
                var res = from r in context.HealthDatas
                          where (r.PatientPatientId == patientId && ((r.Time.Value.Day).Equals(x.Day)) && ((r.Time.Value.Month).Equals(x.Month)) && ((r.Time.Value.Year).Equals(x.Year)))
                          select Convert.ToDouble(r.PulseRate);
                var resMin = res.Min();
                return resMin.ToString();

            }
            catch (Exception e)
            {
                return "N/A";

            }
        }

        public string FindPulseAv(string uid)
        {
            DateTime x = DateTime.Now;
            try
            {
                HappyWatchDataDataContext context = new HappyWatchDataDataContext();

                var pepId = (from r in context.Peoples where r.Email.Equals(uid) select r.PeopleId).Single();
                var patientId = (from r in context.Patients where r.Person_PeopleId == pepId select r.PatientId).Single();
                var res = from r in context.HealthDatas
                          where (r.PatientPatientId == patientId && ((r.Time.Value.Day).Equals(x.Day)) && ((r.Time.Value.Month).Equals(x.Month)) && ((r.Time.Value.Year).Equals(x.Year)))
                          select Convert.ToDouble(r.PulseRate);
                double resAv = res.Average();
                return resAv.ToString();

            }
            catch (Exception e)
            {
                return "N/A";

            }
        }

        public string FindPulseMaxMonth(string uid)
        {
            DateTime x = DateTime.Now;
            try
            {
                HappyWatchDataDataContext context = new HappyWatchDataDataContext();

                var pepId = (from r in context.Peoples where r.Email.Equals(uid) select r.PeopleId).Single();
                var patientId = (from r in context.Patients where r.Person_PeopleId == pepId select r.PatientId).Single();
                var res = from r in context.HealthDatas
                          where (r.PatientPatientId == patientId && ((r.Time.Value.Month).Equals(x.Month)) && ((r.Time.Value.Year).Equals(x.Year)))
                          select Convert.ToDouble(r.PulseRate);
                double resMax = res.Max();
                return resMax.ToString();

            }
            catch (Exception e)
            {
                return "N/A";

            }

        }

        public string FindPulseMinMonth(string uid)
        {
            DateTime x = DateTime.Now;
            try
            {
                HappyWatchDataDataContext context = new HappyWatchDataDataContext();

                var pepId = (from r in context.Peoples where r.Email.Equals(uid) select r.PeopleId).Single();
                var patientId = (from r in context.Patients where r.Person_PeopleId == pepId select r.PatientId).Single();
                var res = from r in context.HealthDatas
                          where (r.PatientPatientId == patientId && ((r.Time.Value.Month).Equals(x.Month)) && ((r.Time.Value.Year).Equals(x.Year)))
                          select Convert.ToDouble(r.PulseRate);
                double resMin = res.Min();
                return resMin.ToString();

            }
            catch (Exception e)
            {
                return "N/A";

            }

        }

        public string FindPulseAvMonth(string uid)
        {
            DateTime x = DateTime.Now;
            try
            {
                HappyWatchDataDataContext context = new HappyWatchDataDataContext();

                var pepId = (from r in context.Peoples where r.Email.Equals(uid) select r.PeopleId).Single();
                var patientId = (from r in context.Patients where r.Person_PeopleId == pepId select r.PatientId).Single();
                var res = from r in context.HealthDatas
                          where (r.PatientPatientId == patientId && ((r.Time.Value.Month).Equals(x.Month)) && ((r.Time.Value.Year).Equals(x.Year)))
                          select Convert.ToDouble(r.PulseRate);
                double resAv = res.Average();
                return resAv.ToString();

            }
            catch (Exception e)
            {
                return "N/A";

            }

        }


        public string ObserverInfo(string uid)
        {

            try
            {
                string send;
                HappyWatchDataDataContext context = new HappyWatchDataDataContext();
                var pepId = ((from r in context.Peoples
                             where (r.Email.Equals(uid))
                             select r.PeopleId).Take(1)).Single();
                var obId = ((from r in context.Observers
                             where r.Person_PeopleId == pepId
                             select r.ObserverId).Take(1)).Single();
                var oName = ((from r in context.Peoples where r.PeopleId== pepId  select r.Name).Take(1)).Single();

                var oMail = ((from r in context.Peoples where r.PeopleId == pepId  select r.Email).Take(1)).Single();

                var oDob = ((from r in context.Peoples where r.PeopleId==pepId  select r.BirthDay).Take(1)).Single();

                var oGender = ((from r in context.Peoples where r.PeopleId==pepId  select r.Gender).Take(1)).Single();

                var oContact = ((from r in context.Peoples where r.PeopleId==pepId  select r.ContactNumber).Take(1)).Single();

                var oPass = ((from r in context.Peoples where r.PeopleId == pepId select r.Password).Take(1)).Single();


                send = oName.ToString() + "#" + oMail.ToString() + "#" + oDob.ToString() + "#" + oGender.ToString() + "#" + oContact.ToString()+"#" +oPass.ToString();

                return send;

            }
            catch (Exception e)
            {
                return "N/A#N/A#N/A#N/A#N/A#N/A#N/A#N/A#N/A";
                //return e.ToString();
            }



        }

        //public List<string> PatientInfo(string uid)
        public string PatientInfo(string uid)
        {
            string send;
            try
            {



                HappyWatchDataDataContext context = new HappyWatchDataDataContext();
                var pepId = ((from r in context.Peoples
                             where (r.Email.Equals(uid))
                             select r.PeopleId).Take(1)).Single();
                var pName = ((from r in context.Peoples where pepId == r.PeopleId select r.Name).Take(1)).Single();

                var pPass = ((from r in context.Peoples where pepId == r.PeopleId select r.Password).Take(1)).Single();

                var pMail = ((from r in context.Peoples where pepId == r.PeopleId select r.Email).Take(1)).Single();

                var pDob = ((from r in context.Peoples where pepId == r.PeopleId select r.BirthDay).Take(1)).Single();

                var pGender = ((from r in context.Peoples where pepId == r.PeopleId select r.Gender).Take(1)).Single();

                var pContact = ((from r in context.Peoples where pepId == r.PeopleId select r.ContactNumber).Take(1)).Single();

                var pDeviceId = ((from r in context.Patients where r.Person_PeopleId == pepId select r.DeviceId).Take(1)).Single();

                var pCondition = ((from r in context.Patients where r.Person_PeopleId == pepId select r.PhysicalCondition).Take(1)).Single();

                send = pName.ToString() + "#" + pMail.ToString() + "#" + pDob.ToString() + "#" + pGender.ToString() + "#" + pContact.ToString() + "#" + pDeviceId.ToString() + "#" + pCondition.ToString()+"#"+pPass.ToString();

                return send;

            }
            catch (Exception e)
            {

                /////////////////////////////////
                return e.ToString();
            }



        }

        public List<string> FindPulseList(string uid)
        {
            List<string> error = new List<string>();
            error.Add("error");
            try
            {
                HappyWatchDataDataContext context = new HappyWatchDataDataContext();
                var pepId = (from r in context.Peoples where r.Email.Equals(uid) select r.PeopleId).Single();
                var patientId = (from r in context.Patients where r.Person_PeopleId == pepId select r.PatientId).Single();
                var pulseList = from r in context.HealthDatas orderby r.DataId descending where r.PatientPatientId == patientId select r.PulseRate;
                return pulseList.ToList();
            }
            catch (Exception ex)
            {
                ////////////do something
                return error;
            }

        }

        public string GetLocation(string uid)
        {
            return "Dhaka";
        }

        public string GetTime(String uid)
        {
            try
            {
                HappyWatchDataDataContext context = new HappyWatchDataDataContext();
                var pepId = (from r in context.Peoples where r.Email.Equals(uid) select r.PeopleId).Single();
                var patientId = (from r in context.Patients where r.Person_PeopleId == pepId select r.PatientId).Single();
                var time = (from r in context.HealthDatas where r.PatientPatientId == patientId select r.Time).Single();
                return time.ToString();
            }
            catch (Exception ex)
            {
                return "N/A";
            }
        }

        public List<ObPatient> AllObserversList()
        {
            HappyWatchDataDataContext context = new HappyWatchDataDataContext();
            var pepId = from r in context.Observers select r.Person_PeopleId;

            List<ObPatient> ob = new List<ObPatient>();
            foreach (var id in pepId)
            {
                ObPatient temp = new ObPatient();
                var obName = (from r in context.Peoples
                              where (r.PeopleId == id && r.Status.Equals("active"))
                              select r.Name).Single();
                var obMail = (from r in context.Peoples
                              where (r.PeopleId == id && r.Status.Equals("active"))
                              select r.Email).Single();
                temp.pName = obName.ToString();
                temp.pMail = obMail.ToString();

                ob.Add(temp);



            }

            return ob.ToList();
        }

        public bool MakeRelationObRequest(string uid)
        {
            try
            {
                PatientVSObserver data = new PatientVSObserver();
                string[] str = new string[10];
                str = uid.Split('#');
                HappyWatchDataDataContext context = new HappyWatchDataDataContext();
                var patientPeopleId = ((from r in context.Peoples
                                        where r.Email.Equals(str[0])
                                        select r.PeopleId).Take(1)).Single();
                var observerPeopleId = ((from r in context.Peoples
                                         where r.Email.Equals(str[1])
                                         select r.PeopleId).Take(1)).Single();
                var patientId = ((from r in context.Patients
                                  where r.Person_PeopleId == patientPeopleId
                                  select r.PatientId).Take(1)).Single();
                var observerId = ((from r in context.Observers
                                   where r.Person_PeopleId == observerPeopleId
                                   select r.ObserverId).Take(1)).Single();
                data.PatientPatientId = patientId;
                data.ObserverObserverId = observerId;
                data.POStatus = "pending";
                data.Relation = str[2];
                context.PatientVSObservers.InsertOnSubmit(data);
                context.SubmitChanges();
                return true;
            }
            catch (Exception e)
            {
                return false;
            }




        }

        /*    public List<ObPatient> PatientList(string uid)
            {
                HappyWatchDataDataContext context = new HappyWatchDataDataContext();
                ObPatient tempOb = new ObPatient();
                List<ObPatient> observed = new List<ObPatient>();
                try
                {

                    //  string[] str = new string[10];
                    //str = uid.Split(',');


                    var dPeopleId = (from r in context.Peoples
                                     where ((r.Email.Equals(uid)))
                                     select r.PeopleId).Single();

                    var dDocId = (from r in context.Doctors where (r.Person_PeopleId == dPeopleId) select r.DoctorId).Single();

                    var patientIdList = from r in context.PatientVSDoctors where (r.DoctorDoctorId == dDocId) select r.PatientPatientId;

                    foreach (var pId in patientIdList)
                    {
                        var pPeopleId = (from r in context.Patients where (r.PatientId == pId) select r.Person_PeopleId).Single();
                        tempOb.pName = (from r in context.Peoples where (r.PeopleId == pPeopleId) select r.Name).Single();
                        tempOb.pMail = (from r in context.Peoples where (r.PeopleId == pPeopleId) select r.Email).Single();
                        observed.Add(tempOb);

                    }
                }
                catch (Exception ex)
                {
                    ////////////////////////////////put code here
                }
                return observed.ToList();
 
            }*/


        public bool MakeRelationRequest(string uid)
        {
            try
            {
                DocVSpatientReq data = new DocVSpatientReq();
                PatientVSDoctor data1 = new PatientVSDoctor();
                
                string[] str = new string[10];
                str = uid.Split(',');
                HappyWatchDataDataContext context = new HappyWatchDataDataContext();
                var patientPeopleId = ((from r in context.Peoples
                                        where r.Email.Equals(str[0])
                                        select r.PeopleId).Take(1)).Single();
                var docPeopleId = ((from r in context.Peoples
                                         where r.Email.Equals(str[1])
                                         select r.PeopleId).Take(1)).Single();
                var patientId = ((from r in context.Patients
                                  where r.Person_PeopleId == patientPeopleId
                                  select r.PatientId).Take(1)).Single();
                var docId = ((from r in context.Doctors
                                   where r.Person_PeopleId == docPeopleId
                                   select r.DoctorId).Take(1)).Single();
                data.PatientPatientId= patientId;
                data.DoctorDoctorId = docId;
                data.Status = "pending";

                context.DocVSpatientReqs.InsertOnSubmit(data);
                context.SubmitChanges();

                data1.PatientPatientId = patientId;
                data1.DoctorDoctorId = docId;
                data1.PDStatus = "pending";
                data1.Relation = "med";


                context.PatientVSDoctors.InsertOnSubmit(data1);
                context.SubmitChanges();
                
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
 
        }

        public List<ObPatient> HospitalDoctorList(string uid)
        {
            List<ObPatient> docList = new List<ObPatient>();
            try
            {
                HappyWatchDataDataContext context = new HappyWatchDataDataContext();
              


                var adminId = ((from r in context.Admins
                                where r.Name.Equals(uid)
                                select r.Id).Take(1)).Single();
                var hospitalId = ((from r in context.Hospitals
                                   where r.Admin_Id == adminId
                                   select r.Id).Take(1)
                                     ).Single();

                var docIdList = from r in context.Doctors
                                where r.HospitalId == hospitalId && r.DoctorStatus.Equals("active")
                                select r.DoctorId;

                foreach (var id in docIdList)
                {
                    ObPatient temp = new ObPatient();
                    var docPepId = ((from r in context.Doctors
                                     where r.DoctorId == id
                                     select r.Person_PeopleId).Take(1)).Single();

                    temp.pName = ((from r in context.Peoples
                                   where r.PeopleId == docPepId
                                   select r.Name).Take(1)).Single();


                    temp.pMail = ((from r in context.Peoples
                                   where r.PeopleId == docPepId
                                   select r.Email).Take(1)).Single();
                    docList.Add(temp);



                }
            }
            catch (Exception ex)
            {
                //////////////
            }
            return docList.ToList();
        }

        public List<ObPatient> HospitalPatientList(string uid)
        {
            List<ObPatient> patientList = new List<ObPatient>();
            try
            {
                HappyWatchDataDataContext context = new HappyWatchDataDataContext();



                var adminId = ((from r in context.Admins
                                where r.Name.Equals(uid)
                                select r.Id).Take(1)).Single();
                var hospitalId = ((from r in context.Hospitals
                                   where r.Admin_Id == adminId
                                   select r.Id).Take(1)
                                     ).Single();
                var patientIdList = from r in context.PatientVSHospitals1s
                                    where r.HospitalId == hospitalId
                                    select r.PatientPatientId;

                foreach(var id in patientIdList)
                {
                    ObPatient temp = new ObPatient();
                    var patientPepId = ((from r in context.Patients
                                         where r.PatientId == id
                                         select r.Person_PeopleId).Take(1)
                                           ).Single();

                    temp.pName = ((from r in context.Peoples
                                   where r.PeopleId == patientPepId
                                   select r.Name).Take(1)).Single();

                    temp.pMail = ((from r in context.Peoples
                                   where r.PeopleId == patientPepId
                                   select r.Email).Take(1)).Single();

                    patientList.Add(temp);

                }

            }
            catch (Exception ex)
            {
                //
            }
            return patientList.ToList();


  
        }

        public string HospitalInfoGet(string uid)
        {
            try
            {
                HappyWatchDataDataContext context = new HappyWatchDataDataContext();
                var adminId=((from r in context.Admins
                             where r.Name.Equals(uid)
                             select r.Id).Take(1)).Single();
                var hospitalId = ((from r in context.Hospitals
                                   where r.Admin_Id == adminId
                                   select r.Id).Take(1)).Single();
                string name = ((from r in context.Hospitals
                                where r.Id == hospitalId
                                select r.Name).Take(1)).Single();

                string address = ((from r in context.Hospitals
                                where r.Id == hospitalId
                                select r.Address).Take(1)).Single();

                string mail = ((from r in context.Hospitals
                                where r.Id == hospitalId
                                select r.Mail).Take(1)).Single();
                var patients = from r in context.PatientVSHospitals1s where( r.HospitalId == hospitalId && r.PHStatus.Equals("active")) select r.PatientPatientId;
                int countPatient=0;
                int countDoc=0;
                foreach(var p in patients)
                {
                    countPatient++;

                }
                var doctors=from r in context.Doctors where r.HospitalId== hospitalId && r.DoctorStatus.Equals("active")
                            select r.DoctorId;

                foreach(var d in doctors)
                {
                    countDoc++;
                }
                string res=name+"$"+mail+"$"+address+"$"+countPatient.ToString()+"$"+countDoc.ToString();
                return res;
                

            }
            catch (Exception ex)
            {
                return "N/A";
            }

 
        }

        public List<DocSignup> SignUprequests(string uid)
        {
            List<DocSignup> req=new List<DocSignup>();
            try
            {
                HappyWatchDataDataContext context = new HappyWatchDataDataContext();
                var adminId = ((from r in context.Admins
                                where r.Name.Equals(uid)
                                select r.Id).Take(1)).Single();
                var hospitalId = ((from r in context.Hospitals
                                   where r.Admin_Id == adminId
                                   select r.Id).Take(1)).Single();

                DocSignReq request = new DocSignReq();
                var reqs=from r in context.DocSignReqs
                         where (r.HospitalId==hospitalId && r.Status.Equals("pending"))
                         select r.Doctor_DoctorId;
                

                foreach (var res in reqs)
                {
                    DocSignup temp = new DocSignup();
                    var pepId = ((from r in context.Doctors
                           where r.DoctorId == res
                           select r.Person_PeopleId).Take(1)).Single();
                    temp.dName = ((from r in context.Peoples
                             where r.PeopleId == pepId
                             select r.Name).Take(1)).Single();

                    temp.dMail = ((from r in context.Peoples
                                   where r.PeopleId == pepId
                                   select r.Email).Take(1)).Single();

                    temp.dLicense = ((from r in context.Doctors
                                      where r.Person_PeopleId == pepId
                                      select r.LisenceId).Take(1)).Single();
                    temp.specialist = ((from r in context.Doctors
                                        where r.Person_PeopleId == pepId
                                        select r.Specialist).Take(1)).Single();

                    req.Add(temp);
                    
                }

                         
            }
            catch (Exception ex)
            {
                ////
            }
            return req.ToList();
 
        }

        public List<AddRequest> AddRequests(string uid)
        {
            List<AddRequest> reqs = new List<AddRequest>();
            try
            {
                HappyWatchDataDataContext context = new HappyWatchDataDataContext();
                var adminId = ((from r in context.Admins
                                where r.Name.Equals(uid) 
                                select r.Id).Take(1)).Single();

                
                var hospitalId = ((from r in context.Hospitals
                                   where r.Admin_Id == adminId
                                   select r.Id).Take(1)).Single();
                var reqIds = from r in context.DocVSpatientReqs
                             where( r.HospitalId == hospitalId && r.Status.Equals("pending"))
                             select r.DPRequestId;
                foreach (var ids in reqIds)
                {
                    AddRequest ob = new AddRequest();
                    var patientId = ((from r in context.DocVSpatientReqs
                                      where r.DPRequestId == ids
                                      select r.PatientPatientId).Take(1)).Single();
                    var docId = ((from r in context.DocVSpatientReqs
                                  where r.DPRequestId == ids
                                  select r.DoctorDoctorId).Take(1)).Single();
                    var patientPepId=((from r in context.Patients
                                           where r.PatientId==patientId
                                           select r.Person_PeopleId).Take(1)).Single();
                    var docPepId = ((from r in context.Doctors
                                     where r.DoctorId == docId
                                     select r.Person_PeopleId).Take(1)).Single();
                    ob.PatientName = ((from r in context.Peoples
                                       where r.PeopleId == patientPepId
                                       select r.Name).Take(1)).Single();
                    ob.PatientMail = ((from r in context.Peoples
                                       where r.PeopleId == patientPepId
                                       select r.Email).Take(1)).Single();
                    ob.DoctorName = ((from r in context.Peoples
                                      where r.PeopleId == docPepId
                                      select r.Name).Take(1)).Single();
                    ob.DoctorMail = ((from r in context.Peoples
                                      where r.PeopleId == docPepId
                                      select r.Email).Take(1)).Single();
                    reqs.Add(ob);
                    


                }




            }
            catch (Exception ex)
            {
                ////
            }
            return reqs.ToList();

 
        }

       /* public Boolean MakeRelation(string uid)
        {
            try
            {
                string[] ar = new string[10];
                HappyWatchDataDataContext context = new HappyWatchDataDataContext();
                ar = uid.Split(',');

            }
            catch (Exception ex)
            {
 
            }

        }*/

        public string MakeSign(string uid)
        {
           
            try
            {
                HappyWatchDataDataContext context = new HappyWatchDataDataContext();
               

                var docPepId = ((from r in context.Peoples
                              where r.Email.Equals(uid) && r.Designation.Equals("doctor")
                              select r.PeopleId).Take(1)).Single();

                var docId = ((from r in context.Doctors
                              where r.Person_PeopleId == docPepId
                              select r.DoctorId).Take(1)).Single();

                var req = ((from r in context.DocSignReqs
                            where r.Doctor_DoctorId == docId
                            select r).Take(1)).Single();

                req.Status = "active";
                
                context.SubmitChanges();

                var pep=((from r in context.Peoples
                              where r.PeopleId==docPepId
                              select r).Take(1)).Single();
                pep.Status = "active";
                
                context.SubmitChanges();


                var docTable = ((from r in context.Doctors
                                 where r.Person_PeopleId == docPepId
                                 select r
                                    ).Take(1)).Single();
                docTable.DoctorStatus = "active";
                context.SubmitChanges();
                return "true";

                
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }

        public string RejectSign(string uid)
        {
            try
            {
                HappyWatchDataDataContext context = new HappyWatchDataDataContext();


                var docPepId = ((from r in context.Peoples
                                 where r.Email.Equals(uid) && r.Designation.Equals("doctor")
                                 select r.PeopleId).Take(1)).Single();

                var docId = ((from r in context.Doctors
                              where r.Person_PeopleId == docPepId
                              select r.DoctorId).Take(1)).Single();

                var req = ((from r in context.DocSignReqs
                            where r.Doctor_DoctorId == docId
                            select r).Take(1)).Single();

                req.Status = "reject";

                context.SubmitChanges();

                var pep = ((from r in context.Peoples
                            where r.PeopleId == docPepId
                            select r).Take(1)).Single();
                pep.Status = "reject";

                context.SubmitChanges();


                var docTable = ((from r in context.Doctors
                                 where r.Person_PeopleId == docPepId
                                 select r
                                    ).Take(1)).Single();
                docTable.DoctorStatus = "reject";
                context.SubmitChanges();
                return "true";


            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
 
        }

        public Boolean MakeMedRelation(string uid)
        {
            string[] ar = new string[10];
            try
            {
                HappyWatchDataDataContext context = new HappyWatchDataDataContext();
                ar = uid.Split('#');
                var patientPepId = ((from r in context.Peoples
                                     where r.Email.Equals(ar[0])
                                     select r.PeopleId).Take(1)).Single();
                var docPepId = ((from r in context.Peoples
                                 where r.Email.Equals(ar[1])
                                 select r.PeopleId).Take(1)).Single();
                var PatientId = ((from r in context.Patients
                                  where r.Person_PeopleId == patientPepId
                                  select r.PatientId).Take(1)).Single();
                var DocId = ((from r in context.Doctors
                              where r.Person_PeopleId == docPepId
                              select r.DoctorId).Take(1)).Single();

                var reqRow = ((from r in context.DocVSpatientReqs
                               where (r.DoctorDoctorId == DocId && r.PatientPatientId == PatientId)
                               select r).Take(1)).Single();
                reqRow.Status = "active";
                context.SubmitChanges();
                var relRow=((from r in context.PatientVSDoctors
                                 where (r.DoctorDoctorId == DocId && r.PatientPatientId == PatientId)
                               select r).Take(1)).Single();
                relRow.PDStatus = "active";
                context.SubmitChanges();



                return true;

            }
            catch (Exception ex)
            {
                return false;

            }
        }

        public Boolean RejectMedRelation(string uid)
        {
            string[] ar = new string[10];
            try
            {
                HappyWatchDataDataContext context = new HappyWatchDataDataContext();
                ar = uid.Split('#');
                var patientPepId = ((from r in context.Peoples
                                     where r.Email.Equals(ar[0])
                                     select r.PeopleId).Take(1)).Single();
                var docPepId = ((from r in context.Peoples
                                 where r.Email.Equals(ar[1])
                                 select r.PeopleId).Take(1)).Single();
                var PatientId = ((from r in context.Patients
                                  where r.Person_PeopleId == patientPepId
                                  select r.PatientId).Take(1)).Single();
                var DocId = ((from r in context.Doctors
                              where r.Person_PeopleId == docPepId
                              select r.DoctorId).Take(1)).Single();

                var reqRow = ((from r in context.DocVSpatientReqs
                               where (r.DoctorDoctorId == DocId && r.PatientPatientId == PatientId)
                               select r).Take(1)).Single();
                reqRow.Status = "inactive";
                context.SubmitChanges();
                var relRow = ((from r in context.PatientVSDoctors
                               where (r.DoctorDoctorId == DocId && r.PatientPatientId == PatientId)
                               select r).Take(1)).Single();
                relRow.PDStatus = "inactive";
                context.SubmitChanges();



                return true;

            }
            catch (Exception ex)
            {
                return false;

            }
 
        }

        public List<ObPatient> MakeOwnDocList(string uid)
        {
            List<ObPatient> docList = new List<ObPatient>();
            try
            {
                HappyWatchDataDataContext context = new HappyWatchDataDataContext();
                var pepId = ((from r in context.Peoples
                              where r.Email.Equals(uid)
                              select r.PeopleId).Take(1)).Single();
                var patientId = ((from r in context.Patients
                                  where r.Person_PeopleId == pepId
                                  select r.PatientId).Take(1)).Single();
                var docids = from r in context.PatientVSDoctors
                             where (r.PatientPatientId == patientId && r.PDStatus.Equals("active"))
                             select r.DoctorDoctorId;
                foreach (var id in docids)
                {
                    ObPatient temp = new ObPatient();
                    var docPepId = ((from r in context.Doctors
                                     where r.DoctorId == id
                                     select r.Person_PeopleId).Take(1)).Single();
                    temp.pName = ((from r in context.Peoples
                                   where r.PeopleId == docPepId
                                   select r.Name).Take(1)).Single();
                    temp.pMail = ((from r in context.Peoples
                                   where r.PeopleId == docPepId
                                   select r.Email).Take(1)).Single();
                    docList.Add(temp);

                    
                }
            }
            catch (Exception ex)
            {
                ////do something plz
            }

            return docList.ToList();
        }

        public Boolean MakeDocRequest(string uid)
        {
            string[] ar = new string[10];
            try
            {
                HappyWatchDataDataContext context=new HappyWatchDataDataContext();
                DocVSpatientReq req = new DocVSpatientReq();
                PatientVSDoctor pVd = new PatientVSDoctor();
                ar = uid.Split('#');
                var docPepId = ((from r in context.Peoples
                                 where r.Email.Equals(ar[1])
                                 select r.PeopleId
                                   ).Take(1)).Single();
                var docId = ((from r in context.Doctors
                              where r.Person_PeopleId == docPepId
                              select r.DoctorId).Take(1)).Single();

                var patientPepId = ((from r in context.Peoples
                                     where r.Email.Equals(ar[0])
                                     select r.PeopleId).Take(1)).Single();
                var patId = ((from r in context.Patients
                              where r.Person_PeopleId == patientPepId
                              select r.PatientId).Take(1)).Single();
                var hospitalId = ((from r in context.Doctors
                                   where r.Person_PeopleId == docPepId
                                   select r.HospitalId).Take(1)).Single();

                req.DoctorDoctorId = docId;
                req.PatientPatientId = patId;
                req.HospitalId = hospitalId;
                req.Status = "pending";

                context.DocVSpatientReqs.InsertOnSubmit(req);
                context.SubmitChanges();

                pVd.DoctorDoctorId = docId;
                pVd.PatientPatientId = patId;
                pVd.PDStatus = "pending";
                pVd.Relation = "med";
                context.PatientVSDoctors.InsertOnSubmit(pVd);
                context.SubmitChanges();
                return true;




            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public List<ObPatient> MakeAvailableDocList(string uid)
        {
            List<ObPatient> docList = new List<ObPatient>();
            try
            {
                HappyWatchDataDataContext context=new HappyWatchDataDataContext();
                var patPepId = ((from r in context.Peoples
                                 where r.Email.Equals(uid)
                                 select r.PeopleId).Take(1)).Single();
                var patId = ((from r in context.Patients
                              where r.Person_PeopleId == patPepId
                              select r.PatientId).Take(1)).Single();
                var hospitalIds = from r in context.PatientVSHospitals1s
                                  where r.PatientPatientId == patId && r.PHStatus.Equals("active")
                                  select r.HospitalId;
                foreach(var hospitalId in hospitalIds)
                {
                    var docIds = from r in context.Doctors
                                 where r.HospitalId == hospitalId
                                 select r.DoctorId;
                    foreach(var docId in docIds)
                    {
                        ObPatient temp = new ObPatient();
                        var pepId=((from r in context.Doctors
                                        where r.DoctorId==docId
                                        select r.Person_PeopleId).Take(1)).Single();
                        temp.pName = ((from r in context.Peoples
                                       where r.PeopleId == pepId
                                       select r.Name).Take(1)).Single();
                        temp.pMail = ((from r in context.Peoples
                                       where r.PeopleId == pepId
                                       select r.Email).Take(1)).Single();
                        docList.Add(temp);
                    }
                }
            }
            catch (Exception ex)
            {
                /////make something here
            }
            return docList.ToList();
        }

        public List<ObPatient> RelationReqs(string uid)
        {
            List<ObPatient> ob = new List<ObPatient>();
            try
            {
                HappyWatchDataDataContext context=new HappyWatchDataDataContext();
                var pepId = ((from r in context.Peoples
                              where r.Email.Equals(uid) && r.Designation.Equals("observer")
                              select r.PeopleId).Take(1)).Single();
                var obId = ((from r in context.Observers
                             where r.Person_PeopleId == pepId
                             select r.ObserverId).Take(1)).Single();
                var reqIds = from r in context.PatientVSObservers
                             where r.ObserverObserverId == obId
                             select r.PatientPatientId;
                foreach (var req in reqIds)
                {
                    ObPatient temp = new ObPatient();
                    var PatPepId = ((from r in context.Patients
                                  where r.PatientId == req
                                  select r.Person_PeopleId).Take(1)).Single();
                    temp.pName = ((from r in context.Peoples
                                   where r.PeopleId == PatPepId
                                   select r.Name).Take(1)).Single();
                    temp.pMail = ((from r in context.Peoples
                                    where r.PeopleId == PatPepId
                                    select r.Email).Take(1)).Single();
                    ob.Add(temp);

 


                }

            }
            catch (Exception ex)
            {
                /////do something
            }
            return ob.ToList();
        }

        public Boolean ObserverAcceptRelation(string uid)
        {
            string[] ar = new string[10];
            try
            {
                HappyWatchDataDataContext context = new HappyWatchDataDataContext();
                ar = uid.Split('#');
                var obPepId = ((from r in context.Peoples
                                where r.Email.Equals(ar[0])
                                select r.PeopleId).Take(1)).Single();
                var patPepId = ((from r in context.Peoples
                                 where r.Email.Equals(ar[1])
                                 select r.PeopleId).Take(1)).Single();
                var obId = ((from r in context.Observers
                             where r.Person_PeopleId == obPepId
                             select r.ObserverId).Take(1)).Single();
                var patId = ((from r in context.Patients
                              where r.Person_PeopleId == patPepId
                              select r.PatientId).Take(1)).Single();

                var rel = ((from r in context.PatientVSObservers
                            where (r.PatientPatientId == patId && r.ObserverObserverId == obId)
                            select r).Take(1)).Single();
                rel.POStatus = "active";
                context.SubmitChanges();
                return true;


            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public Boolean ObserverRejectRelation(string uid)
        {
            string[] ar = new string[10];
            try
            {
                HappyWatchDataDataContext context = new HappyWatchDataDataContext();
                ar = uid.Split('#');
                var obPepId = ((from r in context.Peoples
                                where r.Email.Equals(ar[0])
                                select r.PeopleId).Take(1)).Single();
                var patPepId = ((from r in context.Peoples
                                 where r.Email.Equals(ar[1])
                                 select r.PeopleId).Take(1)).Single();
                var obId = ((from r in context.Observers
                             where r.Person_PeopleId == obPepId
                             select r.ObserverId).Take(1)).Single();
                var patId = ((from r in context.Patients
                              where r.Person_PeopleId == patPepId
                              select r.PatientId).Take(1)).Single();

                var rel = ((from r in context.PatientVSObservers
                            where (r.PatientPatientId == patId && r.ObserverObserverId == obId)
                            select r).Take(1)).Single();
                rel.POStatus = "inactive";
                context.SubmitChanges();
                return true;


            }
            catch (Exception ex)
            {
                return false;
            }
 
        }

        public List<HospitalMod> MakeAllHosptialList()
        {
            List<HospitalMod> hosp = new List<HospitalMod>();
            try
            {
                HappyWatchDataDataContext context = new HappyWatchDataDataContext();
                var hospitalIds = from r in context.Hospitals select r.Id;
                foreach (var id in hospitalIds)
                {
                    HospitalMod temp = new HospitalMod();

                    temp.Name = ((from r in context.Hospitals
                                  where r.Id == id
                                  select r.Name).Take(1)).Single();
                    temp.Mail = ((from r in context.Hospitals
                                  where r.Id == id
                                  select r.Mail).Take(1)).Single();
                    temp.Address = ((from r in context.Hospitals
                                     where r.Id == id
                                     select r.Address).Take(1)).Single();
                    if (temp.Name.Equals("NONE"))
                    {
                    }
                    else
                    {
                        hosp.Add(temp);
                    }
                }
            }
            catch (Exception ex)
            {
                /////do something
            }
            return hosp.ToList();
        }

        public List<HospitalMod> MakeOwnHosptialList(string uid)
        {
            List<HospitalMod> hosp = new List<HospitalMod>();
            try
            {
                HappyWatchDataDataContext context = new HappyWatchDataDataContext();
                var patPepId=((from r in context.Peoples
                                   where r.Email.Equals(uid)
                                   select r.PeopleId).Take(1)).Single();
                var patId=((from r in context.Patients
                                where r.Person_PeopleId==patPepId
                                select r.PatientId).Take(1)).Single();
                var hospitalIds = from r in context.PatientVSHospitals1s
                                  where (r.PatientPatientId == patId && r.PHStatus.Equals("active"))
                                  select r.HospitalId;
                foreach (var id in hospitalIds)
                {
                    HospitalMod temp = new HospitalMod();
                    temp.Name = ((from r in context.Hospitals
                                  where r.Id == id
                                  select r.Name).Take(1)).Single();

                    temp.Mail = ((from r in context.Hospitals
                                  where r.Id == id
                                  select r.Mail).Take(1)).Single();

                    temp.Address = ((from r in context.Hospitals
                                  where r.Id == id
                                  select r.Address).Take(1)).Single();
                    hosp.Add(temp);
 
                }
                

            }
            catch (Exception ex)
            {
                //
            }
            return hosp.ToList();

        }

        public Boolean MakeHospitalReq(string uid)
        {
            string[] ar = new string[10];
            try
            {
                ar = uid.Split('#');
                HappyWatchDataDataContext context = new HappyWatchDataDataContext();
                var patPepId = ((from r in context.Peoples
                                 where r.Email.Equals(ar[0])
                                 select r.PeopleId).Take(1)).Single();
                var patId = ((from r in context.Patients
                              where r.Person_PeopleId == patPepId
                              select r.PatientId).Take(1)).Single();
                var hospitalId = ((from r in context.Hospitals
                                   where r.Mail.Equals(ar[1])
                                   select r.Id).Take(1)).Single();
                PatientVSHospitals1 req = new PatientVSHospitals1();
                req.PatientPatientId = patId;
                req.HospitalId = hospitalId;
                req.PHStatus = "pending";
                context.PatientVSHospitals1s.InsertOnSubmit(req);
                context.SubmitChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }

        }

        public List<ObPatient> ObserverVsPatientRequest(string uid)
        {
            List<ObPatient> obs = new List<ObPatient>();
            try
            {
                HappyWatchDataDataContext context = new HappyWatchDataDataContext();
                var pepId = ((from r in context.Peoples
                              where r.Email.Equals(uid)
                              select r.PeopleId).Take(1)).Single();
                var obId = ((from r in context.Observers
                             where r.Person_PeopleId == pepId
                             select r.ObserverId).Take(1)).Single();
                var patientIds = from r in context.PatientVSObservers
                                 where (r.ObserverObserverId == obId && r.POStatus.Equals("pending"))
                                 select r.PatientPatientId;
                foreach (var id in patientIds)
                {
                    ObPatient temp = new ObPatient();
                    var patPepId = ((from r in context.Patients
                                     where r.PatientId == id
                                     select r.Person_PeopleId).Take(1)).Single();
                    temp.pName = ((from r in context.Peoples
                                   where r.PeopleId == patPepId
                                   select r.Name).Take(1)).Single();
                    temp.pMail = ((from r in context.Peoples
                                   where r.PeopleId == patPepId
                                   select r.Email).Take(1)).Single();
                    obs.Add(temp);

                }


            }
            catch (Exception ex)
            {
                //do some
            }
            return obs.ToList();
        }

        public Boolean AcceptPatientReq(string uid)
        {
            string[] ar = new string[10];
            try
            {
                HappyWatchDataDataContext context = new HappyWatchDataDataContext();
                ar = uid.Split('#');
                var ObPepId = ((from r in context.Peoples
                                where r.Email.Equals(ar[0])
                                select r.PeopleId).Take(1)).Single();
                var PatPepId = ((from r in context.Peoples
                                 where r.Email.Equals(ar[1])
                                 select r.PeopleId).Take(1)).Single();
                var ObId = ((from r in context.Observers
                             where r.Person_PeopleId == ObPepId
                             select r.ObserverId).Take(1)).Single();
                var PatId = ((from r in context.Patients
                              where r.Person_PeopleId == PatPepId
                              select r.PatientId).Take(1)).Single();
                var req = ((from r in context.PatientVSObservers
                            where( r.PatientPatientId == PatId && r.ObserverObserverId == ObId)
                            select r).Take(1)).Single();
                req.POStatus = "active";
                context.SubmitChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public Boolean RejecttPatientReq(string uid)
        {
            string[] ar = new string[10];
            try
            {
                HappyWatchDataDataContext context = new HappyWatchDataDataContext();
                ar = uid.Split('#');
                var ObPepId = ((from r in context.Peoples
                                where r.Email.Equals(ar[0])
                                select r.PeopleId).Take(1)).Single();
                var PatPepId = ((from r in context.Peoples
                                 where r.Email.Equals(ar[1])
                                 select r.PeopleId).Take(1)).Single();
                var ObId = ((from r in context.Observers
                             where r.Person_PeopleId == ObPepId
                             select r.ObserverId).Take(1)).Single();
                var PatId = ((from r in context.Patients
                              where r.Person_PeopleId == PatPepId
                              select r.PatientId).Take(1)).Single();
                var req = ((from r in context.PatientVSObservers
                            where (r.PatientPatientId == PatId && r.ObserverObserverId == ObId)
                            select r).Take(1)).Single();
                req.POStatus = "inactive";
                context.SubmitChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
 
        }
        public Boolean AcceptPatientHospitalRequest(string uid)
        {
            string[] ar = new string[10];
            try
            {
                ar = uid.Split('#');
                HappyWatchDataDataContext context = new HappyWatchDataDataContext();
                var HospitalId = ((from r in context.Hospitals
                                   where r.Mail.Equals(ar[0])
                                   select r.Id).Take(1)).Single();
                var PatPepId = ((from r in context.Peoples
                                 where r.Email.Equals(ar[1])
                                 select r.PeopleId).Take(1)).Single();
                var PatId = ((from r in context.Patients
                              where r.Person_PeopleId == PatPepId
                              select r.PatientId).Take(1)).Single();
                var req = ((from r in context.PatientVSHospitals1s
                            where (r.PatientPatientId == PatId && r.HospitalId == HospitalId && r.PHStatus.Equals("pending"))
                            select r).Take(1)).Single();
                req.PHStatus = "active";
                context.SubmitChanges();
                return true;

            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public Boolean RejectPatientHospitalRequest(string uid)
        {
            string[] ar = new string[10];
            try
            {
                ar = uid.Split('#');
                HappyWatchDataDataContext context = new HappyWatchDataDataContext();
                var HospitalId = ((from r in context.Hospitals
                                   where r.Mail.Equals(ar[0])
                                   select r.Id).Take(1)).Single();
                var PatPepId = ((from r in context.Peoples
                                 where r.Email.Equals(ar[1])
                                 select r.PeopleId).Take(1)).Single();
                var PatId = ((from r in context.Patients
                              where r.Person_PeopleId == PatPepId
                              select r.PatientId).Take(1)).Single();
                var req = ((from r in context.PatientVSHospitals1s
                            where (r.PatientPatientId == PatId && r.HospitalId == HospitalId && r.PHStatus.Equals("pending"))
                            select r).Take(1)).Single();
                req.PHStatus = "inactive";
                context.SubmitChanges();
                return true;

            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public List<ObPatient> PatientHospitalReqList(string uid)
        {
            List<ObPatient> obs = new List<ObPatient>();
            try
            {
                HappyWatchDataDataContext context = new HappyWatchDataDataContext();
                var HospitalId = ((from r in context.Hospitals
                                   where r.Mail.Equals(uid)
                                   select r.Id).Take(1)).Single();
                var patientIds = from r in context.PatientVSHospitals1s
                                 where (r.HospitalId == HospitalId && r.PHStatus.Equals("pending"))
                                 select r.PatientPatientId;
                foreach (var id in patientIds)
                {
                    ObPatient temp = new ObPatient();
                    var patpepid = ((from r in context.Patients
                                     where r.PatientId == id
                                     select r.Person_PeopleId).Take(1)).Single();
                    temp.pName = ((from r in context.Peoples
                                   where r.PeopleId == patpepid
                                   select r.Name).Take(1)).Single();
                    temp.pMail = ((from r in context.Peoples
                                   where r.PeopleId == patpepid
                                   select r.Email).Take(1)).Single();
                    obs.Add(temp);
                    
 
                }
            }
            catch (Exception ex)
            {
                //do some
            }
            return obs.ToList();
        }

        public string GetLastPrescription(string uid)
        {
            string[] ar=new string[10];
            try
            {
                HappyWatchDataDataContext context=new HappyWatchDataDataContext();
               
                var patPepId = ((from r in context.Peoples
                                 where r.Email.Equals(uid)
                                 select r.PeopleId).Take(1)).Single();
               
                var patId = ((from r in context.Patients
                              where r.Person_PeopleId == patPepId
                              select r.PatientId).Take(1)).Single();
               


                var prescript = ((from r in context.Prescriptions
                                  orderby r.PrescriptionId descending
                                  where (r.PatientPatientId == patId )
                                  select r.Prescribed).Take(1)).Single();
                var docid = ((from r in context.Prescriptions
                              orderby r.PrescriptionId descending
                              where (r.PatientPatientId == patId)
                              select r.DoctorDoctorId).Take(1)).Single();

                var pepId = ((from r in context.Doctors
                              where r.DoctorId == docid
                              select r.Person_PeopleId).Take(1)).Single();
                var name = ((from r in context.Peoples
                             where r.PeopleId == pepId
                             select r.Name).Take(1)).Single();
                string sent;
                sent =name.ToString()+"#" +prescript.ToString();

                return sent;
            }
            catch (Exception ex)
            {
                return "N/A";
            }

        }

        public string GetDocInfo(string uid)
        {
            try
            {
                HappyWatchDataDataContext context = new HappyWatchDataDataContext();
                var pepId = ((from r in context.Peoples
                              where r.Email.Equals(uid)
                              select r.PeopleId).Take(1)).Single();
                var docId = ((from r in context.Doctors
                              where r.Person_PeopleId == pepId
                              select r.DoctorId).Take(1)).Single();
                var docName = ((from r in context.Peoples
                                where r.PeopleId == pepId
                                select r.Name).Take(1)).Single();
                var docMail = ((from r in context.Peoples
                                where r.PeopleId == pepId
                                select r.Email).Take(1)).Single();
                var docPass = ((from r in context.Peoples
                                where r.PeopleId == pepId
                                select r.Password).Take(1)).Single();
                var docCon = ((from r in context.Peoples
                               where r.PeopleId == pepId
                               select r.ContactNumber).Take(1)).Single();
                var docGen = ((from r in context.Peoples
                               where r.PeopleId == pepId
                               select r.Gender).Take(1)).Single();

                var docDob = ((from r in context.Peoples
                               where r.PeopleId == pepId
                               select r.BirthDay).Take(1)).Single();
                string sent = docName.ToString() + '#' + docMail.ToString() + '#' + docPass.ToString() + '#' + docCon.ToString() + '#' + docGen.ToString() + '#' + docDob.ToString();

                return sent;
            }
            catch (Exception ex)
            {
                return "N/A#N/A#N/A#N/A#N/A#N/A#N/A#N/A#N/A#N/A#N/A#N/A";
 
            }
        }

        public Boolean InsertPrescription(string uid)
        {
            string[] ar = new string[10];
            try
            {
                HappyWatchDataDataContext context = new HappyWatchDataDataContext();
                ar = uid.Split('#');
                var docPepId = ((from r in context.Peoples
                                 where r.Email.Equals(ar[0])
                                 select r.PeopleId).Take(1)).Single();
                var patPepId = ((from r in context.Peoples
                                 where r.Email.Equals(ar[1])
                                 select r.PeopleId).Take(1)).Single();
                var docId = ((from r in context.Doctors
                              where r.Person_PeopleId == docPepId
                              select r.DoctorId).Take(1)).Single();
                var patId = ((from r in context.Patients
                              where r.Person_PeopleId == patPepId
                              select r.PatientId).Take(1)).Single();
                Prescription presc = new Prescription();
                presc.DoctorDoctorId = docId;
                presc.PatientPatientId = patId;
                presc.Prescribed = ar[2];
                context.Prescriptions.InsertOnSubmit(presc);


                context.SubmitChanges();
                return true;

            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public Boolean PateintDeleteObserver(string uid)
        {
            string[] ar = new string[10];
            try
            {
                HappyWatchDataDataContext context = new HappyWatchDataDataContext();
                ar = uid.Split('#');
                var PatPepId = ((from r in context.Peoples
                                 where r.Email.Equals(ar[0])
                                 select r.PeopleId).Take(1)).Single();
                var PatId = ((from r in context.Patients
                              where r.Person_PeopleId == PatPepId
                              select r.PatientId).Take(1)).Single();
                var ObPepId = ((from r in context.Peoples
                                 where r.Email.Equals(ar[1])
                                 select r.PeopleId).Take(1)).Single();
                var ObId = ((from r in context.Observers
                             where r.Person_PeopleId == ObPepId
                             select r.ObserverId).Take(1)).Single();
                var req = ((from r in context.PatientVSObservers
                            where (r.PatientPatientId == PatId && r.ObserverObserverId == ObId)
                            select r).Take(1)).Single();
                req.POStatus = "inactive";
                context.SubmitChanges();
                return true;


            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public Boolean AdminDeletePatient(string uid)
        {
            string[] ar = new string[10];
            try
            {
                HappyWatchDataDataContext context = new HappyWatchDataDataContext();
                ar = uid.Split('#');
                var HospitalId = ((from r in context.Hospitals
                                   where r.Mail.Equals(ar[0])
                                   select r.Id).Take(1)).Single();
                var PatPepId = ((from r in context.Peoples
                                 where r.Email.Equals(ar[1])
                                 select r.PeopleId).Take(1)).Single();
                var PatId = ((from r in context.Patients
                              where r.Person_PeopleId == PatPepId
                              select r.PatientId).Take(1)).Single();
                var req = ((from r in context.PatientVSHospitals1s
                            where (r.HospitalId == HospitalId && r.PatientPatientId == PatId)
                            select r
                              ).Take(1)).Single();
                req.PHStatus = "inactive";
                context.SubmitChanges();
                return true;
                

            }
            catch (Exception ex)
            {
                return false;
            }

        }

        public Boolean AdminDeleteDoctor(string uid)
        {
            string[] ar = new string[10];
            try
            {
                HappyWatchDataDataContext context = new HappyWatchDataDataContext();
                ar = uid.Split('#');
                var HospitalId = ((from r in context.Hospitals
                                   where r.Mail.Equals(ar[0])
                                   select r.Id).Take(1)).Single();
                var DocPepId = ((from r in context.Peoples
                                 where r.Email.Equals(ar[1])
                                 select r.PeopleId).Take(1)).Single();
                var DocId = ((from r in context.Doctors
                              where r.Person_PeopleId == DocPepId
                              select r.DoctorId).Take(1)).Single();
                var req = ((from r in context.Doctors
                            where r.DoctorId == DocId && r.HospitalId == HospitalId
                            select r).Take(1)).Single();
               
                req.DoctorStatus = "inactive";
                context.SubmitChanges();
                return true;


            }
            catch (Exception ex)
            {
                return false;
            }
 
        }
    }
}
