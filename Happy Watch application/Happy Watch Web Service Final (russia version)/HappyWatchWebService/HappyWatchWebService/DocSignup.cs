﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HappyWatchWebService
{
    public class DocSignup
    {
        public string dName { get; set; }
        public string dMail { get; set; }
        public string dLicense { get; set; }
       
        public string specialist { get; set; }
    }
}