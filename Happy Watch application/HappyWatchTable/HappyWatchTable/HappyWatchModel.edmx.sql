
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, and Azure
-- --------------------------------------------------
-- Date Created: 11/09/2013 10:03:05
-- Generated from EDMX file: F:\Dropbox\Happy Watch\Happy Watch application\HappyWatchTable\HappyWatchTable\HappyWatchModel.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [HappyWatchDatabase];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[FK_PeopleObserver]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Observers] DROP CONSTRAINT [FK_PeopleObserver];
GO
IF OBJECT_ID(N'[dbo].[FK_PeoplePatient]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Patients] DROP CONSTRAINT [FK_PeoplePatient];
GO
IF OBJECT_ID(N'[dbo].[FK_PeopleDoctor]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Doctors] DROP CONSTRAINT [FK_PeopleDoctor];
GO
IF OBJECT_ID(N'[dbo].[FK_PatientHealthData]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[HealthDatas] DROP CONSTRAINT [FK_PatientHealthData];
GO
IF OBJECT_ID(N'[dbo].[FK_PatientPatientVSObserver]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[PatientVSObservers] DROP CONSTRAINT [FK_PatientPatientVSObserver];
GO
IF OBJECT_ID(N'[dbo].[FK_PatientPrescription]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Prescriptions] DROP CONSTRAINT [FK_PatientPrescription];
GO
IF OBJECT_ID(N'[dbo].[FK_PatientPatientVSDoctor]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[PatientVSDoctors] DROP CONSTRAINT [FK_PatientPatientVSDoctor];
GO
IF OBJECT_ID(N'[dbo].[FK_ObserverPatientVSObserver]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[PatientVSObservers] DROP CONSTRAINT [FK_ObserverPatientVSObserver];
GO
IF OBJECT_ID(N'[dbo].[FK_DoctorPrescription]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Prescriptions] DROP CONSTRAINT [FK_DoctorPrescription];
GO
IF OBJECT_ID(N'[dbo].[FK_DoctorPatientVSDoctor]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[PatientVSDoctors] DROP CONSTRAINT [FK_DoctorPatientVSDoctor];
GO
IF OBJECT_ID(N'[dbo].[FK_PatientDocVSpatientReq]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[DocVSpatientReqs] DROP CONSTRAINT [FK_PatientDocVSpatientReq];
GO
IF OBJECT_ID(N'[dbo].[FK_DoctorDocVSpatientReq]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[DocVSpatientReqs] DROP CONSTRAINT [FK_DoctorDocVSpatientReq];
GO
IF OBJECT_ID(N'[dbo].[FK_DoctorDocSignReq]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[DocSignReqs] DROP CONSTRAINT [FK_DoctorDocSignReq];
GO
IF OBJECT_ID(N'[dbo].[FK_HospitalAdmin]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Hospitals] DROP CONSTRAINT [FK_HospitalAdmin];
GO
IF OBJECT_ID(N'[dbo].[FK_HospitalDoctor]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Doctors] DROP CONSTRAINT [FK_HospitalDoctor];
GO
IF OBJECT_ID(N'[dbo].[FK_HospitalDocVSpatientReq]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[DocVSpatientReqs] DROP CONSTRAINT [FK_HospitalDocVSpatientReq];
GO
IF OBJECT_ID(N'[dbo].[FK_HospitalDocSignReq]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[DocSignReqs] DROP CONSTRAINT [FK_HospitalDocSignReq];
GO
IF OBJECT_ID(N'[dbo].[FK_PatientPatientVSHospital]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[PatientVSHospitals1] DROP CONSTRAINT [FK_PatientPatientVSHospital];
GO
IF OBJECT_ID(N'[dbo].[FK_HospitalPatientVSHospital]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[PatientVSHospitals1] DROP CONSTRAINT [FK_HospitalPatientVSHospital];
GO

-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[People]', 'U') IS NOT NULL
    DROP TABLE [dbo].[People];
GO
IF OBJECT_ID(N'[dbo].[Patients]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Patients];
GO
IF OBJECT_ID(N'[dbo].[Observers]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Observers];
GO
IF OBJECT_ID(N'[dbo].[Doctors]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Doctors];
GO
IF OBJECT_ID(N'[dbo].[HealthDatas]', 'U') IS NOT NULL
    DROP TABLE [dbo].[HealthDatas];
GO
IF OBJECT_ID(N'[dbo].[PatientVSObservers]', 'U') IS NOT NULL
    DROP TABLE [dbo].[PatientVSObservers];
GO
IF OBJECT_ID(N'[dbo].[Prescriptions]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Prescriptions];
GO
IF OBJECT_ID(N'[dbo].[PatientVSDoctors]', 'U') IS NOT NULL
    DROP TABLE [dbo].[PatientVSDoctors];
GO
IF OBJECT_ID(N'[dbo].[DocVSpatientReqs]', 'U') IS NOT NULL
    DROP TABLE [dbo].[DocVSpatientReqs];
GO
IF OBJECT_ID(N'[dbo].[DocSignReqs]', 'U') IS NOT NULL
    DROP TABLE [dbo].[DocSignReqs];
GO
IF OBJECT_ID(N'[dbo].[Hospitals]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Hospitals];
GO
IF OBJECT_ID(N'[dbo].[Admins]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Admins];
GO
IF OBJECT_ID(N'[dbo].[PatientVSHospitals1]', 'U') IS NOT NULL
    DROP TABLE [dbo].[PatientVSHospitals1];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'People'
CREATE TABLE [dbo].[People] (
    [id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NOT NULL,
    [ContactNumber] nvarchar(max)  NOT NULL,
    [Gender] nvarchar(max)  NOT NULL,
    [BirthDay] datetimeoffset  NOT NULL,
    [Status] nvarchar(max)  NOT NULL,
    [Designation] nvarchar(max)  NOT NULL,
    [Email] nvarchar(max)  NOT NULL,
    [Password] nvarchar(max)  NOT NULL,
    [Image] tinyint  NULL
);
GO

-- Creating table 'Patients'
CREATE TABLE [dbo].[Patients] (
    [id] int IDENTITY(1,1) NOT NULL,
    [DeviceId] nvarchar(max)  NOT NULL,
    [PhysicalCondition] nvarchar(max)  NOT NULL,
    [Isactive] nvarchar(max)  NOT NULL,
    [MinPulse] nvarchar(max)  NULL,
    [MaxPulse] nvarchar(max)  NULL,
    [Person_id] int  NOT NULL
);
GO

-- Creating table 'Observers'
CREATE TABLE [dbo].[Observers] (
    [id] int IDENTITY(1,1) NOT NULL,
    [Person_id] int  NOT NULL
);
GO

-- Creating table 'Doctors'
CREATE TABLE [dbo].[Doctors] (
    [id] int IDENTITY(1,1) NOT NULL,
    [LisenceId] nvarchar(max)  NOT NULL,
    [HospitalName] nvarchar(max)  NOT NULL,
    [DoctorStatus] nvarchar(max)  NOT NULL,
    [Specialist] nvarchar(max)  NOT NULL,
    [HospitalId] int  NOT NULL,
    [Person_id] int  NOT NULL
);
GO

-- Creating table 'HealthDatas'
CREATE TABLE [dbo].[HealthDatas] (
    [id] int IDENTITY(1,1) NOT NULL,
    [DeviceId] nvarchar(max)  NOT NULL,
    [PulseRate] nvarchar(max)  NULL,
    [Time] datetime  NULL,
    [Location] nvarchar(max)  NULL,
    [PatientPatientId] int  NOT NULL
);
GO

-- Creating table 'PatientVSObservers'
CREATE TABLE [dbo].[PatientVSObservers] (
    [id] int IDENTITY(1,1) NOT NULL,
    [POStatus] nvarchar(max)  NOT NULL,
    [Relation] nvarchar(max)  NOT NULL,
    [PatientPatientId] int  NOT NULL,
    [ObserverObserverId] int  NOT NULL
);
GO

-- Creating table 'Prescriptions'
CREATE TABLE [dbo].[Prescriptions] (
    [id] int IDENTITY(1,1) NOT NULL,
    [Time] datetime  NULL,
    [Prescribed] nvarchar(max)  NOT NULL,
    [PatientPatientId] int  NOT NULL,
    [DoctorDoctorId] int  NOT NULL
);
GO

-- Creating table 'PatientVSDoctors'
CREATE TABLE [dbo].[PatientVSDoctors] (
    [id] int IDENTITY(1,1) NOT NULL,
    [PDStatus] nvarchar(max)  NOT NULL,
    [Relation] nvarchar(max)  NOT NULL,
    [DoctorDoctorId] int  NOT NULL,
    [PatientPatientId] int  NOT NULL
);
GO

-- Creating table 'DocVSpatientReqs'
CREATE TABLE [dbo].[DocVSpatientReqs] (
    [id] int IDENTITY(1,1) NOT NULL,
    [Status] nvarchar(max)  NOT NULL,
    [PatientPatientId] int  NOT NULL,
    [DoctorDoctorId] int  NOT NULL,
    [HospitalId] int  NOT NULL
);
GO

-- Creating table 'DocSignReqs'
CREATE TABLE [dbo].[DocSignReqs] (
    [id] int IDENTITY(1,1) NOT NULL,
    [Status] nvarchar(max)  NOT NULL,
    [HospitalId] int  NOT NULL,
    [Doctor_id] int  NOT NULL
);
GO

-- Creating table 'Hospitals'
CREATE TABLE [dbo].[Hospitals] (
    [id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NOT NULL,
    [Address] nvarchar(max)  NOT NULL,
    [Mail] nvarchar(max)  NOT NULL,
    [Admin_id] int  NOT NULL
);
GO

-- Creating table 'Admins'
CREATE TABLE [dbo].[Admins] (
    [id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NOT NULL,
    [Password] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'PatientVSHospitals1'
CREATE TABLE [dbo].[PatientVSHospitals1] (
    [id] int IDENTITY(1,1) NOT NULL,
    [PHStatus] nvarchar(max)  NOT NULL,
    [PatientPatientId] int  NOT NULL,
    [HospitalId] int  NOT NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [id] in table 'People'
ALTER TABLE [dbo].[People]
ADD CONSTRAINT [PK_People]
    PRIMARY KEY CLUSTERED ([id] ASC);
GO

-- Creating primary key on [id] in table 'Patients'
ALTER TABLE [dbo].[Patients]
ADD CONSTRAINT [PK_Patients]
    PRIMARY KEY CLUSTERED ([id] ASC);
GO

-- Creating primary key on [id] in table 'Observers'
ALTER TABLE [dbo].[Observers]
ADD CONSTRAINT [PK_Observers]
    PRIMARY KEY CLUSTERED ([id] ASC);
GO

-- Creating primary key on [id] in table 'Doctors'
ALTER TABLE [dbo].[Doctors]
ADD CONSTRAINT [PK_Doctors]
    PRIMARY KEY CLUSTERED ([id] ASC);
GO

-- Creating primary key on [id] in table 'HealthDatas'
ALTER TABLE [dbo].[HealthDatas]
ADD CONSTRAINT [PK_HealthDatas]
    PRIMARY KEY CLUSTERED ([id] ASC);
GO

-- Creating primary key on [id] in table 'PatientVSObservers'
ALTER TABLE [dbo].[PatientVSObservers]
ADD CONSTRAINT [PK_PatientVSObservers]
    PRIMARY KEY CLUSTERED ([id] ASC);
GO

-- Creating primary key on [id] in table 'Prescriptions'
ALTER TABLE [dbo].[Prescriptions]
ADD CONSTRAINT [PK_Prescriptions]
    PRIMARY KEY CLUSTERED ([id] ASC);
GO

-- Creating primary key on [id] in table 'PatientVSDoctors'
ALTER TABLE [dbo].[PatientVSDoctors]
ADD CONSTRAINT [PK_PatientVSDoctors]
    PRIMARY KEY CLUSTERED ([id] ASC);
GO

-- Creating primary key on [id] in table 'DocVSpatientReqs'
ALTER TABLE [dbo].[DocVSpatientReqs]
ADD CONSTRAINT [PK_DocVSpatientReqs]
    PRIMARY KEY CLUSTERED ([id] ASC);
GO

-- Creating primary key on [id] in table 'DocSignReqs'
ALTER TABLE [dbo].[DocSignReqs]
ADD CONSTRAINT [PK_DocSignReqs]
    PRIMARY KEY CLUSTERED ([id] ASC);
GO

-- Creating primary key on [id] in table 'Hospitals'
ALTER TABLE [dbo].[Hospitals]
ADD CONSTRAINT [PK_Hospitals]
    PRIMARY KEY CLUSTERED ([id] ASC);
GO

-- Creating primary key on [id] in table 'Admins'
ALTER TABLE [dbo].[Admins]
ADD CONSTRAINT [PK_Admins]
    PRIMARY KEY CLUSTERED ([id] ASC);
GO

-- Creating primary key on [id] in table 'PatientVSHospitals1'
ALTER TABLE [dbo].[PatientVSHospitals1]
ADD CONSTRAINT [PK_PatientVSHospitals1]
    PRIMARY KEY CLUSTERED ([id] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- Creating foreign key on [Person_id] in table 'Observers'
ALTER TABLE [dbo].[Observers]
ADD CONSTRAINT [FK_PeopleObserver]
    FOREIGN KEY ([Person_id])
    REFERENCES [dbo].[People]
        ([id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_PeopleObserver'
CREATE INDEX [IX_FK_PeopleObserver]
ON [dbo].[Observers]
    ([Person_id]);
GO

-- Creating foreign key on [Person_id] in table 'Patients'
ALTER TABLE [dbo].[Patients]
ADD CONSTRAINT [FK_PeoplePatient]
    FOREIGN KEY ([Person_id])
    REFERENCES [dbo].[People]
        ([id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_PeoplePatient'
CREATE INDEX [IX_FK_PeoplePatient]
ON [dbo].[Patients]
    ([Person_id]);
GO

-- Creating foreign key on [Person_id] in table 'Doctors'
ALTER TABLE [dbo].[Doctors]
ADD CONSTRAINT [FK_PeopleDoctor]
    FOREIGN KEY ([Person_id])
    REFERENCES [dbo].[People]
        ([id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_PeopleDoctor'
CREATE INDEX [IX_FK_PeopleDoctor]
ON [dbo].[Doctors]
    ([Person_id]);
GO

-- Creating foreign key on [PatientPatientId] in table 'HealthDatas'
ALTER TABLE [dbo].[HealthDatas]
ADD CONSTRAINT [FK_PatientHealthData]
    FOREIGN KEY ([PatientPatientId])
    REFERENCES [dbo].[Patients]
        ([id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_PatientHealthData'
CREATE INDEX [IX_FK_PatientHealthData]
ON [dbo].[HealthDatas]
    ([PatientPatientId]);
GO

-- Creating foreign key on [PatientPatientId] in table 'PatientVSObservers'
ALTER TABLE [dbo].[PatientVSObservers]
ADD CONSTRAINT [FK_PatientPatientVSObserver]
    FOREIGN KEY ([PatientPatientId])
    REFERENCES [dbo].[Patients]
        ([id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_PatientPatientVSObserver'
CREATE INDEX [IX_FK_PatientPatientVSObserver]
ON [dbo].[PatientVSObservers]
    ([PatientPatientId]);
GO

-- Creating foreign key on [PatientPatientId] in table 'Prescriptions'
ALTER TABLE [dbo].[Prescriptions]
ADD CONSTRAINT [FK_PatientPrescription]
    FOREIGN KEY ([PatientPatientId])
    REFERENCES [dbo].[Patients]
        ([id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_PatientPrescription'
CREATE INDEX [IX_FK_PatientPrescription]
ON [dbo].[Prescriptions]
    ([PatientPatientId]);
GO

-- Creating foreign key on [PatientPatientId] in table 'PatientVSDoctors'
ALTER TABLE [dbo].[PatientVSDoctors]
ADD CONSTRAINT [FK_PatientPatientVSDoctor]
    FOREIGN KEY ([PatientPatientId])
    REFERENCES [dbo].[Patients]
        ([id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_PatientPatientVSDoctor'
CREATE INDEX [IX_FK_PatientPatientVSDoctor]
ON [dbo].[PatientVSDoctors]
    ([PatientPatientId]);
GO

-- Creating foreign key on [ObserverObserverId] in table 'PatientVSObservers'
ALTER TABLE [dbo].[PatientVSObservers]
ADD CONSTRAINT [FK_ObserverPatientVSObserver]
    FOREIGN KEY ([ObserverObserverId])
    REFERENCES [dbo].[Observers]
        ([id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_ObserverPatientVSObserver'
CREATE INDEX [IX_FK_ObserverPatientVSObserver]
ON [dbo].[PatientVSObservers]
    ([ObserverObserverId]);
GO

-- Creating foreign key on [DoctorDoctorId] in table 'Prescriptions'
ALTER TABLE [dbo].[Prescriptions]
ADD CONSTRAINT [FK_DoctorPrescription]
    FOREIGN KEY ([DoctorDoctorId])
    REFERENCES [dbo].[Doctors]
        ([id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_DoctorPrescription'
CREATE INDEX [IX_FK_DoctorPrescription]
ON [dbo].[Prescriptions]
    ([DoctorDoctorId]);
GO

-- Creating foreign key on [DoctorDoctorId] in table 'PatientVSDoctors'
ALTER TABLE [dbo].[PatientVSDoctors]
ADD CONSTRAINT [FK_DoctorPatientVSDoctor]
    FOREIGN KEY ([DoctorDoctorId])
    REFERENCES [dbo].[Doctors]
        ([id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_DoctorPatientVSDoctor'
CREATE INDEX [IX_FK_DoctorPatientVSDoctor]
ON [dbo].[PatientVSDoctors]
    ([DoctorDoctorId]);
GO

-- Creating foreign key on [PatientPatientId] in table 'DocVSpatientReqs'
ALTER TABLE [dbo].[DocVSpatientReqs]
ADD CONSTRAINT [FK_PatientDocVSpatientReq]
    FOREIGN KEY ([PatientPatientId])
    REFERENCES [dbo].[Patients]
        ([id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_PatientDocVSpatientReq'
CREATE INDEX [IX_FK_PatientDocVSpatientReq]
ON [dbo].[DocVSpatientReqs]
    ([PatientPatientId]);
GO

-- Creating foreign key on [DoctorDoctorId] in table 'DocVSpatientReqs'
ALTER TABLE [dbo].[DocVSpatientReqs]
ADD CONSTRAINT [FK_DoctorDocVSpatientReq]
    FOREIGN KEY ([DoctorDoctorId])
    REFERENCES [dbo].[Doctors]
        ([id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_DoctorDocVSpatientReq'
CREATE INDEX [IX_FK_DoctorDocVSpatientReq]
ON [dbo].[DocVSpatientReqs]
    ([DoctorDoctorId]);
GO

-- Creating foreign key on [Doctor_id] in table 'DocSignReqs'
ALTER TABLE [dbo].[DocSignReqs]
ADD CONSTRAINT [FK_DoctorDocSignReq]
    FOREIGN KEY ([Doctor_id])
    REFERENCES [dbo].[Doctors]
        ([id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_DoctorDocSignReq'
CREATE INDEX [IX_FK_DoctorDocSignReq]
ON [dbo].[DocSignReqs]
    ([Doctor_id]);
GO

-- Creating foreign key on [Admin_id] in table 'Hospitals'
ALTER TABLE [dbo].[Hospitals]
ADD CONSTRAINT [FK_HospitalAdmin]
    FOREIGN KEY ([Admin_id])
    REFERENCES [dbo].[Admins]
        ([id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_HospitalAdmin'
CREATE INDEX [IX_FK_HospitalAdmin]
ON [dbo].[Hospitals]
    ([Admin_id]);
GO

-- Creating foreign key on [HospitalId] in table 'Doctors'
ALTER TABLE [dbo].[Doctors]
ADD CONSTRAINT [FK_HospitalDoctor]
    FOREIGN KEY ([HospitalId])
    REFERENCES [dbo].[Hospitals]
        ([id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_HospitalDoctor'
CREATE INDEX [IX_FK_HospitalDoctor]
ON [dbo].[Doctors]
    ([HospitalId]);
GO

-- Creating foreign key on [HospitalId] in table 'DocVSpatientReqs'
ALTER TABLE [dbo].[DocVSpatientReqs]
ADD CONSTRAINT [FK_HospitalDocVSpatientReq]
    FOREIGN KEY ([HospitalId])
    REFERENCES [dbo].[Hospitals]
        ([id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_HospitalDocVSpatientReq'
CREATE INDEX [IX_FK_HospitalDocVSpatientReq]
ON [dbo].[DocVSpatientReqs]
    ([HospitalId]);
GO

-- Creating foreign key on [HospitalId] in table 'DocSignReqs'
ALTER TABLE [dbo].[DocSignReqs]
ADD CONSTRAINT [FK_HospitalDocSignReq]
    FOREIGN KEY ([HospitalId])
    REFERENCES [dbo].[Hospitals]
        ([id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_HospitalDocSignReq'
CREATE INDEX [IX_FK_HospitalDocSignReq]
ON [dbo].[DocSignReqs]
    ([HospitalId]);
GO

-- Creating foreign key on [PatientPatientId] in table 'PatientVSHospitals1'
ALTER TABLE [dbo].[PatientVSHospitals1]
ADD CONSTRAINT [FK_PatientPatientVSHospital]
    FOREIGN KEY ([PatientPatientId])
    REFERENCES [dbo].[Patients]
        ([id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_PatientPatientVSHospital'
CREATE INDEX [IX_FK_PatientPatientVSHospital]
ON [dbo].[PatientVSHospitals1]
    ([PatientPatientId]);
GO

-- Creating foreign key on [HospitalId] in table 'PatientVSHospitals1'
ALTER TABLE [dbo].[PatientVSHospitals1]
ADD CONSTRAINT [FK_HospitalPatientVSHospital]
    FOREIGN KEY ([HospitalId])
    REFERENCES [dbo].[Hospitals]
        ([id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_HospitalPatientVSHospital'
CREATE INDEX [IX_FK_HospitalPatientVSHospital]
ON [dbo].[PatientVSHospitals1]
    ([HospitalId]);
GO

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------