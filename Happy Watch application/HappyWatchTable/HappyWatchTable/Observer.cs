//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace HappyWatchTable
{
    using System;
    using System.Collections.Generic;
    
    public partial class Observer
    {
        public Observer()
        {
            this.PatientVSObservers = new HashSet<PatientVSObserver>();
        }
    
        public int id { get; set; }
    
        public virtual People Person { get; set; }
        public virtual ICollection<PatientVSObserver> PatientVSObservers { get; set; }
    }
}
